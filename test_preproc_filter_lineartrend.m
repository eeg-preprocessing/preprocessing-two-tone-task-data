% Test the preprocessing filter

Fs = 256; % sampling rate, in Hz
dur = 30; % duration of the signal, in s
signal = ((0:(dur*Fs-1))/dur - dur/2)'; % linearly increase from -0.5 to 0.5 over duration

% run the preprocessing filter
% flt = preproc_filter(signal,Fs);
flt = prefiltereeg(signal,Fs);
corrflt = rmfltartifact(flt,Fs);

figure
subplot(3,1,1)
plot(0:(dur*Fs-1),signal,'k');
title('Original signal');

subplot(3,1,2)
plot(0:(dur*Fs-1),flt,'b');
title('Filtered signal');

subplot(3,1,3)
plot(0:(dur*Fs-1),corrflt,'g');
title('Filtered after artifact removal');