function xLag = lagGen(x,lags)
%lagGen Lag generator.
%   [XLAG] = LAGGEN(X,LAGS) returns the matrix XLAG containing the lagged
%   time series of X for a range of time lags given by the vector LAGS. If
%   X is multivariate, LAGGEN will concatenate the features for each lag
%   along the columns of XLAG.
%
%   Inputs:
%   x    - vector or matrix of time series data (time by features)
%   lags - vector of integer time lags (samples)
%
%   Outputs:
%   xLag - matrix of lagged time series data (time by lags*feats)
%
%   Nate Zuk (2020): Modified from Mick Crosse's mTRF Toolbox function,
%   lagGen.m
%       - lag matrix ordered so that all lags for each column along x are
%       grouped together

nt = size(x,1); % number of time points
nvar = size(x,2); % number of separate variables (columns of x)

xLag = zeros(nt,nvar*length(lags));

for ii = 1:nvar
    var_idx = (1:length(lags))+length(lags)*(ii-1); % identify the indexes
        % at which to place the lag matrix for x-variable i (ensures that
        % lags for a specific variable are grouped together)
    for jj = 1:length(lags)
        % insert the lagged versions, zeropadding the ends
        if lags(jj) < 0 % when lag < 0, the array is shifted up in rows of x
            xLag(1:end+lags(jj),var_idx(jj)) = x(-lags(jj)+1:end,ii);
        elseif lags(jj) > 0 % when lag > 0, the array is shifted down in rows of x
            xLag(lags(jj)+1:end,var_idx(jj)) = x(1:end-lags(jj),ii);
        else
            xLag(:,var_idx(jj)) = x(:,ii);
        end
    end
end

end