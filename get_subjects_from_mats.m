function sbjs = get_subjects_from_mats(res_path)
% For a results directory containing .mat files, get a list of all of the
% subjects. This assumes that 1) the subject ID is at the start of the
% filename, 2) descriptors are separated by underscores.
% Nate Zuk (2021)

% Get all files
fls = what(res_path);
% Get just .mat files
mats = fls.mat;

all_sbjs = {};
% Iterate through each mat file
for m = 1:length(mats)
    % Locate the underscores
    underscore_idx = strfind(mats{m},'_');
    % Get the text up to the first underscore,
    % this is the subject ID
    s = mats{m}(1:underscore_idx(1)-1);
    % store the subject id
    all_sbjs = [all_sbjs; {s}];
end

% Get only the unique subject IDs
sbjs = unique(all_sbjs);