function [ERP,dly] = compute_erp_linreg(stim,eeg,Fs,mindly,maxdly,baseline,idx,varargin)
% Compute the event-triggered EEG signal, equivalent to an ERP (event-related potential). 
% Inputs:
% - eeg = cell array containing EEG per trial
% - stim = array of stimulus vector for each trial (must contain 1s or 0s)
% - Fs = sampling frequency (Hz)
% - mindly = minimum possible delay of the model (ms)
% - maxdly = maximum possible delay of the model (ms)
% - baseline = two element array of delays to use as a baseline (ms)
% - idx = cell array of indexes to include in each trial (one element for each trial) 
% Outputs:
% - ERP = event-related potential model (lags x channels)
% - dly = delays corresponding to the ERP (in ms)
% Nate Zuk (2021)
% ** Modified from eventtrigeeg.m, Nate Zuk, 2017

ncond = size(stim{1},2); % # of conditions (columns of stim)

if nargin < 7 || isempty(idx), % if idx isn't specified...
    idx = {}; % set an empty cell
end

verbose = 1;

% Parse varargin
if ~isempty(varargin),
    for n = 2:2:length(varargin),
        eval([varargin{n-1} '=varargin{n};']);
    end
end

% note: delays are negative to account for the shift when creating the
% design matrix
dly = -floor(mindly/1000*Fs):-1:-ceil(maxdly/1000*Fs);

%% Create the matrices for X and Y
if verbose, fprintf('Creating X and Y matrices...'); end
mattm = tic;
X = create_design_matrix(eeg,dly,idx);
    % don't zero-center columns to get an unbiased ERP
Y = create_design_matrix(stim,0,idx);
clear eeg
if verbose, fprintf('Completed @ %.3f s\n',toc(mattm)); end

% Baseline the EEG responses in the design matrix
bsln_idx = dly<=-floor(baseline(1)/1000*Fs) & dly>=-ceil(baseline(2)/1000*Fs);
bsln = mean(X(:,bsln_idx),1); % average across indexes in baseline range
X = X - bsln*ones(1,length(dly));

% Compute XTX and XTy
[~,xty] = compute_linreg_matrices(X,Y,[],[],'verbose',verbose);

%% Compute the ERP model for each lambda value
if verbose, fprintf('Computing ERP model\n'); end
ERP = NaN(size(xty));
for c = 1:ncond,
    % divide by the number of events (non-whitened ERP)
    nevt = sum(cellfun(@(x) sum(x(:,c)),stim));
    ERP(:,c) = 1/nevt*xty(:,c);
end

% convert delays into ms, and return the negative of the delays (easier for
% plotting)
dly = -dly/Fs*1000;