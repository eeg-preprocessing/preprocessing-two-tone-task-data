% Test the EEG highpass filter
% (10-10-2021) Adjusted to test higher-frequency filtering (3-90 Hz) with
% 50 Hz notch

Fs = 256;
fc = 30; % carrier frequency of the sinusoid
dur = 10; % during of signal

% Make a sinusoid
t = (0:dur*Fs-1)/Fs;
x = cos(2*pi*t*fc)';
% x = randn(dur*Fs,1);

% Filter the signal
flt_x = prefiltereeg_high(x,Fs);

% Get the effect of edges
% flt_x = rmfltartifact_high(flt_x,Fs);
dlt_start = [1; zeros(dur*Fs-1,1)];
flt_start = prefiltereeg_high(dlt_start,Fs);
dlt_end = [zeros(dur*Fs-1,1); 1];
flt_end = prefiltereeg_high(dlt_end,Fs);

% Plot both the original and the filtered version (should be the same)
figure
hold on
plot(t,x,'k');
plot(t,flt_x,'b');
plot(t,flt_start,'r');
plot(t,flt_end,'r');
xlabel('Time (s)');

% Remove the artifact
corr_x = rmfltartifact(flt_x,Fs);
figure
hold on
plot(t,x,'k');
plot(t,corr_x,'g');
xlabel('Time (s)');
