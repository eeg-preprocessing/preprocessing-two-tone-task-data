% Merge multiple EEG preprocessed files together
preprocpth = 'A:\PreprocessedEEG\Shahaf2017\';

fls_to_merge = {'AVNE2973_active_lowfreq', 'AVNE2973_active_2_lowfreq'};
sv_flnm = 'AVNE2973_active_lowfreq_merge'; % file to use for the merged files

load([preprocpth fls_to_merge{1}]);

for n = 2:length(fls_to_merge)
    d = load([preprocpth fls_to_merge{n}]);
    eeg = [eeg; d.eeg];
    trigs = [trigs; d.trigs];
    nose = [nose; d.nose];
    mastoids = [mastoids; d.mastoids];
    eog = [eog; d.eog];
    eyeazim = [eyeazim; d.eyeazim];
end

save([preprocpth sv_flnm],'eeg','eFs','trigs','nose','mastoids','eog','eyeazim',...
    'vEEG','spCZ','chan_lbls');