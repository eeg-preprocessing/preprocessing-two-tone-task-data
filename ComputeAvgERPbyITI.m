% Load the evoked responses for each subject in each experiment group, and
% plot the average ERPs across sessions for each ITI, separated by experiment.
% Each experiment (generation of Shahaf's two-tone task) will be plotted 
% separately as different subplots.
% Nate Zuk (2021)

erppth = 'A:\ERP\';
% erppth = '/Volumes/NO NAME/ERP/';
% exp_labels = {'sagi2015','Shahaf2017','Shahaf2020','Shahaf2021','test_eeg_quality'};
% exp_labels = {'sagi2015','Shahaf2017','Shahaf2020','Shahaf2021','test_eeg_pas-act','test_eeg_act-pas'};
exp_labels = {'Shahaf2017','Shahaf2017_ASD_reject'};
exp_conditions = {'passive','active'};
% sound_delays = [50 60]; % indicates the time between trigger and sound onset for each experiment
% erp_range = [0 300]; % range of delays to use to get the ERP (in ms)
% baseline = [-100 0]; % range of delays to use as the baseline
itis = [2 3.5 6.5 9.5]; % ITIs for each condition (the floor of these values is 
    % the second number in each trigger value)
% Apply reference (can select either nose or mastoids)
reference = 'mastoids';
% Identify the channel to plot
chan_to_plot = {'Cz','Fz'};
chan_idx = [32 31]; % indexes in the ERP matrix corresponding to these channels

% store ERPs a cell arrays 
all_erps = cell(length(exp_conditions),length(exp_labels));
all_sbjs = cell(length(exp_labels),1);
for n = 1:length(exp_labels)
    fprintf('-- %s\n',exp_labels{n});
    % Get all subjects that were preprocessed
    sbjs = get_subjects_from_mats([erppth exp_labels{n} '/']);
    % Setup the peak-to-peak array
%     erps{n} = NaN(length(sbjs),length(exp_conditions),length(chan_to_plot));
    erps = cell(length(exp_conditions),length(sbjs));
    % Iterate through each subject
    for s = 1:length(sbjs)
        % Get the subject's ERP
        fl = sprintf('%s_ref-%s',sbjs{s},reference);
        res = load([erppth exp_labels{n} '/' fl]);
        for jj = 1:length(exp_conditions)
            erps{jj,s} = res.ERPs{jj};
        end
        dly = res.dly; % delay vector for the ERPs
        % show that this file has been loaded
        disp(fl);
    end
    % Now reorganize all of the ERPs so subjects are along the 4th
    % dimension of each array
    nchan = size(erps{1,1},2); % get the number of channels (assumed the same for all ERPs)
    % get the index for the particular ITI
%     iti_idx = res.itis == iti;
    for jj = 1:length(exp_conditions)
        all_erps{jj,n} = NaN(length(dly),nchan,length(itis),length(sbjs));
        for s = 1:length(sbjs)
            all_erps{jj,n}(:,:,:,s) = erps{jj,s};
        end
    end
    % Save the list of subjects/sessions
    all_sbjs{n} = sbjs;
end

%% Plotting
% Plot the ERPs
plot_avgerps_by_exp(all_erps,dly,chan_idx,...
    exp_conditions,chan_to_plot,itis,exp_labels,[1100 600]);