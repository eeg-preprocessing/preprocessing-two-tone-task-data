% Preprocess an individual bdf file using the same pre-processing pipeline
% that will be applied to all of the data
% Nate Zuk (2021)

% Using fieldtrip for data loading and some analysis
addpath('C:\Users\natha\Documents\Matlab\fieldtrip-20210614\');
% addpath('~/Documents/Matlab/fieldtrip-20200607/');
% Add NoiseTools
addpath('C:\Users\natha\Documents\Matlab\NoiseTools\');
% addpath('~/Documents/Matlab/NoiseTools/');
% To create documents with the figures
addpath('C:\Users\natha\Documents\Matlab\export_fig\');
% addpath('~/Documents/Matlab/export_fig/');

% Parameters relating to the organization of the stored EEG data
nchan = 32;
trigger_channel = 48;

% Directory for storing figures
fig_path = 'fig/';

%% Load the bdf file
% eegpth = '/Volumes/NO NAME/RawEEG/';
eegpth = 'A:\RawEEG\';
exppth = 'Shahaf2017/';
sbj = 'RORU8269';
condition = 'active_2'; % typically 1 is for the passive condition, 2 is for active
if strcmp(condition,'passive')
    eegfnm = sprintf('%s_1.bdf',sbj);
elseif strcmp(condition,'active')
    eegfnm = sprintf('%s_2.bdf',sbj);
else
    % for these subjects, the active condition appears to be split into two bdf files: 2
    % and 3
    if any(strcmp(sbj,{'AVNE2973','OSMA7669','SHAB5396'})) ...
            && strcmp(condition,'active_2')
        eegfnm = sprintf('%s_3.bdf',sbj);
    elseif strcmp(sbj,'NASH7723') && strcmp(condition,'passive_2')
        eegfnm = sprintf('%s_1_2.bdf',sbj);
    elseif strcmp(sbj,'RORU8269') && strcmp(condition,'active_2')
        eegfnm = sprintf('%s_2-2.bdf',sbj);
    else
        error('condition must be passive or active');
    end
end

disp('Loading EEG...');
cfg = [];
cfg.dataset = [eegpth exppth eegfnm];
data_eeg = ft_preprocessing(cfg);

% load the eeg data
eeg = data_eeg.trial{1}(1:nchan,1:end)';

% get the triggers
trigs = data_eeg.trial{1}(trigger_channel,1:end)';
% subtract the minimum trigger value, corresponding to 0
trigs = trigs-min(trigs);
% get the triggers modulo 256 (because sometimes the trigger indexes are
% offset by 65536, especially when the recording is restarted)
trigs = mod(trigs,256);

% save the nose channel separately
if strcmp(sbj(1:max(length(sbj))),'AMBO7268_2')
    % In AMBO7268, we used a new computer and reinstalled ActiView, so
    % these labeles were named as EXB1-7 by default
    nose = get_labeled_channel(data_eeg,{'EXG1'});
    % save the EOG channels separately
    eog = get_labeled_channel(data_eeg,{'EXG4','EXG5'});
    % save the HR/HL channels separately (left-right, azimuthal eye movements)
    eyeazim = get_labeled_channel(data_eeg,{'EXG3','EXG2'});
    % save the mastoid channels separately
    mastoids = get_labeled_channel(data_eeg,{'EXG6','EXG7'});
else
    nose = get_labeled_channel(data_eeg,{'Nose'});
    % save the EOG channels separately
    eog = get_labeled_channel(data_eeg,{'EOGu','EOGd'});
    % save the HR/HL channels separately (left-right, azimuthal eye movements)
    eyeazim = get_labeled_channel(data_eeg,{'HL','HR'});
    % save the mastoid channels separately
    mastoids = get_labeled_channel(data_eeg,{'M1','M2'});
end


% In AMBO7268, we need to remove long segments between blocks with large
% artifacts (after 1st block of active
if strcmp(sbj(1:max(length(sbj))),'AMBO7268-2') && strcmp(condition,'active')
    skip_samples = 130000:170000;
    eeg(skip_samples,:) = [];
    trigs(skip_samples) = [];
    nose(skip_samples) = [];
    eog(skip_samples,:) = [];
    eyeazim(skip_samples,:) = [];
    mastoids(skip_samples,:) = [];
end

if strcmp(sbj(1:max(length(sbj))),'AMAV4047-2') && strcmp(condition,'active')
    skip_samples = 355000:403000;
    eeg(skip_samples,:) = [];
    trigs(skip_samples) = [];
    nose(skip_samples) = [];
    eog(skip_samples,:) = [];
    eyeazim(skip_samples,:) = [];
    mastoids(skip_samples,:) = [];
end

% get the sampling rate
eFs = data_eeg.fsample;
% save the channel labels
chan_lbls = data_eeg.label(1:nchan);

clear data_eeg % remove the data structure, to save space

%% Linear detrending
%%% Use robust linear detrending (use NoiseTools: de Cheveigne & Arzounian),
%%% apply to each channel individually
disp('Linear detrending...');
eeg = nt_detrend(eeg,1);
nose = nt_detrend(nose,1);
eog = nt_detrend(eog,1);
eyeazim = nt_detrend(eyeazim,1);
mastoids = nt_detrend(mastoids,1);

% AMBO7268 passive block 1 was collected at 2048. Downsample this to 256.
if eFs>256
    disp('Resampling the EEG to 256 Hz...');
    eeg = resample(eeg,256,eFs);
    nose = resample(nose,256,eFs);
    eog = resample(eog,256,eFs);
    eyeazim = resample(eyeazim,256,eFs);
    mastoids = resample(mastoids,256,eFs);
    % ...and downsample the triggers using 'downsample'
    trigs = downsample(trigs,eFs/256);
    % then save the new sampling rate
    eFs = 256;
end

%% Filtering
%%% Filter from 1 - 30 Hz with a 4th order butterworth filter
%%% (removing the artifact at the beginning and end with linear regression
%%% seems more effective than padding the start and end)
disp('Filtering between 1-30 Hz with 4th order butterworth...');
eeg = prefiltereeg(eeg,eFs); eeg = rmfltartifact(eeg,eFs);
nose = prefiltereeg(nose,eFs); nose = rmfltartifact(nose,eFs);
eog = prefiltereeg(eog,eFs); eog = rmfltartifact(eog,eFs);
eyeazim = prefiltereeg(eyeazim,eFs); eyeazim = rmfltartifact(eyeazim,eFs);
mastoids = prefiltereeg(mastoids,eFs); mastoids = rmfltartifact(mastoids,eFs);

% very sharp notch filter around 50 Hz
% elect_noise = brickfilter(eeg,[45 55],eFs); eeg = eeg - elect_noise;

%% Re-referencing to nose
%%% Reference to the nose channel
% eeg = eeg - nose*ones(1,nchan);
% eog = eog - nose*ones(1,2);
% eyeazim = eyeazim - nose*ones(1,2);
% mastoids = mastoids - nose*ones(1,2);
% clear nose

%% Variance calculation
%%% (Amos's recommendation) Save the channel variance before removing
%%% components
vEEG = var(eeg);
[spCZ,frq] = pwelch(eeg(:,32),2*eFs,[],[],eFs);

%%% Make an example plot of an eyeblink before and after removal (use Fp1)
fp = eeg(:,1);
% get an eyeblink based on the presence of sparse voltages
eyeblink_thres = quantile(fp,0.993);
% for each triggered eyeblink, get the peak voltage within a 500 ms window
% around the peak
[eyeblink,eyeblink_idx] = get_triggered_eyeblinks(fp,eyeblink_thres,eFs);

%% Remove EOG channels
%%% From each channel, remove:
%%% 1) the best fit of the EOG channels (using linear regression) to remove
%%% eyeblinks
eeg = remove_linreg_component_fit(eeg,[eog eyeazim]);
%%% 2) the best fit of the HR/HL channels (using linear regression) to remove
%%% eye motion artifacts
% eeg = remove_linreg_component_fit(eeg,eyeazim);
%%% 3) (maybe) the best fit of the two mastoid channels to remove
%%% additional electrical artifacts
vEEG_afterrmv = var(eeg);
spCZ_afterrmv = pwelch(eeg(:,32),2*eFs,[],[],eFs);
%%% ** Note: spectrum of Cz doesn't change much after artifact removal if the
%%% signals are not being subtracted from the nose reference.
%%% Remove eyeblinks from nose reference
nose = remove_linreg_component_fit(nose,[eog eyeazim]);
%%% Remove eyeblinks from mastoid reference
mastoids = remove_linreg_component_fit(mastoids,[eog eyeazim]);

%%% Identify channels with high or low variance and interpolate those
%%% channels (need to identify a reasonable threshold)
%%% -- May not be needed, very few channels used

% Re-reference to nose channel
% eeg = eeg - nose*ones(1,nchan);

% get the indexes originally with eyeblinks after eyeblink removal
rmv_eyeblink = NaN(size(eyeblink));
for n = 1:size(eyeblink,2)
    use_idx = ~isnan(eyeblink_idx(:,n));
    rmv_eyeblink(use_idx,n) = eeg(eyeblink_idx(use_idx,n),1);
end

%% Plotting
fig_fl = sprintf('%s_%s_preproc.pdf',sbj,condition);
% Plot fp1 before and after eyeblink removal
dly = -ceil(0.25*eFs):ceil(0.25*eFs);
figure
hold on
plot(dly,eyeblink,'k');
plot(dly,rmv_eyeblink,'r');
set(gca,'FontSize',14);
xlabel('Delay (ms)');
ylabel('\muV');
title('FP1, before and after eyeblink removal');
export_fig([fig_path exppth fig_fl]);

% Plot the variance of the electrodes before and after removing EOG / HR /
% HL electrodes
figure
hold on
plot(vEEG,'k');
plot(vEEG_afterrmv,'b');
set(gca,'FontSize',12,'XTick',1:nchan,'XTickLabel',chan_lbls,...
    'XTickLabelRotation',90);
ylabel('Channel variance (\muV^2)');
legend('Original','After EOG removal');
export_fig([fig_path exppth fig_fl],'-append');

% plot the spectra
figure
hold on
plot(frq,spCZ,'k');
plot(frq,spCZ_afterrmv,'b');
set(gca,'FontSize',14,'XScale','log','YScale','log');
ylabel('Power spectrum of Cz');
legend('Original','After EOG removal','Location','southwest');
export_fig([fig_path exppth fig_fl],'-append');

%% Save the eeg data and the triggers
% svpth = '/Volumes/NO NAME/PreprocessedEEG/';
svpth = 'A:\PreprocessedEEG\';
svfnm = sprintf('%s_%s',sbj,condition);
save([svpth exppth svfnm],'eeg','eFs','trigs','nose','mastoids',...
    'vEEG','vEEG_afterrmv','spCZ','spCZ_afterrmv','chan_lbls');

%% ** Functions **
function chan = get_labeled_channel(data_eeg,labels)
% For a cell array of strings containing channel labels, get those channels
% from the data structure
chan_idx = NaN(length(labels),1);
for n = 1:length(labels), chan_idx(n) = find(strcmp(data_eeg.label,labels{n})); end
chan = data_eeg.trial{1}(chan_idx,1:end)';
end

function rmvd_eeg = remove_linreg_component_fit(eeg,component)
% For each channel, find the best fit linear combination of the components
% (including a constant term) and remove the component from the channel
X = [ones(size(component,1),1) component];
b = X \ eeg;
rmvd_eeg = eeg - X*b;
end