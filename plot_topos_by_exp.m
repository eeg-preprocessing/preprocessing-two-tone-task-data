function plot_topos_by_exp(erps,dly,dly_ranges,iti_idx,cond_labels,...
    range_labels,iti_values,exp_labels,chanlocs,fig_position)
% Plot average topographies of the ERPs for a specific set of delay ranges.
% Each channel and ITI is plotted in its own figure. Conditions and 
% experiments are plotted in a ncond x nexp subplot in each figure.
% Inputs:
% - erps = a cell array of dimensions: conditions x experiments. Each
%       cell contains a 4D array with dimensions: delay x channels x ITI x
%       session
% - dly = array of ERP delays
% - dly_range = cell array of two-element numeric arrays containing the
% start and end of each range to be plotted
% - iti_idx = set of indexes for the ITIs that should be plotted
% - cond_labels = labels for each condition (cell array)
% - range_labels = labels for each range of delays (cell array)
% - iti_values = values for each ITI (numeric array)
% - exp_labels = labels for each experiment (cell array)
% - chanlocs = filename or structure containing the channel locations 
% - fig_position (optional) = size of the figure, in pixels
% Nate Zuk (2021)

if nargin<9 || isempty(fig_position)
    fig_position = '';
end

ncond = length(cond_labels);
nexp = length(exp_labels);

% setup the color map
try
    cmap = colormap('turbo');
catch err
    cmap = colormap('jet');
end
for n = 1:length(iti_idx)
    for m = 1:length(dly_ranges)
        % Plot for this pair of channel x iti
        figure;
        if ~isempty(fig_position)
            % specify the figure size
            set(gcf,'Position',[(m + (n-1)*length(iti_idx))*100 0 fig_position]);
        end
        % get the indexes for delays to include in the average
        dly_idx = dly>=dly_ranges{m}(1) & dly<=dly_ranges{m}(2);
        for jj = 1:nexp
            for ii = 1:ncond
                % identify the correct subplot to use
%                 sbplt_idx = (ii-1)*ncond+jj;
                sbplt_idx = (ii-1)*nexp+jj;
                subplot(ncond,nexp,sbplt_idx);
                hold on;
                % get the ERPs for this specific condition, experiment, and
                % ITI, but averaged across subjects
                sp_erp = mean(erps{ii,jj}(:,:,iti_idx(n),:),4);
                % plot the topography of the average across delays
                topoplot(mean(sp_erp(dly_idx,:)),chanlocs);
                colorbar;
                set(gca,'FontSize',12);
                % Labeling
                xlabel('Time (ms)');
                ylabel('Average ERP (\muV)');
                % Create the title
                tle = sprintf('%s, %s, %s; %.1f s ITI',range_labels{m},...
                    exp_labels{jj},cond_labels{ii},iti_values(n));
                title(tle);
            end
        end
    end
end