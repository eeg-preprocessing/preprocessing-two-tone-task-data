function plot_avgerps_by_exp(erps,dly,ch_idx,cond_labels,ch_labels,...
    iti_values,exp_labels,fig_position)
% Plot average ERPs for each ITI separately for each set of sessions in an
% experiment. This is intended to compare ERPs between generations of
% Shahaf's two-tone experiment. ITIs are plotted together in each figure.
% Conditions and experiments are plotted in a ncond x nexp subplot in each
% figure.
% Inputs:
% - erps = a cell array of dimensions: conditions x experiments. Each
%       cell contains a 4D array with dimensions: delay x channels x ITI x
%       session
% - dly = array of ERP delays
% - ch_idx = set of indexes for the channels that should be plotted
% - cond_labels = labels for each condition (cell array)
% - ch_labels = labels for each channel (cell array)
% - iti_values = values for each ITI (numeric array)
% - exp_labels = labels for each experiment (cell array)
% - fig_position (optional) = size of the figure, in pixels
% Nate Zuk (2021)

if nargin<8 || isempty(fig_position)
    fig_position = '';
end

ncond = length(cond_labels);
nexp = length(exp_labels);
nitis = size(erps{1},3); % the number of ITIs

% Create the legend for ITI values
iti_leg = cell(nitis,1);
for kk = 1:nitis
    iti_leg{kk} = sprintf('%.1f s ITI',iti_values(kk));
end

% setup the color map
try
    cmap = colormap('turbo');
catch err
    cmap = colormap('jet')*0.8;
end

for c = 1:length(ch_idx)
    % Plot for this pair of channel x iti
    figure;
    if ~isempty(fig_position)
        % specify the figure size
        set(gcf,'Position',[c*100 0 fig_position]);
    end
    for jj = 1:nexp
        for ii = 1:ncond
            % identify the correct subplot to use
%                 sbplt_idx = (ii-1)*ncond+jj;
            sbplt_idx = (ii-1)*nexp+jj;
            subplot(ncond,nexp,sbplt_idx);
            hold on;
            % plot the ERP for this specific channel and ITI, color
            % code by sessions
            for kk = 1:nitis
                % Get the erps for the specific ITI
                sp_erp = squeeze(erps{ii,jj}(:,ch_idx(c),kk,:));
                % plot each ITI as a different color
                clr_idx = get_color_idx(kk,nitis,cmap);
                plot(dly,mean(sp_erp,2),'Color',cmap(clr_idx,:),'LineWidth',1.5);
            end
            set(gca,'FontSize',14,'YLim',[-6 8]);
            % Labeling
            xlabel('Time (ms)');
            ylabel('Average ERP (\muV)');
            % Create the title
            tle = sprintf('%s, %s; %s',exp_labels{jj},cond_labels{ii},...
                ch_labels{c});
            title(tle,'Interpreter','none');
            legend(iti_leg)
        end
    end
end