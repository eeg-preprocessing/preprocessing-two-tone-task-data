function [dpr,propcorr,ntr,iti_set,stbias_sep,iti_order,f_smt_diff,f_tr_mean] = ...
    calculate_prevtrial_dpr_bias(sbj,behavpth,behavsuffix,exp)
% Calculate the short-term difference in d' for bias+ and bias- relative to
% the previous trial.
% Adaptated for Shahaf's experiment, dpr calculated for each ITI separately
% Update (10-5-2022): Output the short term bias separated by ITI
% Nate Zuk (2021)

if nargin<3 || isempty(behavsuffix)
    % by default, use the behavioral file labeled "_1" at the end
    behavsuffix = '2';
end

if nargin<4
    exp = 'FD_jitter';
end
bhv_fl = sprintf('%s_%s_%s',exp,sbj,behavsuffix);

% Load the data
d = load([behavpth bhv_fl]);
% convert responses from strings to 0 = f1>f2, 1 = f1<f2
resp = NaN(length(d.s1),1);
for n = 1:length(resp)
    if strcmp(d.resp{n},'1'), resp(n) = false;
    elseif strcmp(d.resp{n},'2'), resp(n) = true;
    else
        resp(n) = NaN;
    end
end

% Compute the semitone difference between tones
f_smt_diff = (log2(d.s2)-log2(d.s1))*12;

% Compute the trial means
f_tr_mean = (log2(d.s2)+log2(d.s1))/2;

% Identify the short-term bias on each trial
% bias+ = 1: tone direction is away from the mean of the previous trial
% bias- = 2: tone direction is towards the mean of the previous trial
stbias = NaN(length(d.s1),1);
for n = 2:length(d.s1)
    if log2(d.s1(n)) > f_tr_mean(n-1) && log2(d.s2(n)) > f_tr_mean(n-1) % if above tones in previous trial
        if f_smt_diff(n)>0, stbias(n) = 1; % tones go up - bias+
        elseif f_smt_diff(n)<0, stbias(n) = 2; % tones go down - bias-
        end
    elseif log2(d.s1(n)) < f_tr_mean(n-1) && log2(d.s2(n)) < f_tr_mean(n-1) % if below tones in previous trial
        if f_smt_diff(n)<0, stbias(n) = 1; % tones go down - bias+
        elseif f_smt_diff(n)>0, stbias(n) = 2; % tones go up - bias-
        end
    end
end

% Get the ITIs for each trial
iti = d.ITI;
[iti_set,iti_idx] = unique(iti);
% skip ITI==5, which corresponds to the start of a trial
iti_idx(iti_set==5.0) = [];
iti_set(iti_set==5.0) = [];

% Get the iti order, based on the number of trials per set
ntrials_per_block = length(iti)/length(iti_set);
iti_order = floor(iti_idx/ntrials_per_block)+1;

% Calculate the d-prime for bias+ trials and bias- trials
%%% Update (10-5-2022) -- I use stbias later, but it's
%%% missing bias0 trials...
stbias_short = stbias;
iti_short = iti;
% Calculate the d-prime for bias+ trials and bias- trials
corr = NaN(length(f_smt_diff),1);
corr(f_smt_diff>0) = true; % goes up = true
corr(f_smt_diff<0) = false; % goes down = false
% skip missed trials
% stbias(isnan(resp)) = [];
stbias_short(isnan(resp)) = [];
corr(isnan(resp)) = [];
% shorten the iti array also
iti_short(isnan(resp)) = [];
resp(isnan(resp)) = [];
% remove trials that are bias0 -- the span of the two tones in a trial
% includes the average of the previous trial
% corr(isnan(stbias)) = [];
% resp(isnan(stbias)) = [];
corr(isnan(stbias_short)) = [];
resp(isnan(stbias_short)) = [];
iti_short(isnan(stbias_short)) = [];
stbias_short(isnan(stbias_short)) = [];

% bias+ performance
dpr = NaN(length(iti_set),2);
propcorr = NaN(length(iti_set),2);
ntr = NaN(length(iti_set),2);
% separate short-term bias arrays into separate ITIs
stbias_sep = NaN(length(iti)/length(iti_set)-1,length(iti_set));
    % subtract 1 because we will skip the first trial (had ITI=5)
for ii = 1:length(iti_set)
    % % get number of short-term bias+ and bias- trials
    idx_bp = stbias_short==1&iti_short==iti_set(ii);
    idx_bm = stbias_short==2&iti_short==iti_set(ii);
    ntr(ii,1) = sum(idx_bp);
    ntr(ii,2) = sum(idx_bm);
    
    % calculate d-prime
    dpr(ii,1) = calc_dpr(corr(idx_bp),resp(idx_bp));
    dpr(ii,2) = calc_dpr(corr(idx_bm),resp(idx_bm));

    % proportion correct for bias+ and bias-
    propcorr(ii,1) = sum(corr(idx_bp)==resp(idx_bp))/ntr(ii,1);
    propcorr(ii,2) = sum(corr(idx_bm)==resp(idx_bm))/ntr(ii,2);
    
    % Get the short term bias for the trials with this ITI (all trials,
    % without excluding bias0 or no response)
    idx = iti==iti_set(ii);
    stbias_sep(:,ii) = stbias(idx);
end