function rt = get_fd_rt(sbj,behavpth,behavsuffix)
% Load and return the response times in the 2s/2s experiment

if nargin<3
    % by default, use the behavioral file labeled "_1" at the end
    behavsuffix = '2';
end

exp = 'FD_jitter';
bhv_fl = sprintf('%s_%s_%s',exp,sbj,behavsuffix);

% Load the data
d = load([behavpth bhv_fl]);

% Get the total number of trials
rt = d.rt;