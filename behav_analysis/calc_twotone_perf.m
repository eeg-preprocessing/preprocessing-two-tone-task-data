function [ncorr,nincorr,nmiss,tottr] = calc_twotone_perf(sbj,behavpth,behavsuffix)
% Calculate the performance for a single subject in the 2s/2s experiment

if nargin<3
    % by default, use the behavioral file labeled "_1" at the end
    behavsuffix = '2';
end

exp = 'FD_jitter';
bhv_fl = sprintf('%s_%s_%s',exp,sbj,behavsuffix);

% Load the data
d = load([behavpth bhv_fl]);

% Get the total number of trials
tottr = length(d.acc);

% Get the number of correct and incorrect responses
ncorr = sum(d.acc,'omitnan');
nincorr = sum(d.acc==0);

% Display the number of misses
nmiss = sum(isnan(d.acc));