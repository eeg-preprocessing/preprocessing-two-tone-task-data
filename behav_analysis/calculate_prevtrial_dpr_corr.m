function [dpr,propcorr,ntr,iti_order] = calculate_prevtrial_dpr_corr(sbj,behavpth,behavsuffix,exp)
% Calculate the short-term difference in d' difference conditioned on
% correct or incorrect trials
% Each of the outputs have 4 values:
% - 1) Bias+ & previous correct response
% - 2) Bias- & previous correct response
% - 3) Bias+ & previous incorrect response
% - 4) Bias- & previous incorrect response
% (Copied from erp-adaptation 2s/2s experiment, modified to suit FD_jitter)
% Nate Zuk (2022)

if nargin<3 || isempty(behavsuffix)
    % by default, use the behavioral file labeled "_1" at the end
    behavsuffix = '2';
end

if nargin<4
    exp = 'FD_jitter';
end
bhv_fl = sprintf('%s_%s_%s',exp,sbj,behavsuffix);

% Load the data
d = load([behavpth bhv_fl]);
% convert responses from strings to 1 = f1>f2, 2 = f1<f2
resp = NaN(length(d.s1),1);
for n = 1:length(resp)
    if strcmp(d.resp{n},'1'), resp(n) = false;
    elseif strcmp(d.resp{n},'2'), resp(n) = true;
    else
        resp(n) = NaN;
    end
end

% Compute the semitone difference between tones
f_smt_diff = (log2(d.s2)-log2(d.s1))*12;

% Compute the trial means
f_tr_mean = (log2(d.s2)+log2(d.s1))/2;

% Identify the short-term bias on each trial
% bias+ = 1: tone direction is away from the mean of the previous trial
% bias- = 2: tone direction is towards the mean of the previous trial
stbias = NaN(length(d.s1),1);
for n = 2:length(d.s1)
    if log2(d.s1(n)) > f_tr_mean(n-1) && log2(d.s2(n)) > f_tr_mean(n-1) % if above tones in previous trial
        if f_smt_diff(n)>0, stbias(n) = 1; % tones go up - bias+
        elseif f_smt_diff(n)<0, stbias(n) = 2; % tones go down - bias-
        end
    elseif log2(d.s1(n)) < f_tr_mean(n-1) && log2(d.s2(n)) < f_tr_mean(n-1) % if below tones in previous trial
        if f_smt_diff(n)<0, stbias(n) = 1; % tones go down - bias+
        elseif f_smt_diff(n)>0, stbias(n) = 2; % tones go up - bias-
        end
    end
end

% Get the ITIs for each trial
iti = d.ITI;
[iti_set,iti_idx] = unique(iti);
% skip ITI==5, which corresponds to the start of a trial
iti_idx(iti_set==5.0) = [];
iti_set(iti_set==5.0) = [];

% Get the iti order, based on the number of trials per set
ntrials_per_block = length(iti)/length(iti_set);
iti_order = floor(iti_idx/ntrials_per_block)+1;

% Identify the correct answer (up or down)
corr = NaN(length(f_smt_diff),1);
corr(f_smt_diff>0) = true; % goes up = true
corr(f_smt_diff<0) = false; % goes down = false

prev_acc = d.acc; % get if the subject was correct or incorrect on each trial
% now include all t-1 trials in prev_corr and current trials in corr and
% resp
prev_acc = reshape(prev_acc,[ntrials_per_block length(iti_set)]);
prev_acc = prev_acc(1:end-1,:);
% rearrange back to a column array
prev_acc = reshape(prev_acc,[(ntrials_per_block-1)*length(iti_set) 1]);
% remove the first index of each block in corr and resp, which does not have a previous trial
corr = rmv_first_per_block(corr,ntrials_per_block);
% corr = reshape(corr,[ntrials_per_block length(iti_set)]);
% corr = corr(2:end,:);
% corr = reshape(corr,[(ntrials_per_block-1)*length(itis_set) 1]);
resp = rmv_first_per_block(resp,ntrials_per_block);
% resp = reshape(resp,[ntrials_per_block length(iti_set)]);
% resp = resp(2:end,:);
% resp = reshape(resp,[(ntrials_per_block-1)*length(itis_set) 1]);
% setup stbias_short, which will be used to store short-term biases for
% only trials included in analysis
stbias_short = rmv_first_per_block(stbias,ntrials_per_block);
% stbias_short = reshape(stbias,[ntrials_per_block length(iti_set)]);
% stbias_short = stbias_short(2:end,:);
% stbias_short = reshape(stbias_short,[(ntrials_per_block-1)*length(itis_set) 1]);
iti_short = rmv_first_per_block(iti,ntrials_per_block);

% remove nans (ignore missed trials)
prev_acc(isnan(resp)) = [];
stbias_short(isnan(resp)) = [];
iti_short(isnan(resp)) = [];
corr(isnan(resp)) = [];
resp(isnan(resp)) = [];
% remove trials that are bias0 -- the span of the two tones in a trial
% includes the average of the previous trial
% corr(isnan(stbias)) = [];
% resp(isnan(stbias)) = [];
prev_acc(isnan(stbias_short)) = [];
corr(isnan(stbias_short)) = [];
resp(isnan(stbias_short)) = [];
iti_short(isnan(stbias_short)) = [];
stbias_short(isnan(stbias_short)) = [];

% get number of short-term bias+ and bias- trials
ntr = NaN(4,length(iti_set));
dpr = NaN(4,length(iti_set));
propcorr = NaN(4,length(iti_set));

for ii = 1:length(iti_set)
    ntr(1,ii) = sum(stbias_short==1&prev_acc==1&iti_short==iti_set(ii)); % short-term bias correct
    ntr(2,ii) = sum(stbias_short==2&prev_acc==1&iti_short==iti_set(ii));
    ntr(3,ii) = sum(stbias_short==1&prev_acc==0&iti_short==iti_set(ii)); % short-term bias incorrect
    ntr(4,ii) = sum(stbias_short==2&prev_acc==0&iti_short==iti_set(ii));

    % bias+ performance (conditioned on correct/incorrect in previous trial)
    dpr(1,ii) = calc_dpr(corr(stbias_short==1&prev_acc==1&iti_short==iti_set(ii)),...
        resp(stbias_short==1&prev_acc==1&iti_short==iti_set(ii)));
    dpr(2,ii) = calc_dpr(corr(stbias_short==2&prev_acc==1&iti_short==iti_set(ii)),...
        resp(stbias_short==2&prev_acc==1&iti_short==iti_set(ii)));
    dpr(3,ii) = calc_dpr(corr(stbias_short==1&prev_acc==0&iti_short==iti_set(ii)),...
        resp(stbias_short==1&prev_acc==0&iti_short==iti_set(ii)));
    dpr(4,ii) = calc_dpr(corr(stbias_short==2&prev_acc==0&iti_short==iti_set(ii)),...
        resp(stbias_short==2&prev_acc==0&iti_short==iti_set(ii)));

    % proportion correct for bias+ and bias- (conditioned on
    % correct/incorrect in current trial)
    propcorr(1,ii) = sum(corr(stbias_short==1&prev_acc==1&iti_short==iti_set(ii))==...
        resp(stbias_short==1&prev_acc==1&iti_short==iti_set(ii)))/ntr(1,ii);
    propcorr(2,ii) = sum(corr(stbias_short==2&prev_acc==1&iti_short==iti_set(ii))==...
        resp(stbias_short==2&prev_acc==1&iti_short==iti_set(ii)))/ntr(2,ii);
    propcorr(3,ii) = sum(corr(stbias_short==1&prev_acc==0&iti_short==iti_set(ii))==...
        resp(stbias_short==1&prev_acc==0&iti_short==iti_set(ii)))/ntr(3,ii);
    propcorr(4,ii) = sum(corr(stbias_short==2&prev_acc==0&iti_short==iti_set(ii))==...
        resp(stbias_short==2&prev_acc==0&iti_short==iti_set(ii)))/ntr(4,ii);
end

function y = rmv_first_per_block(x,ntr_per_block)
% remove the index of hte first trial in each block for column array x
y = reshape(x,[ntr_per_block length(x)/ntr_per_block]);
y = y(2:end,:);
y = reshape(y,[numel(y) 1]);