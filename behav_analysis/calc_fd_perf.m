function [ncorr,nincorr,nmiss,tottr,acc] = calc_fd_perf(sbj,behavpth,behavsuffix)
% Calculate the performance for a single subject in the FD_jitter experiment

if nargin<3
    % the active condition is saved with _2 at the end
    behavsuffix = '2';
end

exp = 'FD_jitter';
bhv_fl = sprintf('%s_%s_%s',exp,sbj,behavsuffix);

% Load the data
d = load([behavpth bhv_fl]);
acc = d.acc; % store correct/incorrect responses

% Get the total number of trials
tottr = length(d.acc);

% Get the number of correct and incorrect responses
ncorr = sum(d.acc,'omitnan');
nincorr = sum(d.acc==0);

% Display the number of misses
nmiss = sum(isnan(d.acc));