function plot_erps_by_exp(erps,dly,ch_idx,iti_idx,cond_labels,ch_labels,...
    iti_values,exp_labels,fig_position)
% Plot individual and average ERPs for each set of session in an
% experiment. This is intended to compare ERPs between generations of
% Shahaf's two-tone experiment. Each channel and ITI is plotted in its own
% figure. Conditions and experiments are plotted in a ncond x nexp subplot in each
% figure.
% Inputs:
% - erps = a cell array of dimensions: conditions x experiments. Each
%       cell contains a 4D array with dimensions: delay x channels x ITI x
%       session
% - dly = array of ERP delays
% - ch_idx = set of indexes for the channels that should be plotted
% - iti_idx = set of indexes for the ITIs that should be plotted
% - cond_labels = labels for each condition (cell array)
% - ch_labels = labels for each channel (cell array)
% - iti_values = values for each ITI (numeric array)
% - exp_labels = labels for each experiment (cell array)
% - fig_position (optional) = size of the figure, in pixels
% Nate Zuk (2021)

if nargin<9 || isempty(fig_position)
    fig_position = '';
end

ncond = length(cond_labels);
nexp = length(exp_labels);

% setup the color map
try
    cmap = colormap('turbo');
catch err
    cmap = colormap('jet');
end
for n = 1:length(iti_idx)
    for c = 1:length(ch_idx)
        % Plot for this pair of channel x iti
        figure;
        if ~isempty(fig_position)
            % specify the figure size
            set(gcf,'Position',[(c + (n-1)*length(iti_idx))*100 0 fig_position]);
        end
        for jj = 1:nexp
            for ii = 1:ncond
                % identify the correct subplot to use
%                 sbplt_idx = (ii-1)*ncond+jj;
                sbplt_idx = (ii-1)*nexp+jj;
                subplot(ncond,nexp,sbplt_idx);
                hold on;
                % get the ERPs for this specific condition, experiment,
                % channel, and ITI
                sp_erp = squeeze(erps{ii,jj}(:,ch_idx(c),iti_idx(n),:));
                % plot the ERP for this specific channel and ITI, color
                % code by sessions
                nsbj = size(sp_erp,2); % get the number of sessions
                for s = 1:nsbj
                    % plot each subject as a different color
                    clr_idx = get_color_idx(s,nsbj,cmap);
                    plot(dly,sp_erp(:,s),'Color',cmap(clr_idx,:),'LineWidth',1.5);
                end
                % Plot the average of the ERPs across sessions
                plot(dly,mean(sp_erp,2),'k','LineWidth',4);
                set(gca,'FontSize',14,'YLim',[-6 8]);
                % Labeling
                xlabel('Time (ms)');
                ylabel('Average ERP (\muV)');
                % Create the title
                tle = sprintf('%s, %s; %s, %.1f s ITI',exp_labels{jj},cond_labels{ii},...
                    ch_labels{c},iti_values(n));
                title(tle,'Interpreter','none');
            end
        end
    end
end