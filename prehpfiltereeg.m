function flteeg = prehpfiltereeg(eeg,Fs,varargin)
% Filter the eeg using a zero-phase highpass butterworth filter 

N = 4; % order of the butterworth filter
Fc_low = 0.05; % 3 dB lower cutoff frequency of the butterworth filter

if ~isempty(varargin),
    for n = 2:2:length(varargin),
        eval([varargin{n-1} '=varargin{n};']);
    end
end

% Generate bandpass filter
b = fdesign.highpass('N,F3dB',N,Fc_low,Fs);
bpf = design(b,'butter');

flteeg = filtfilthd(bpf,eeg);