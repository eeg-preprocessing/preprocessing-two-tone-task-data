% After applying preprocessing (referencing, baseline) save all EEG
% segments following triggers.

% preprocpth = '/Volumes/NO NAME/PreprocessedEEG/';
preprocpth = 'A:\PreprocessedEEG\';
% preprocpth = 'exmp_preproc_data/';
sv_path = 'A:\AllSegs\';
% sv_path = '/Volumes/NO NAME/AllSegs/';
exppth = 'Shahaf2017/';
sbj = 'RORU8269';
exp_conditions = {'passive','active'};
% exp_conditions = {'passive'};
sound_delay = 60; % amount of time between the trigger and the stimulus (in ms)
erp_range = [-100 1200]; % range of delays to use to get the ERP (in ms)
baseline = [-100 0]; % range of delays to use as the baseline
itis = [2 3.5 6.5 9.5]; % ITIs for each condition (the floor of these values is
% itis = [2 9.5];
    % the second number in each trigger value)
% Apply reference (can select either nose or mastoids)
reference = 'mastoids';

fig_path = 'fig/';

% for each condition
% ERPs = cell(length(exp_conditions),1);
calc_iti = NaN(length(itis));
std_iti = NaN(length(itis));
for ii = 1:length(exp_conditions)
    if (any(strcmp(sbj,{'AVNE2973','OSMA7669','SHAB5396','RORU8269'})) ...
            && strcmp(exp_conditions{ii},'active')) || ...
            (strcmp(sbj,'NASH7723') && strcmp(exp_conditions{ii},'passive'))
         % for these subjects, the active condition was recorded in two bdfs
         % for one subject, the passive condition was recorded in two bdfs
        preproc_fl_i = sprintf('%s_%s',sbj,exp_conditions{ii});
        d_i = load([preprocpth exppth preproc_fl_i]);
        disp(preproc_fl_i);
        preproc_fl_ii = sprintf('%s_%s_2',sbj,exp_conditions{ii});
        d_ii = load([preprocpth exppth preproc_fl_ii]);
        disp(preproc_fl_ii);
        % concatenate all of the data
        eeg = [d_i.eeg; d_ii.eeg];
        eFs = d_i.eFs;
        nose = [d_i.nose; d_ii.nose];
        mastoids = [d_i.mastoids; d_ii.mastoids];
        trigs = [d_i.trigs; d_ii.trigs];
        % get the channel labels
        chan_lbls = d_i.chan_lbls;
    else
        % Load the preprocessed data
        preproc_fl = sprintf('%s_%s',sbj,exp_conditions{ii});
        d = load([preprocpth exppth preproc_fl]);
        disp(preproc_fl);
        % Get data
        eeg = d.eeg;
        eFs = d.eFs;
        nose = d.nose;
        mastoids = d.mastoids;
        trigs = d.trigs;
        % Get the channel labels
        chan_lbls = d.chan_lbls;
    end
    % retain only the trigger onsets
    difftrigs = [0; diff(trigs)];
    trigs(difftrigs<=0) = 0;
    % Get the block onsets
    block_onsets = trigs==35;
    block_idx = find(block_onsets);
    % Setup the stimulus matrix (each row is a different condition)
    stim = zeros(length(trigs),length(itis));
    for jj = 1:length(itis)
        % check if the second digit of the trigger matches the floor(iti)
        trig_onsets = find(mod(trigs,10)==floor(itis(jj)));
        % find the block trigger just before the first ITI trigger
        block_distances = abs(block_idx-trig_onsets(1));
        closest_block = find(block_distances==min(block_distances));
        % store the index for the block start, but offset by sound_delay
        sound_delay_idx = round(sound_delay/1000*eFs);
        stim(block_idx(closest_block) + sound_delay_idx,jj) = 1;
        % store all other trial start indexes
        stim(trig_onsets + sound_delay_idx,jj) = 1;
        [calc_iti(jj,ii),std_iti(jj,ii)] = calculate_iti(stim(:,jj),eFs);
    end
    % Remove the reference
    veeg = var(eeg);
    fprintf('EEG channel variance: %.3f [%.3f %.3f]\n',...
        median(veeg),quantile(veeg,0.25),quantile(veeg,0.75));
    if strcmp(reference,'nose')
        fprintf('Nose variance: %.3f\n',var(nose));
        reeg = eeg - nose*ones(1,size(eeg,2));
    elseif strcmp(reference,'mastoids')
        fprintf('Mastoids variance: %.3f, %.3f\n',var(mastoids(:,1)),var(mastoids(:,2)));
        use_mastoids = var(mastoids)<3*quantile(veeg,0.75);
        if any(use_mastoids==0)
            warning('Mastoid channel %d was rejected',find(use_mastoids==0));
        end
        reeg = eeg - mean(mastoids(:,use_mastoids),2)*ones(1,size(eeg,2));
    end
    % Get the segments following each event
    [erp,dly] = get_all_erps(stim,reeg,d.eFs,erp_range(1),erp_range(2),...
        baseline);
    % Save the segments
    sv_fl = sprintf('%s_ref-%s-%s',sbj,reference,exp_conditions{ii});
    save([sv_path exppth sv_fl],'erp','dly','sound_delay',...
        'baseline','itis','calc_iti','std_iti','chan_lbls');
end

%% Functions %%
function [iti,st] = calculate_iti(stim,fs)
% Calculate the ITI from a vector of trial onsets
rw = find(stim);
iti = mean(diff(rw)/fs);
st = std(diff(rw)/fs);
end