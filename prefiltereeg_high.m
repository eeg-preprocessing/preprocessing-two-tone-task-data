function flteeg = prefiltereeg_high(eeg,Fs,varargin)
% Filter the eeg using a zero-phase lowpass butterworth filter 
% Setup for Prob_sequence

N = 4; % order of the butterworth filter
Fc_low = 3; % 3 dB lower cutoff frequency of the butterworth filter
Fc_high = 90; % 3 dB upper cutoff frequency of the butterworth filter
Fcnt_notch = 50; % use a brick filter to remove this notch
Q = 2.5; % quality factor of the notch filter

if ~isempty(varargin),
    for n = 2:2:length(varargin),
        eval([varargin{n-1} '=varargin{n};']);
    end
end

% Generate bandpass filter
b = fdesign.bandpass('N,F3dB1,F3dB2',N,Fc_low,Fc_high,Fs);
bpf = design(b,'butter');

flteeg = filtfilthd(bpf,eeg);

% Generate and apply the notch filter
nch = fdesign.notch('N,F0,Q',N,Fcnt_notch,Q,Fs);
nchf = design(nch,'butter');
flteeg = filtfilthd(nchf,flteeg);