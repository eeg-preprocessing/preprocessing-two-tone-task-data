% Preprocessing script that is similar to
% PreprocessBDFFile_fasticaeyeblinkrmv.m, except that the data is filtered
% from 1-30 Hz for CNV analysis in the 2s/2s experiment. The same spatial
% topographies for eyeblinks and eye movements calculated from the 1-30 Hz
% filtered data are re-applied here.
% (26-9-2022, This way the same eyeblink removal is performed separately
% and can be applied to both low frequency and mid-frequency preprocessing)
% Nate Zuk (2021)

% Using fieldtrip for data loading and some analysis
U = userpath;
addpath([U '\fieldtrip-20210614\']);
% Add NoiseTools
addpath([U '\NoiseTools\']);
% To create documents with the figures
addpath([U '\export_fig\']);
addpath([U '\FastICA_25\']);
% addpath('../preprocessing/');

% Parameters relating to the organization of the stored EEG data
nchan = 32;
trigger_channel = 48;

% Directory for storing figures
fig_path = 'fig/';

% Directory for storing preprocessing files
preprocpth = 'A:\PreprocessedEEG\';

%% Load the bdf file
eegpth = 'A:\RawEEG\';
exppth = 'Shahaf2017/';
sbj = 'ZUBE4020';
condition = 'active'; % typically 1 is for the passive condition, 2 is for active
if strcmp(condition,'passive')
    eegfnm = sprintf('%s_1.bdf',sbj);
elseif strcmp(condition,'active')
    eegfnm = sprintf('%s_2.bdf',sbj);
else
    % for these subjects, the active condition appears to be split into two bdf files: 2
    % and 3
    if any(strcmp(sbj,{'AVNE2973','OSMA7669','SHAB5396'})) ...
            && strcmp(condition,'active_2')
        eegfnm = sprintf('%s_3.bdf',sbj);
    elseif strcmp(sbj,'NASH7723') && strcmp(condition,'passive_2')
        eegfnm = sprintf('%s_1_2.bdf',sbj);
    elseif strcmp(sbj,'RORU8269') && strcmp(condition,'active_2')
        eegfnm = sprintf('%s_2-2.bdf',sbj);
    else
        error('condition must be passive or active');
    end
end

disp('Loading EEG...');
cfg = [];
cfg.dataset = [eegpth exppth eegfnm];
data_eeg = ft_preprocessing(cfg);

% load the eeg data
eeg = data_eeg.trial{1}(1:nchan,1:end)';

% get the triggers
trigs = data_eeg.trial{1}(trigger_channel,1:end)';
% subtract the minimum trigger value, corresponding to 0
trigs = trigs-min(trigs);
% get the triggers modulo 256 (because sometimes the trigger indexes are
% offset by 65536, especially when the recording is restarted)
trigs = mod(trigs,256);

nose = get_labeled_channel(data_eeg,{'Nose'});
% save the EOG channels separately
eog = get_labeled_channel(data_eeg,{'EOGu','EOGd'});
% save the HR/HL channels separately (left-right, azimuthal eye movements)
eyeazim = get_labeled_channel(data_eeg,{'HL','HR'});
% save the mastoid channels separately
mastoids = get_labeled_channel(data_eeg,{'M1','M2'});

% get the sampling rate
eFs = data_eeg.fsample;
% save the channel labels
chan_lbls = data_eeg.label(1:nchan);

clear data_eeg % remove the data structure, to save space

%% Linear detrending
%%% Use robust linear detrending (use NoiseTools: de Cheveigne & Arzounian),
%%% apply to each channel individually
disp('Linear detrending...');
eeg = nt_detrend(eeg,1);
nose = nt_detrend(nose,1);
eog = nt_detrend(eog,1);
eyeazim = nt_detrend(eyeazim,1);
mastoids = nt_detrend(mastoids,1);

%% Filtering
disp('Filtering between 1-30 Hz with 4th order butterworth...');
eeg = prefiltereeg(eeg,eFs); eeg = rmfltartifact(eeg,eFs);
nose = prefiltereeg(nose,eFs); nose = rmfltartifact(nose,eFs);
eog = prefiltereeg(eog,eFs); eog = rmfltartifact(eog,eFs);
eyeazim = prefiltereeg(eyeazim,eFs); eyeazim = rmfltartifact(eyeazim,eFs);
mastoids = prefiltereeg(mastoids,eFs); mastoids = rmfltartifact(mastoids,eFs);

%% Variance calculation
%%% (Amos's recommendation) Save the channel variance before removing
%%% components
vEEG = var(eeg);
[spCZ,frq] = pwelch(eeg(:,32),2*eFs,[],[],eFs);

%%% Make an example plot of an eyeblink before and after removal (use Fp1)
fp = eeg(:,1);
% get an eyeblink based on the presence of sparse voltages
eyeblink_thres = quantile(fp,0.993);
% for each triggered eyeblink, get the peak voltage within a 500 ms window
% around the peak
[eyeblink,eyeblink_idx] = get_triggered_eyeblinks(fp,eyeblink_thres,eFs);

%% Eyeblink removal using previously calculated components
% Load the components calculated from ICA
disp('Loading ICA components and removing eyeblinks...');
highfreq_fl = sprintf('%s_%s_eyeblinkrmv',sbj,condition);
preproc = load([preprocpth exppth highfreq_fl]);
% get the separation and mixing matrices
W = preproc.W;
A = preproc.A;
rmv_cmps = preproc.rmv_cmps;
% compute the independent components
icasig = W*[eeg, eog, eyeazim, nose, mastoids]';

% Remove the eye movement components
eeg = eeg - (A(1:32,rmv_cmps)*icasig(rmv_cmps,:))';
eog = eog - (A(33:34,rmv_cmps)*icasig(rmv_cmps,:))';
eyeazim = eyeazim - (A(35:36,rmv_cmps)*icasig(rmv_cmps,:))';
nose = nose - (A(37,rmv_cmps)*icasig(rmv_cmps,:))';
mastoids = mastoids - (A(38:39,rmv_cmps)*icasig(rmv_cmps,:))';

% get the indexes originally with eyeblinks after eyeblink removal
rmv_eyeblink = NaN(size(eyeblink));
for n = 1:size(eyeblink,2)
    use_idx = ~isnan(eyeblink_idx(:,n));
    rmv_eyeblink(use_idx,n) = eeg(eyeblink_idx(use_idx,n),1);
end

% Check power after eyeblink removal
vEEG_afterrmv = var(eeg);
spCZ_afterrmv = pwelch(eeg(:,32),2*eFs,[],[],eFs);

%% Plotting
fig_fl = sprintf('%s_%s_preproc_midfreq.pdf',sbj,condition);
% Plot fp1 before and after eyeblink removal
dly = -ceil(0.25*eFs):ceil(0.25*eFs);
figure
hold on
plot(dly,eyeblink,'k');
plot(dly,rmv_eyeblink,'r');
set(gca,'FontSize',14);
xlabel('Delay (ms)');
ylabel('\muV');
title('FP1, before and after eyeblink removal');
export_fig([fig_path exppth fig_fl]);

% Plot the variance of the electrodes before and after removing EOG / HR /
% HL electrodes
figure
hold on
plot(vEEG,'k');
plot(vEEG_afterrmv,'b');
set(gca,'FontSize',12,'XTick',1:nchan,'XTickLabel',chan_lbls,...
    'XTickLabelRotation',90);
ylabel('Channel variance (\muV^2)');
legend('Original','After EOG removal');
export_fig([fig_path exppth fig_fl],'-append');

% plot the spectra
figure
hold on
plot(frq,spCZ,'k');
plot(frq,spCZ_afterrmv,'b');
set(gca,'FontSize',14,'XScale','log','YScale','log');
ylabel('Power spectrum of Cz');
legend('Original','After EOG removal','Location','southwest');
export_fig([fig_path exppth fig_fl],'-append');

%% Save the eeg data and the triggers
svfnm = sprintf('%s_%s_midfreq',sbj,condition);
save([preprocpth exppth svfnm],'eeg','eFs','trigs','nose','mastoids','eog','eyeazim',...
    'vEEG','spCZ','chan_lbls');

%% ** Functions **
function chan = get_labeled_channel(data_eeg,labels)
% For a cell array of strings containing channel labels, get those channels
% from the data structure
chan_idx = NaN(length(labels),1);
for n = 1:length(labels), chan_idx(n) = find(strcmp(data_eeg.label,labels{n})); end
chan = data_eeg.trial{1}(chan_idx,1:end)';
end