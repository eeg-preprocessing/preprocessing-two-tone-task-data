% Load the subject-averaged DSS components (from PCA, see
% AllSbjDSScmpTopos.m), find the best-fit of the individual-subject
% components to the subject-average components, and plot the ERPs as a
% function of ITI

% To save figures to a pdf
% addpath('~/Documents/Matlab/export_fig/');
% addpath('~/Documents/Matlab/fieldtrip-20200607/');
addpath('..');
addpath('C:\Users\zsxbo\Documents\Matlab\export_fig\');
addpath('C:\Users\zsxbo\Documents\Matlab\fieldtrip-20210614\');

preprocpth = 'A:/PreprocessedEEG/';
% preprocpth = '/Volumes/NO NAME/PreprocessedEEG/';
dsspth = 'A:/DSS/';
% dsspth = '/Volumes/NO NAME/DSS/';
exppth = 'Shahaf2017/';
exp_conditions = {'passive','active'};
nchan = 32; % number of channels in the EEG
sound_delay = 50;
erp_range = [-100 2000]; % range of delays to use to get the ERP (in ms)
baseline = [-100 0]; % range of delays to use as the baseline
itis = [2 3.5 6.5 9.5]; % ITIs for each condition (the floor of these values is
%     % the second number in each trigger value)
% Apply reference (can select either nose or mastoids)
reference = 'mastoids';
ndss_to_use = 3; % number of DSS components to use

% Load the subject-averaged DSS
sbjavg_fl = sprintf('AllSbjDSS_%s',exppth(1:end-1));
sbjavg = load(sbjavg_fl);
sbjs = sbjavg.sbjs;
dss_fls = sbjavg.dss_fls;

% for each condition
dss_topos = cell(length(exp_conditions),1);
for ii = 1:length(exp_conditions)
    % Preallocate the arrays
    dss_topos{ii} = NaN(nchan,nchan,length(sbjs));
    for s = 1:length(sbjs)
        % load the DSS info
        dss_info = load([dsspth exppth dss_fls{ii}{s}]);
        dss_topos{ii}(:,:,s) = dss_info.toeeg;
        % show the file has been loaded
        disp(dss_fls{ii}{s})
    end
end

% find the best fit combination of subject DSS topos to match the
% subject-averaged topos
dss_matched = cell(length(exp_conditions),1);
for ii = 1:length(exp_conditions)
    if strcmp(exp_conditions{ii},'passive')
        cf = sbjavg.cf_pas;
    elseif strcmp(exp_conditions{ii},'active')
        cf = sbjavg.cf_act;
    else
        error('Unknown experiment condition');
    end
    % Preallocate
    dss_matched{ii} = NaN(nchan,ndss_to_use,length(sbjs));
    for s = 1:length(sbjs)
        % Get only individual-subject DSS components that pass the AUC
        % threshold
        dss_idx = find(sbjavg.pass_thres{ii}(:,s));
        indiv_cmps = dss_topos{ii}(dss_idx,:,s)';
        % Find a linear combination of topos that best matches the
        % subject-average
        for n = 1:ndss_to_use
            X = [ones(nchan,1) indiv_cmps];
            % invert cross-subject weights if Cz<0
            cf(:,n) = cf(:,n)*sign(cf(32,n));
            b = X \ cf(:,n);
            dss_matched{ii}(:,n,s) = X*b;
        end
    end
end

%% Compute the ERP using the DSS components relative to the subject average
disp('Computing the ERPs and transforming to DSS...');
DSS = cell(length(itis),length(exp_conditions),length(sbjs));
calc_iti = NaN(length(itis),length(exp_conditions),length(sbjs));
std_iti = NaN(length(itis),length(exp_conditions),length(sbjs));
for ii = 1:length(exp_conditions)
    for s = 1:length(sbjs)
        % Load preprocessing data
        if (any(strcmp(sbjs{s},{'AVNE2973','OSMA7669','SHAB5396'})) ...
                && strcmp(exp_conditions{ii},'active')) || ...
                (strcmp(sbjs{s},'NASH7723') && strcmp(exp_conditions{ii},'passive'))
             % for these subjects, the active condition was recorded in two bdfs
             % for one subject, the passive condition was recorded in two bdfs
            preproc_fl_i = sprintf('%s_%s',sbjs{s},exp_conditions{ii});
            d_i = load([preprocpth exppth preproc_fl_i]);
            disp(preproc_fl_i);
            preproc_fl_ii = sprintf('%s_%s_2',sbjs{s},exp_conditions{ii});
            d_ii = load([preprocpth exppth preproc_fl_ii]);
            disp(preproc_fl_ii);
            % concatenate all of the data
            eeg = [d_i.eeg; d_ii.eeg];
            eFs = d_i.eFs;
            nose = [d_i.nose; d_ii.nose];
            mastoids = [d_i.mastoids; d_ii.mastoids];
            trigs = [d_i.trigs; d_ii.trigs];
        else
            % Load the preprocessed data
            preproc_fl = sprintf('%s_%s',sbjs{s},exp_conditions{ii});
            d = load([preprocpth exppth preproc_fl]);
            disp(preproc_fl);
            % Get data
            eeg = d.eeg;
            eFs = d.eFs;
            nose = d.nose;
            mastoids = d.mastoids;
            trigs = d.trigs;
        end
        % retain only the trigger onsets
        difftrigs = [0; diff(trigs)];
        trigs(difftrigs<=0) = 0;
        % Get the block onsets
        block_onsets = trigs==35;
        block_idx = find(block_onsets);
        % Setup the stimulus matrix (each row is a different condition)
        stim = zeros(length(trigs),length(itis));
        for jj = 1:length(itis)
            % check if the second digit of the trigger matches the floor(iti)
            trig_onsets = find(mod(trigs,10)==floor(itis(jj)));
            % find the block trigger just before the first ITI trigger
            block_distances = abs(block_idx-trig_onsets(1));
            closest_block = find(block_distances==min(block_distances));
            % store the index for the block start, but offset by sound_delay
            sound_delay_idx = round(sound_delay/1000*eFs);
            stim(block_idx(closest_block) + sound_delay_idx,jj) = 1;
            % store all other trial start indexes
            stim(trig_onsets + sound_delay_idx,jj) = 1;
            [calc_iti(jj,ii,s),std_iti(jj,ii,s)] = calculate_iti(stim(:,jj),eFs);
        end
        % Remove the reference
        veeg = var(eeg);
        fprintf('EEG channel variance: %.3f [%.3f %.3f]\n',...
            median(veeg),quantile(veeg,0.25),quantile(veeg,0.75));
        if strcmp(reference,'nose')
            fprintf('Nose variance: %.3f\n',var(nose));
            reeg = eeg - nose*ones(1,size(eeg,2));
        elseif strcmp(reference,'mastoids')
            fprintf('Mastoids variance: %.3f, %.3f\n',var(mastoids(:,1)),var(mastoids(:,2)));
            use_mastoids = var(mastoids)<3*quantile(veeg,0.75);
            if any(use_mastoids==0)
                warning('Mastoid channel %d was rejected',find(use_mastoids==0));
            end
            reeg = eeg - mean(mastoids(:,use_mastoids),2)*ones(1,size(eeg,2));
        end
        % Calculate the ERP
        [erp,dly] = get_all_erps(stim,reeg,d.eFs,erp_range(1),erp_range(2),...
            baseline);
        % Transform into DSS components
        for jj = 1:length(itis)
            ntr = size(erp{jj},3);
            DSS{jj,ii,s} = NaN(length(dly),length(ndss_to_use),size(erp{jj},3));
            for n = 1:ntr
                for m = 1:ndss_to_use
                    DSS{jj,ii,s}(:,m,n) = erp{jj}(:,:,n)*pinv(dss_matched{ii}(:,m,s)');
                end
            end
        end
    end
end

% Compute the DSS components averaged across trials
allsbjdss = NaN(length(dly),ndss_to_use,length(itis),length(sbjs),length(exp_conditions));
for ii = 1:length(exp_conditions)
    for s = 1:length(sbjs)
        for jj = 1:length(itis)
            %allsbjdss(:,:,jj,s,ii) = median(DSS{jj,ii,s},3);
            % use spline-transformed median instead (ds=3 implements upper
            % cutoff of 30 Hz, see SplineSpecEffect.m)
            allsbjdss(:,:,jj,s,ii) = spline_median(DSS{jj,ii,s},3,3);
        end
    end
end

% Compute correlation between ITI and z-scored component for each
% delay
iti_corr = NaN(length(dly),ndss_to_use,length(exp_conditions));
for ii = 1:length(exp_conditions)
    for m = 1:ndss_to_use
        for n = 1:length(dly)
            dss_vals = reshape(zscore(squeeze(allsbjdss(n,m,:,:,ii))),[length(itis)*length(sbjs) 1]);
            iti_rep = repmat(itis',length(sbjs),1);
            iti_corr(n,m,ii) = corr(iti_rep,dss_vals,'type','Spearman');
        end
    end
end

%% Plot the subject-averaged DSS components
% Plot all matched dss components
cfg = [];
cfg.layout = 'biosemi32.lay';
cfg.baselinetype = 'absolute';
layout = ft_prepare_layout(cfg);
for ii = 1:length(exp_conditions)
    figure
    for m = 1:ndss_to_use
        for s = 1:length(sbjs)
            subplot(ndss_to_use,length(sbjs),(m-1)*length(sbjs)+s);
            ft_plot_topo(layout.pos(1:nchan,1),layout.pos(1:nchan,2),dss_matched{ii}(:,m,s),...
                'mask',layout.mask,'outline',layout.outline,'interplim','mask');
        end
    end
end

iti_lgd = cell(length(itis),1);
for ii = 1:length(itis), iti_lgd{ii} = sprintf('%.2g s ITI',itis(ii)); end

figure
erp_pl = NaN(length(itis),1);
for ii = 1:length(exp_conditions)
    for m = 1:ndss_to_use
        subplot(length(exp_conditions),ndss_to_use,(ii-1)*ndss_to_use+m);
        plot(dly,squeeze(median(allsbjdss(:,m,:,:,ii),4)),'LineWidth',2);
        set(gca,'FontSize',14,'XLim',erp_range);
        xlabel('Delay (ms)');
        ylabel('ERP (A.U.)');
        title(sprintf('DSS %d, %s',m,exp_conditions{ii}));
    end
end

%% Functions %%
function [iti,st] = calculate_iti(stim,fs)
% Calculate the ITI from a vector of trial onsets
rw = find(stim);
iti = mean(diff(rw)/fs);
st = std(diff(rw)/fs);
end