% Compare the DSS topographies within and across subjects

% To save figures to a pdf
% addpath('~/Documents/Matlab/export_fig/');
% addpath('~/Documents/Matlab/fieldtrip-20200607/');
addpath('..');
addpath('C:\Users\natha\Documents\Matlab\export_fig\');
addpath('C:\Users\natha\Documents\Matlab\fieldtrip-20210614\');

dsspth = 'A:/DSS/';
% dsspth = '/Volumes/NO NAME/DSS/';
exppth = 'Shahaf2017_ASD/';
exp_conditions = {'passive','active'};
nchan = 32; % number of channels in the EEG
% erp_range = [-100 1200]; % range of delays to use to get the ERP (in ms)
% baseline = [-100 0]; % range of delays to use as the baseline
% itis = [2 3.5 6.5 9.5]; % ITIs for each condition (the floor of these values is
%     % the second number in each trigger value)
% Apply reference (can select either nose or mastoids)
reference = 'mastoids';
niter = 100; % number of iterations for getting the CV and null power ratios for DSS
auc_thres = 0.95;

% Get the files for DSS
suffix = sprintf('ref-%s_DSS',reference);
[dss_fls{1},sbjs] = get_res_filenames([dsspth exppth],exp_conditions{1},suffix);
[dss_fls{2},~] = get_res_filenames([dsspth exppth],exp_conditions{2},suffix);

% for each condition
dss_topos = cell(length(exp_conditions),1);
cv_rat = cell(length(exp_conditions),1);
nl_rat = cell(length(exp_conditions),1);
for ii = 1:length(exp_conditions)
    % Preallocate the arrays
    dss_topos{ii} = NaN(nchan,nchan,length(sbjs));
    cv_rat{ii} = NaN(niter,nchan,length(sbjs));
    nl_rat{ii} = NaN(niter,nchan,length(sbjs));
    for s = 1:length(sbjs)
        % load the DSS info
        dss_info = load([dsspth exppth dss_fls{ii}{s}]);
        dss_topos{ii}(:,:,s) = dss_info.toeeg;
        cv_rat{ii}(:,:,s) = dss_info.cv_pwr_rat';
        nl_rat{ii}(:,:,s) = dss_info.null_pwr_rat';
        % show the file has been loaded
        disp(dss_fls{ii}{s})
    end
end

% For each subject and DSS component, compute the AUC distance between the
% CV power ratios and the null power ratios
auc_rat = cell(length(exp_conditions),1);
pass_thres = cell(length(exp_conditions),1);
for ii = 1:length(exp_conditions)
    auc_rat{ii} = NaN(nchan,length(sbjs));
    pass_thres{ii} = false(nchan,length(sbjs));
    for s = 1:length(sbjs)
        for n = 1:nchan
            [~,~,~,auc_rat{ii}(n,s)] = perfcurve([ones(niter,1); zeros(niter,1)],...
                [cv_rat{ii}(:,n,s); nl_rat{ii}(:,n,s)],1);
            % Flag AUC that are greater than the threshold
            if auc_rat{ii}(n,s) > auc_thres
                pass_thres{ii}(n,s) = true;
            end
        end
    end
end

% For each topo matrix, identify if the passive and active DSS topographies are
% correlated within subjects (but only DSS that pass the AUC threshold)
disp('Examining correlation between passive and active components within subject...');
topo_pas_act_corr = NaN(nchan,nchan,length(sbjs));
allcorrs = NaN(nchan*nchan,length(sbjs));
figure
for s = 1:length(sbjs)
    passive_cmp = find(pass_thres{1}(:,s));
    active_cmp = find(pass_thres{2}(:,s));
%     topo_pas_act_corr(passive_cmp,active_cmp,s) = corr(dss_topos{1}(passive_cmp,:,s)',dss_topos{2}(active_cmp,:,s)');
    topo_pas_act_corr(:,:,s) = corr(dss_topos{1}(:,:,s)',dss_topos{2}(:,:,s)');
    allcorrs(:,s) = reshape(topo_pas_act_corr(:,:,s),[nchan*nchan 1]);
    % Plot the correlations
    nplcols = ceil(sqrt(length(sbjs)));
    nplrows = floor(sqrt(length(sbjs)));
    subplot(nplrows,nplcols,s);
    imagesc(topo_pas_act_corr(passive_cmp,active_cmp,s)');
    caxis([-1 1]);
    colorbar;
    xlabel('Passive DSS');
    ylabel('Active DSS');
end
% Plot the distribution of all correlations for comparison
figure
ALLCORRS = reshape(allcorrs,[numel(allcorrs),1]);
histogram(abs(ALLCORRS),100,'FaceColor',[0 0 0]);
set(gca,'FontSize',14,'XLim',[0 1]);
xlabel('Correlation');
ylabel('# passive-active DSS pairs');
title('Passive-active correlations with all components');

% Run PCA on all topographies for each condition to identify common
% components
disp('PCA on all subject components...');
allpassive_cmps = [];
allactive_cmps = [];
for s = 1:length(sbjs)
    passive_cmp = find(pass_thres{1}(:,s));
    % Positively weight Cz
    pas_topo = dss_topos{1}(passive_cmp,:,s)';
    allpassive_cmps = [allpassive_cmps pas_topo.*(ones(size(pas_topo,1),1)*sign(pas_topo(32,:)))];
    active_cmp = find(pass_thres{2}(:,s));
    act_topo = dss_topos{1}(active_cmp,:,s)';
    allactive_cmps = [allactive_cmps act_topo.*(ones(size(act_topo,1),1)*sign(act_topo(32,:)))];
end
[cf_pas,sc_pas,~,~,vexp_pas] = pca(allpassive_cmps');
[cf_act,sc_act,~,~,vexp_act] = pca(allactive_cmps');
figure
hold on
plot(cumsum(vexp_pas));
plot(cumsum(vexp_act));
set(gca,'FontSize',14);
xlabel('Cross-subject component');
ylabel('Variance explained');
legend('Passive','Active');
% Plot the first 5 common components
cfg = [];
cfg.layout = 'biosemi32.lay';
cfg.baselinetype = 'absolute';
layout = ft_prepare_layout(cfg);
figure
ncmps_to_plot = 5;
for n = 1:ncmps_to_plot
    subplot(2,ncmps_to_plot,n);
    ft_plot_topo(layout.pos(1:nchan,1),layout.pos(1:nchan,2),cf_pas(:,n),...
        'mask',layout.mask,'outline',layout.outline,'interplim','mask');
    colorbar;
    set(gca,'FontSize',12);
    title(sprintf('Passive DSS Comp %d',n));
end
for n = 1:ncmps_to_plot
    subplot(2,ncmps_to_plot,ncmps_to_plot+n);
    ft_plot_topo(layout.pos(1:nchan,1),layout.pos(1:nchan,2),cf_act(:,n),...
        'mask',layout.mask,'outline',layout.outline,'interplim','mask');
    colorbar;
    set(gca,'FontSize',12);
    title(sprintf('Active DSS Comp %d',n));
end

% Plot the correlation between passive and active components
[c,p] = corr(cf_pas,cf_act);
figure
subplot(2,1,1);
imagesc(c');
colorbar;
xlabel('Passive DSS component');
ylabel('Active DSS component');
subplot(2,1,2);
imagesc(p');
colorbar;
xlabel('Passive DSS component');
ylabel('Active DSS component');

%% Save the PCA and AUC results
% Use the experiment path to get the file suffix
sv_fl = sprintf('AllSbjDSS_%s',exppth(1:end-1));
save(sv_fl,'sbjs','dss_fls','auc_thres','auc_rat','pass_thres',...
    'cf_pas','sc_pas','vexp_pas','cf_act','sc_act','vexp_act');