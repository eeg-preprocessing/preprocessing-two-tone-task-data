function md = spline_median(x,ds,dim)
% Calculate the median across the dimension specified, after computing the
% B-spline coefficients approximating a smoother representation of the data
% along the first dimension of x (for example, smoothing along delays). Up
% to 4 dimensions are allowed

ndly = size(x,1); % compute the number of sample points along the dimension
% calculate the spline transformation matrices
spline_matrix = spline_transform(1:ndly,ds);
spline_inv = (spline_matrix'*spline_matrix)^(-1)*spline_matrix';

size_x = size(x,1:4); % get the size of all dimensions of x
size_splx = size_x; % get the dimensions for x, the first dimension size will be changed
size_splx(1) = size(spline_matrix,2); % reduce the size of the delay dimension
splx = NaN(size_splx);

% Transform to spline coefficients
for kk = 1:size_splx(4)
    for jj = 1:size_splx(3)
        for ii = 1:size_splx(2)
            splx(:,ii,jj,kk) = spline_inv*x(:,ii,jj,kk);
        end
    end
end

% Compute the median across the desired dimension (skip NaNs)
splmd = median(splx,dim,'omitnan');

% Transform back to delays
dim_idx = setxor(1:4,dim);
md = NaN(size_x(dim_idx));
for jj = 1:size_x(dim_idx(3))
    for ii = 1:size_x(dim_idx(2))
        md(:,ii,jj) = spline_matrix*splmd(:,ii,jj);
    end
end