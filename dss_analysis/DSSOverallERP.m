% Compute the overall ERP for a vary ITI experiment, and run joint
% decorrelation (DSS) to identify the component(s) that best represent the
% evoked responses.
% Nate Zuk (2021)

% To save figures to a pdf
% addpath('~/Documents/Matlab/export_fig/');
% addpath('~/Documents/Matlab/NoiseTools/');
% addpath('~/Documents/Matlab/fieldtrip-20200607/');
addpath('..');
addpath('C:\Users\zsxbo\Documents\Matlab\export_fig\');
addpath('C:\Users\zsxbo\Documents\Matlab\NoiseTools\');
addpath('C:\Users\zsxbo\Documents\Matlab\fieldtrip-20210614\');

% preprocpth = '/Volumes/NO NAME/PreprocessedEEG/';
preprocpth = 'A:/PreprocessedEEG_low/';
% sv_path = '/Volumes/NO NAME/DSS/';
sv_path = 'A:/DSS_low/';
sbj = 'ITKI6815';
exppth = 'Shahaf2017/';
exp_conditions = 'active';
sound_delay = 50; % amount of time between the trigger and the stimulus (in ms)
erp_range = [-100 1200]; % range of delays to use to get the ERP (in ms)
baseline = [];
nfake = 1000;
itis = [2 3.5 6.5 9.5]; % ITIs for each condition (the floor of these values is
    % the second number in each trigger value)
% Apply mastoids reference
reference = 'mastoids';
ndss_to_plot = 3; % number of DSS components to plot

fig_path = 'fig/';

% Load the preprocessed EEG file
if (any(strcmp(sbj,{'AVNE2973','OSMA7669','SHAB5396'})) ...
        && strcmp(exp_conditions,'active')) || ...
        (strcmp(sbj,'NASH7723') && strcmp(exp_conditions,'passive'))
     % for these subjects, the active condition was recorded in two bdfs
     % for one subject, the passive condition was recorded in two bdfs
    preproc_fl_i = sprintf('%s_%s',sbj,exp_conditions);
    d_i = load([preprocpth exppth preproc_fl_i]);
    disp(preproc_fl_i);
    preproc_fl_ii = sprintf('%s_%s_2',sbj,exp_conditions);
    d_ii = load([preprocpth exppth preproc_fl_ii]);
    disp(preproc_fl_ii);
    % concatenate all of the data
    eeg = [d_i.eeg; d_ii.eeg];
    eFs = d_i.eFs;
    nose = [d_i.nose; d_ii.nose];
    mastoids = [d_i.mastoids; d_ii.mastoids];
    trigs = [d_i.trigs; d_ii.trigs];
else
    % Load the preprocessed data
    preproc_fl = sprintf('%s_%s',sbj,exp_conditions);
    d = load([preprocpth exppth preproc_fl]);
    disp(preproc_fl);
    % Get data
    eeg = d.eeg;
    eFs = d.eFs;
    nose = d.nose;
    mastoids = d.mastoids;
    trigs = d.trigs;
end
% retain only the trigger onsets
difftrigs = [0; diff(trigs)];
trigs(difftrigs<=0) = 0;
% Get the block onsets
% block_onsets = trigs==35;
% block_idx = find(block_onsets);
% Setup the stimulus matrix (each row is a different condition)
stim = zeros(length(trigs),1);
% get any triggers that mark the start of the trial
% 35 trigger for the start of the block, or ending with 2, 3, 6, 9 for each
% ITI
trig_onsets = find(any(mod(trigs,10)==floor(itis),2) | trigs==35);
% store the index for the block start, but offset by sound_delay
sound_delay_idx = round(sound_delay/1000*eFs);
stim(trig_onsets + sound_delay_idx) = 1;
% Remove the reference
veeg = var(eeg);
fprintf('EEG channel variance: %.3f [%.3f %.3f]\n',...
    median(veeg),quantile(veeg,0.25),quantile(veeg,0.75));
fprintf('Mastoids variance: %.3f, %.3f\n',var(mastoids(:,1)),var(mastoids(:,2)));
use_mastoids = var(mastoids)<3*quantile(veeg,0.75);
if any(use_mastoids==0)
    warning('Mastoid channel %d was rejected',find(use_mastoids==0));
end
reeg = eeg - mean(mastoids(:,use_mastoids),2)*ones(1,size(eeg,2));
% Calculate the ERP
% [ERPs,dly] = compute_erp(stim,reeg,eFs,erp_range(1),erp_range(2),...
%     baseline);
% Call all ERPs by trial
[all_erps,dly] = get_all_erps(stim,reeg,d.eFs,erp_range(1),erp_range(2),...
    baseline);% Create a set of fake erps
null_erp = fake_erps(stim,reeg,d.eFs,erp_range(1),erp_range(2),...
    baseline,nfake);

%% Do joint decorrelation
disp('Run DSS...');
[todss,pwr0,pwr1] = nt_dss1(all_erps{1});
% Convert the ERPs to DSS components
DSSERP = NaN(size(all_erps{1}));
ntr = size(all_erps{1},3);
ndss = size(todss,2);
for n = 1:ntr
    DSSERP(:,:,n) = all_erps{1}(:,:,n)*todss;
end
% Convert null ERPs
NERP = NaN(size(null_erp{1}));
for n = 1:nfake
    NERP(:,:,n) = null_erp{1}(:,:,n)*todss;
end

disp('Cross-validate DSS...');
niter = 100;
cv_pwr_rat = crossval_dss_ratio(all_erps{1},niter);

% Compute the null distribution of power ratios
disp('Compute the null distribution of power ratios...');
null_pwr_rat = null_dss_ratio(stim,reeg,d.eFs,erp_range,baseline);

%% Calculate the ratio of the average N1 to the average of the noise
dly_idx = dly>=0 & dly<=250;
n1 = squeeze(min(DSSERP(dly_idx,1,:)));
fn1 = squeeze(min(NERP(dly_idx,1,:)));
fprintf('DSS1: Mean N1 to noise std = %.3f, ',mean(n1)/std(fn1));
fprintf('Noise N1 to noise std = %.3f\n',mean(fn1)/std(fn1));
pk = squeeze(max(DSSERP(dly_idx,1,:))) - squeeze(min(DSSERP(dly_idx,1,:)));
fpk = squeeze(max(NERP(dly_idx,1,:))) - squeeze(min(NERP(dly_idx,1,:)));
fprintf('DSS1: Mean peak-to-peak to noise std = %.3f, ',mean(pk)/std(fpk));
fprintf('Noise peak-to-peak to noise std = %.3f\n',mean(fpk)/std(fpk));

%% Plotting
fig_fl = sprintf('%s_%s_ref-%s_DSS.pdf',sbj,exp_conditions,reference);

% Plot the power ratio of DSS
figure
hold on
% plot(pwr1./pwr0,'bo-','LineWidth',1.5);
plot(median(null_pwr_rat,2),'k');
plot(quantile(null_pwr_rat,0.95,2),'k--');
plot(quantile(null_pwr_rat,0.05,2),'k--');
plot(median(cv_pwr_rat,2),'b','LineWidth',2);
plot(quantile(cv_pwr_rat,0.95,2),'b--','LineWidth',2);
plot(quantile(cv_pwr_rat,0.05,2),'b--','LineWidth',2);
set(gca,'FontSize',14);
xlabel('DSS component');
ylabel('Power ratio');
export_fig([fig_path exppth fig_fl]);

% Plot the AUC for each component
auc_thres = 0.95;
nchan = size(reeg,2);
auc = NaN(nchan,1);
for n = 1:nchan
    [~,~,~,auc(n)] = perfcurve([ones(niter,1); zeros(niter,1)],...
         [cv_pwr_rat(n,:)'; null_pwr_rat(n,:)'],1);
end
figure
hold on
plot(1:nchan,auc,'b','LineWidth',2);
plot([1 nchan],[auc_thres auc_thres],'k--');
set(gca,'FontSize',14);
xlabel('DSS component');
ylabel('Area under ROC curve (AUC)');

% Plot the ERP at Cz
figure
set(gcf,'Position',[550,13,400,1000]);
for jj = 1:ndss_to_plot
    subplot(ndss_to_plot,1,jj);
%     set(gcf,'Position',[200 1 450 350]);
%     plot(dly,squeeze(ERPs(:,chan,:)),'LineWidth',2);
    plot(dly,median(DSSERP(:,jj,:),3),'LineWidth',2);
    set(gca,'FontSize',12);
    xlabel('Delay (ms)');
    ylabel('Average ERP (\muV)');
    title(sprintf('DSS Component %d, %s',jj,exp_conditions));
end
export_fig([fig_path exppth fig_fl],'-append');

% Plot an image of the relationship between DSS components and channels
toeeg = pinv(todss);
% nchan = size(toeeg,2);
figure
imagesc(toeeg);
colorbar;
set(gca,'FontSize',12,'XTick',1:nchan,'XTickLabel',d.chan_lbls,'YTick',1:ndss);
xlabel('Channel');
ylabel('DSS compoent');

% Plot the topographies of these components
cfg = [];
cfg.layout = 'biosemi32.lay';
cfg.baselinetype = 'absolute';
layout = ft_prepare_layout(cfg);
figure
set(gcf,'Position',[650,13,400,1000]);
for n = 1:ndss_to_plot
    subplot(ndss_to_plot,1,n);
    ft_plot_topo(layout.pos(1:nchan,1),layout.pos(1:nchan,2),toeeg(n,:),...
        'mask',layout.mask,'outline',layout.outline,'interplim','mask');
    colorbar;
    set(gca,'FontSize',12);
    title(sprintf('DSS Comp %d',n));
end
export_fig([fig_path exppth fig_fl],'-append');

%% Save the dss components
sv_fl = sprintf('%s_%s_ref-%s_DSS',sbj,exp_conditions,reference);
save([sv_path exppth sv_fl],'todss','toeeg','pwr0','pwr1','cv_pwr_rat','null_pwr_rat',...
    'itis','sound_delay','erp_range','baseline');

%% Functions %%
function null_pwr_rat = null_dss_ratio(stim,eeg,Fs,erp_range,baseline)
% Compute a null set of DSS power ratios by randomly circularly shifting
% the EEG array relative to the stimulus
nnull = 100; % number of iterations for the null distribution
null_pwr_rat = NaN(size(eeg,2),nnull);
for n = 1:nnull
    % Randomly circularly shift the stimulus
    shft_eeg = circshift(eeg,randi(length(stim)));
    % Compute null erps
    null_erps = get_all_erps(stim,shft_eeg,Fs,erp_range(1),erp_range(2),...
        baseline);
    % Run DSS
%     [~,pwr0,pwr1] = nt_dss1(null_erps{1});
    % Compute the power ratio
%     null_pwr_rat(:,n) = pwr1./pwr0;
    null_pwr_rat(:,n) = crossval_dss_ratio(null_erps{1},1);
end

end

function pwr_rat = crossval_dss_ratio(erps,nreps)
% Compute the power ratio by leaving out 20% of the trials, doing DSS on
% the rest, and computing the power ratio on the left-out trials
pwr_rat = NaN(size(erps,2),nreps);
prop_test = 0.2; % proprtion of trials for testing
ntr = size(erps,3);
for n = 1:nreps
    % get the testing data
    test_trs = randperm(ntr,round(prop_test*ntr));
    train_trs = setxor(1:ntr,test_trs);
    % do DSS
    todss = nt_dss1(erps(:,:,train_trs));
    % compute the power ratio on the left-out data
    [C_ub,nC_ub] = nt_cov(erps(:,:,test_trs));
    C_ub = C_ub/nC_ub; % unbiased covariance
    pwr0=sqrt(sum((C_ub'*todss).^2)); % unbiased
    [C_b,nC_b] = nt_cov(mean(erps(:,:,test_trs),3));
    C_b = C_b/nC_b; % unbiased covariance
    pwr1=sqrt(sum((C_b'*todss).^2)); % biased
    pwr_rat(:,n) = pwr1./pwr0;
end

end