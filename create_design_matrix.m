function X = create_design_matrix(x,dlyidx,tmidx,varargin)
% Generate a cell array of design matrices, where each cell corresponds to
% a cell in the data provided in x.  The design matrix consists of lagged
% version of each column (parameter) in x. Columns are ordered in groups of
% lagged versions of each column of x (aka. [x1lags x2lags etc...]).
% Inputs:
% - x = cell array of data that should be used to create the lagged design
% matrices
% - dlyidx = delays to use in the design matrix, in indexes (used by
% lagGen)
% - tmidx = a cell array of time indexes that should be included in each
% design matrix (default, use all times)
% - (Optional) column_center = flag to zero-center columns (default: true)
% - (Optional) row_center = flag to zero-center rows (default: true)
% Nate Zuk (2020)

column_center = true; % zero-center each column of the design matrix
row_center = false; % zero-center each row of the design matrix

if nargin < 3 || isempty(tmidx) % if idx isn't specified...
    tmidx = {}; % set an empty cell
end

if ~isempty(varargin)
    for n = 2:2:length(varargin)
        eval([varargin{n-1} '=varargin{n};']);
    end
end

ntr = length(x);
nparam = size(x{1},2);

X = cell(ntr,1);
for jj = 1:ntr
    % create the design matrix
    Xc = lagGen(x{jj},dlyidx);
    if ~isempty(tmidx)
        Xc = Xc(tmidx{jj},:); % get only the indexes of interest
    end
    % zero-center the columns of the design matrix (this adjustment doesn't
    % change the zeros of each row very much
    if column_center, Xc = Xc - ones(size(Xc,1),1)*mean(Xc,1); end
    % zero-center the lagged EEG at each time point. Center each channel
    % separately
    if row_center
        for kk = 1:nparam
            param_idx = (1:length(dlyidx))+length(dlyidx)*(kk-1);
            Xc(:,param_idx) = Xc(:,param_idx) - mean(Xc(:,param_idx),2)*ones(1,length(dlyidx));
        end
    end
    % place the design matrix for the specific trial in the full design
    % matrix
    X{jj} = Xc; % use cells, because compute_linreg_matrices assumes inputs are cells anyway
end