function plot_values_by_exp(val,val_label,cond_labels,ch_labels,exp_labels,fig_size,...
    title_append,session_labels)
% Plot values (derived from ERPs) as a function of experiment session, but
% separated by different experiments. This is particularly to plot
% different generations of Shahaf's two-tone experiment on one axis. Each
% experiment condition is plotted in a separate subplot, and each channel
% is plotted is a separate figure.
% Inputs:
% - val = a cell array containing 3-D arrays. Each cell corresponds to a
%       different experiment generation. The arrays in each cell are: session x
%       condition x channel
% - val_label = the y-axis label for each plot
% - cond_labels = labels for each condition (cell array)
% - ch_labels = labels for each channel (cell array)
% - exp_labels = labels for each generation of the experiment (cell array)
% - fig_size (optional) = specifies the size of each figure, in pixels
%       (default: use Matlab's default figure size)
% - title_append (optional) = appends all titles with this string 
%       (default: blank)
% - session_labels (optional) = cell array containing cells of session
%       labels (will be placed for each point on x-axis)
% Nate Zuk (2021)

nexp = length(exp_labels);
ncond = length(cond_labels);
nch = length(ch_labels);

if nargin<6 || isempty(fig_size)
    fig_size = [];
end

if nargin<7 || isempty(title_append)
    title_append = '';
end

if nargin<8 || isempty(session_labels)
    session_labels = {};
end

%% Get the x-axis values for each session
% Identify the number of sessions per experiment
xidx = cell(nexp,1);
tot_sessions = 0;
for s = 1:nexp
    xidx{s} = (1:size(val{s},1))' + tot_sessions;
    % increment tot_sessions by the number of sessions in this experiment
    tot_sessions = tot_sessions + size(val{s},1);
end

%% Plotting
for jj = 1:nch
    figure; % make a new figure for each 
    if ~isempty(fig_size) % if a figure size is specified, use that size
        set(gcf,'Position',[jj*100 0 fig_size]);
    end
    for ii = 1:ncond
        subplot(ncond,1,ii); % plot each condition as a separate subplot
        hold on
        % Plot the values
        for s = 1:nexp
            plot(xidx{s},val{s}(:,ii,jj),'k','LineWidth',2);
        end
        set(gca,'FontSize',12);
        if ~isempty(session_labels)
            all_xidx = cell2mat(xidx);
            all_session_lbls = cat(1,session_labels{:});
            set(gca,'XTick',all_xidx,'XTickLabel',all_session_lbls,...
                'XTickLabelRotation',45);
        else
            % Label the experiments at the first index for each group of
            % sessions
    %         lbl_xidx = cumsum([0; cellfun(@(x) length(x),xidx)]);
            exp_L = cellfun(@(x) length(x),xidx);
            cnt = cumsum([0; exp_L(1:end-1)]) + exp_L/2 + 0.5;
            % center of each group of sessions for each experiment
            set(gca,'XTick',cnt,'XTickLabel',exp_labels);
        end
        xlabel('Session');
        ylabel(val_label);
        % Show the title (channel, condition)
        tle = sprintf('%s, %s',ch_labels{jj},cond_labels{ii});
        if ~isempty(title_append)
            tle = [tle ', ' title_append];
        end
        title(tle);
    end
end