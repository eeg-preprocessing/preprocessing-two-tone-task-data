% For each time point in the ERP, fit a model predicting the previous ITI
% based on a linear weighting of all channels. In contrast with
% PredITIbyTime.m, this procedure iteratively leaves out a random selection
% of 1/10th of the trials for testing.

addpath(genpath('C:\Users\natha\Projects\EEG-analysis-tools'));
U = userpath;
addpath([U '\fieldtrip-20210614\']);
addpath([U '\shadedErrorBar\']);
% addpath('~/Documents/MATLAB/shadedErrorBar');
% addpath('model_fitting');
% addpath('..');

preprocpth = 'A:/PreprocessedEEG_low/';
exp_pth = 'Shahaf2017/';
sbj = 'ITKI6815';
exp_condition = 'active';
itis = [2 3.5 6.5 9.5];
erp_range = [-500 1500];
baseline = [-500 0];
reference = 'mastoids';
sound_delay = 50;

%% ERP using 0.05 filter

lowfreq_fl = sprintf('%s_%s_lowfreq',sbj,exp_condition);

% Identify the channel to plot
chan_to_plot = {'Cz','Fz','Pz'};

% Calculate the ERPs
all_erps_low = cell(length(itis),1);
itis_lbl = cell(length(itis),1);
for n = 1:length(itis)
    triggers = [1 2]*10 + floor(itis(n)); % skips the first trial
    [all_erps_low(n),dly,~,chan_lbls] = calc_all_erps([preprocpth exp_pth lowfreq_fl],triggers,erp_range,...
        baseline,reference,sound_delay);
    itis_lbl{n} = sprintf('%.1f s ITI',itis(n));
end

%%% Transform into spatio-temporal arrays
ntr = [0; cumsum(cellfun(@(x) size(x,3),all_erps_low))];
ndim = length(dly)*length(chan_lbls);
E = NaN(ndim,ntr(end));
for n = 1:length(itis)
    % get the indexes where these trials should go in E
    idx = (ntr(n)+1):ntr(n+1);
    E(:,idx) = reshape(all_erps_low{n},ndim,ntr(n+1)-ntr(n));
end

%%% fit a multiclass linear discriminant (assumes gaussian distributions
%%% for each ITI and calculates means)
lbl = repelem((1:4)',100,1); % 100 = # trials per block after the first one
mdl = fitcdiscr(E',lbl);
cvmodel = crossval(mdl);
L = kfoldloss(cvmodel);

% number of eeg channels
nchan = size(erps,2);
niter = 100; % number of iterations of bootstrap per time point
nnull = 100; % number of iterations to get the null distribution

%% Model fitting
disp('Started model fitting...');
mdl_tm = tic;
% compute the window for averaging voltage values
wnd_size = 50; % size of the window to average the response, in ms
wnd = -ceil(wnd_size/2000*eFs):ceil(wnd_size/2000*eFs);
% Fit a linear model to each time point that predicts the previous ITI
b = NaN(length(dly),nchan+1,niter);
rsq = NaN(length(dly),niter);
null_rsq = NaN(length(dly),nnull);
% for t = 1:length(dly)
for t = (wnd(end)+1):(length(dly)+wnd(1)-1)
    fprintf('t = %.3f ms\n',dly(t));
    % get the voltages at a particular time point, and reorient by trial x
    % channels
%     erp_tm = squeeze(erps(t,:,:))';
    % average over the window
    erp_tm = squeeze(mean(erps(t+wnd,:,:)))';
%     erp_tm = squeeze(sc(t,:,:));
    % skip the first trial (no ITI before it)
    erp_tm = erp_tm(2:end,:);
    % remove events that occur at the start of blocks (after long itis)
    erp_tm = erp_tm(use_iti,:);
    % zero-center each channel (we expect a change in voltage with ITI)
%     rsq(t) = 1 - sse/sst;
    %%% for ZUNA1973, fitting to iti or log(iti) produced a nearly
    %%% identical result
    [rsq(t,:),b(t,:,:)] = lin_mdl_bootstrap(erp_tm,iti,niter);
    % Compute the null distribution at this time point
    null_rsq(t,:) = lin_mdl_nulldistr(erp_tm,iti,nnull);
%     for n = 1:nnull
%         shuf_iti = iti(randperm(nevt));
%         [~,null_rsq(t,n)] = lin_mdl_fit(erp_tm,shuf_iti);
%     end
end
fprintf('Completed @ %.3f s\n',toc(mdl_tm));

%% Plotting
% Plot the null distribution of r-sq values and the true values
plot_lin_mdl_perf(dly,rsq,null_rsq);
xlabel('Delay (ms)');

% Plot average r-squared relative to null distribution in z-values
figure
set(gcf,'Position',[15,345,1000,350]);
hold on
z_rsq = (mean(rsq,2)-mean(null_rsq,2))./std(null_rsq,[],2);
plot([dly(1) dly(end)],[0 0],'k--');
plot(dly,z_rsq,'k','LineWidth',2);
set(gca,'FontSize',14);
xlabel('Delay (ms)');
ylabel('Z-scored R^2 (std rel null)');

% Plot the topography of the EEG at the range of peak R-squared values
pk_range = [250 350];
pk_idx = dly>=pk_range(1) & dly<=pk_range(2);
figure
set(gcf,'Position',[360,360,420,320]);
% Plot the topography of the correlations between ITI and average magnitude
% for each channel
chan_avg_mag = squeeze(mean(erps(pk_idx,:,:),1))';
chan_avg_mag = chan_avg_mag(2:end,:);
chan_avg_mag = chan_avg_mag(use_iti,:);
cr = corr(iti,chan_avg_mag);
% topoplot(cr,'chanlocs_32.xyz');
% replaced with fieldtrip
cfg = [];
cfg.layout = 'biosemi32.lay';
cfg.baselinetype = 'absolute';
layout = ft_prepare_layout(cfg);
ft_plot_topo(layout.pos(1:nchan,1),layout.pos(1:nchan,2),cr,...
    'mask',layout.mask,'outline',layout.outline,'interplim','mask');
colorbar;
set(gca,'FontSize',14);
% title(sbj);

% Plot ITI vs EEG magnitude for Pz between 250-350 ms
% chan_pz = 32; % channel index for Pz
% pz_mags = chan_avg_mag(:,chan_pz);
% figure
% set(gcf,'Position',[280,100,400,360]);
% plot(iti,pz_mags,'k.','MarkerSize',14);
% set(gca,'FontSize',14)
% xlabel('Time between first tone onsets (s)');
% ylabel(sprintf('Pz magnitude, averaged between %d-%d ms',pk_range(1),pk_range(2)));