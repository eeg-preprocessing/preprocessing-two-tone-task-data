function dates = load_session_dates(sbjs)
% For a list of subjects, load the exp_dates.csv file, and get the dates
% when those subject's sessions were run.
% Nate Zuk (2021)

dates_fl = 'exp_dates.csv';

%% Open the file
fid = fopen(dates_fl,'r');

% Get the list of subjects and dates
headers = fgetl(fid); % the first line contains headers

all_sbjs = {}; all_dates = {};
while 1
    % get the next line of data
    rw = fgetl(fid);
    % check if it's the end of the file
    if ~ischar(rw) || contains(rw,'END OF FILE'), break, end
    % find the comma separator
    comma_idx = strfind(rw,',');
    % the string before the comma is the subject ID
    % (remove whitespace in the process)
    all_sbjs = [all_sbjs; {strtrim(rw(1:comma_idx-1))}];
    % the string after the comma is the date
    d_str = strtrim(rw(comma_idx+1:end));
    all_dates = [all_dates; datetime(d_str,'Format','dd-MM-yyyy')];
end
fclose(fid);

%% Get the correct dates
% go through each subject in the input cell array, and find the associated
% date
dates = NaT(length(sbjs),1);
for s = 1:length(sbjs)
    % find the index for this subject in the list of all subjects from the
    % dates_fl
    sbj_idx = strcmp(all_sbjs,sbjs{s});
    % retrieve the date at this index
    dates(s) = all_dates(sbj_idx);
end