% Use principal components to analyze the ERPs for shahaf's data
% Nate Zuk (2021)

% Necessary paths
addpath('..');
addpath('../..');
addpath('../behav_analysis/');
addpath(genpath('C:\Users\natha\Projects\EEG-analysis-tools'));
U = userpath;
addpath([U '\fieldtrip-20210614\']);

% Parameters
sbj = 'MOFU8212';
exp_condition = 'active';
eFs = 256; % sampling rate of EEG, needed for cwt
sound_delay = 50; % amount of time between the trigger and the stimulus (in ms)
isi = 600; % inter-stimulus interval in a trial (in ms)
erp_range = [-500 3000]; % range of delays to use to get the ERP (in ms)
% baseline = [];
baseline = [-500 0]; % range of delays to use as the baseline
% Apply mastoids reference
reference = 'mastoids';
% Preprocessing path
preprocpth = 'A:\PreprocessedEEG_low\Shahaf2017\';
% itis = [2 3.5 6.5 9.5]; % inter-trial-onset intervals in each block

%% Subject performance
behavpth = 'A:\Behavior\Shahaf2017\';
[ncorr,~,nmiss,tottr,acc] = calc_fd_perf(sbj,behavpth);

% Display the percent correct
propc = ncorr/tottr;
fprintf('Performance: %.1f%% correct\n',propc*100);

% Display the number of misses
fprintf('%d / %d misses\n',nmiss,tottr);

% Calculate d-prime difference between short-term bias+ and bias-
% conditions
[dpr,propcorr,~,itis,stbias,iti_order,f_smt_diff,f_tr_mean] = calculate_prevtrial_dpr_bias(sbj,behavpth);
disp('D-prime difference between short-term bias+/bias-:');
for n = 1:length(itis)
    fprintf('%.1f s ITI: %.3f\n',itis(n),dpr(n,1)-dpr(n,2));
end
disp('Prop. corr. difference between short-term bias+/bias-:');
for n = 1:length(itis)
    fprintf('%.1fs ITI: %.3f\n',itis(n),propcorr(n,1)-propcorr(n,2));
end

% Show the median response time
rt = get_fd_rt(sbj,behavpth);
% rearrange the reaction time by iti
rt_iti = reshape(rt,[length(rt)/length(itis) length(itis)]);
rt_iti_ordered = rt_iti(:,iti_order);
for n = 1:length(itis)
    fprintf('Response time, %.1f s ITI: %.1f ms\n',itis(n),median(rt_iti_ordered(:,n),'omitnan')-sound_delay);
end

% Rearrange the accuracy array so columns are ordered by iti
ntrials_per_block = 101;
acc = reshape(acc,[length(acc)/length(itis) length(itis)]);
acc = acc(:,iti_order); % rearrange by itis, and remove the first row (first trial, before inter-trial intervals)

%% ERP using 0.05-30 Hz filter

lowfreq_fl = sprintf('%s_%s_lowfreq',sbj,exp_condition);

% Identify the channel to plot
pcs_to_plot = {'PC1','PC2','PC3'};
pc_lbls = cell(32,1);
for c = 1:32
    pc_lbls{c} = sprintf('PC%d',c);
end

% First, get the overall ERP (all triggers)
[all_erps_low,dly,~,chan_lbls] = calc_all_erps([preprocpth lowfreq_fl],[12 13 16 19 22 23 26 29],...
    erp_range,baseline,reference,sound_delay);
% calculate the median
mderp = spline_median(all_erps_low{1},6,3);
% use cross-validation to estimate the number of components that should be
% used
[vexp_cv,vexp_null] = cv_pca(mderp);
% plot the amount of variance explained by each components relative to
% chance
figure
plot(cumsum(vexp_cv)-mean(cumsum(vexp_null),2));
set(gca,'FontSize',14);
xlabel('Principal component');
ylabel('Proportion variance explained more than chance');
% get the PC transformation
cf = pca(mderp);

% Plot the topography of the first three components
figure
nchan = 32;
cfg = [];
cfg.layout = 'biosemi32.lay';
cfg.baselinetype = 'absolute';
layout = ft_prepare_layout(cfg);
for ii = 1:length(pcs_to_plot)
    subplot(ceil(length(pcs_to_plot)/2),2,ii);
    ft_plot_topo(layout.pos(1:nchan,1),layout.pos(1:nchan,2),cf(:,ii),...
        'mask',layout.mask,'outline',layout.outline,'interplim','mask');
    colorbar;
    title(pcs_to_plot{ii});
end
saveas(gcf,sprintf('%s_%s_pctopos.png',sbj,exp_condition));
    
% Calculate the ERPs
pc_erps_low = cell(length(itis),1);
itis_lbl = cell(length(itis),1);
for n = 1:length(itis)
    triggers = [1 2]*10 + floor(itis(n)); % skips the first trial
    [erps,dly,~,chan_lbls] = calc_all_erps([preprocpth lowfreq_fl],triggers,erp_range,...
        baseline,reference,sound_delay);
    itis_lbl{n} = sprintf('%.1f s ITI',itis(n));
    % transform to principal components
    pc_erps_low{n} = NaN(size(erps{1}));
    for ii = 1:size(erps{1},3)
        pc_erps_low{n}(:,:,ii) = erps{1}(:,:,ii)*cf;
    end
end

% Plot the ERPs for 0.05-30 Hz
% updated to plot each ITI separately
plot_cond_mderp(pc_erps_low,dly,itis,pcs_to_plot,pc_lbls,sbj,exp_condition,...
    itis_lbl,'lowfreq_pcs',[0 isi median(rt,'omitnan')-sound_delay]);

% Plot ERPs as a function of response time
% concatenate all trials
ntr = numel(rt_iti_ordered);
RT = reshape(rt_iti_ordered,[ntr,1])-sound_delay; % adjust for delay of sound rel to trigger
PC_ERPS_LOW = NaN(length(dly),nchan,ntr);
for n = 1:length(itis)
    % need to account for skipped first trial in each set, which had
    % trigger 35
    idx = 1 + (1:size(pc_erps_low{n},3)) + (n-1)*(size(pc_erps_low{n},3)+1);
    PC_ERPS_LOW(:,:,idx) = pc_erps_low{n};
end
plot_erpbyval({PC_ERPS_LOW},RT,dly,pcs_to_plot,pc_lbls,'Response time (ms)',sbj,exp_condition,...
    'lowfreq_pcs_rt','crange',[-50 50],'show_vals',true);

% Plot ordered by frequency difference
freqdiff_iti = reshape(f_smt_diff,[length(f_smt_diff)/length(itis) length(itis)]);
freqdiff_ordered = freqdiff_iti(:,iti_order);
FREQDIFF = reshape(freqdiff_ordered,[ntr,1]);
plot_erpbyval({PC_ERPS_LOW},FREQDIFF,dly,pcs_to_plot,pc_lbls,'Frequency difference (semitones)',sbj,exp_condition,...
    'lowfreq_pcs_freqdiff','crange',[-50 50]);

% Plot ordered by average frequency
freqmean_iti = reshape(f_tr_mean,[length(f_tr_mean)/length(itis) length(itis)]);
freqmean_ordered = freqmean_iti(:,iti_order);
FREQMEAN = reshape(freqmean_ordered,[ntr,1]);
plot_erpbyval({PC_ERPS_LOW},2.^(FREQMEAN),dly,pcs_to_plot,pc_lbls,'Average frequency (Hz)',sbj,exp_condition,...
    'lowfreq_pcs_freqmean','crange',[-50 50]);

f1 = (2*f_tr_mean-f_smt_diff/12)/2; % get the tone frequency for the first tone
f1_iti = reshape(f1,[length(f1)/length(itis) length(itis)]);
f1_ordered = f1_iti(:,iti_order);
F1 = reshape(f1_ordered,[ntr,1]);
plot_erpbyval({PC_ERPS_LOW},2.^(F1),dly,pcs_to_plot,pc_lbls,'s_1 frequency (Hz)',sbj,exp_condition,...
    'lowfreq_pcs_f1','crange',[-50 50]);

% Ordered by difference from previous trial average frequency
trdiff_ordered = [NaN(1,length(itis)); diff(freqmean_ordered)];
TRDIFF = reshape(trdiff_ordered,[ntr,1]);
plot_erpbyval({PC_ERPS_LOW},TRDIFF,dly,pcs_to_plot,pc_lbls,'Freq. diff. from prev. trial (semitones)',sbj,exp_condition,...
    'lowfreq_pcs_trdiff','crange',[-50 50]);

% Ordered by correct/incorrect
ACC = reshape(acc,[ntr,1]);
plot_erpbyval({PC_ERPS_LOW},ACC,dly,pcs_to_plot,pc_lbls,'Correct (1) or incorrect (0)',sbj,exp_condition,...
    'lowfreq_pcs_acc','crange',[-50 50]);

% Get the average voltage after each trial
% posttrial_v = calc_v_avg(PC_ERPS_LOW,dly,[1200 2000]);

% Plot voltages separately for each condition, and ordered by the
% trial-based value
plot_vbyval({PC_ERPS_LOW},abs(TRDIFF),dly,[300 600],1,...
    sbj,exp_condition,'Freq. diff. from prev. trial (semitones)',{'2 - 9.5 s ITI'},...
    'lowfreq_pcs_early_alltrdiff','nbins',6);
plot_vbyval({PC_ERPS_LOW},abs(TRDIFF),dly,[800 2000],1,...
    sbj,exp_condition,'Freq. diff. from prev. trial (semitones)',{'2 - 9.5 s ITI'},...
    'lowfreq_pcs_late_alltrdiff','nbins',6);

plot_vbyval({PC_ERPS_LOW},abs(2.^F1),dly,[300 600],1,...
    sbj,exp_condition,'s_1 frequency (Hz)',{'2 - 9.5 s ITI'},...
    'lowfreq_pcs_early_allf1','nbins',6);
% Plot difference between first and second PCs which usually account for
% front and back channels
plot_vbyval({-PC_ERPS_LOW(:,2,:)-PC_ERPS_LOW(:,1,:)},abs(2.^F1),dly,[300 600],1,...
    sbj,exp_condition,'s_1 frequency (Hz)',{'2 - 9.5 s ITI'},...
    'lowfreq_pc21diff_early_allf1','nbins',6);

plot_vbyval({PC_ERPS_LOW},abs(RT),dly,[800 2000],1,...
    sbj,exp_condition,'Response time (ms)',{'2 - 9.5 s ITI'},...
    'lowfreq_pcs_late_allrt','nbins',6);
plot_vbyval({PC_ERPS_LOW},abs(RT),dly,[300 600],1,...
    sbj,exp_condition,'Response time (ms)',{'2 - 9.5 s ITI'},...
    'lowfreq_pcs_early_allrt','nbins',6);

plot_vbyval(pc_erps_low,abs(rt_iti_ordered(2:end,:)-sound_delay),dly,[300 2000],1,...
    sbj,exp_condition,'Response time (ms)',itis_lbl,...
    'lowfreq_pcs_rt','nbins',6);

plot_vbyval(pc_erps_low,abs(trdiff_ordered(2:end,:)),dly,[300 600],1,...
    sbj,exp_condition,'Freq. diff. from prev. trial (semitones)',itis_lbl,...
    'lowfreq_pcs_early_trdiff','nbins',6);
plot_vbyval(pc_erps_low,abs(trdiff_ordered(2:end,:)),dly,[800 2000],1,...
    sbj,exp_condition,'Freq. diff. from prev. trial (semitones)',itis_lbl,...
    'lowfreq_pcs_late_trdiff','nbins',6);

plot_vbyval(pc_erps_low,abs(freqdiff_ordered(2:end,:)),dly,[300 2000],1,...
    sbj,exp_condition,'Frequency difference (semitones)',itis_lbl,...
    'lowfreq_pcs_freqdiff','nbins',6);

plot_vbyval(pc_erps_low,abs(2.^f1_ordered(2:end,:)),dly,[300 600],1,...
    sbj,exp_condition,'s_1 frequency (Hz)',itis_lbl,...
    'lowfreq_pcs_f1','nbins',6);

%% Time-frequency analysis of principal components
plot_wavelet({PC_ERPS_LOW},dly,pcs_to_plot,pc_lbls,sbj,exp_condition,...
    'lowfreq_pcs',[0 isi median(rt,'omitnan')-sound_delay]);

% Get the alpha EEG power
alpha_range = [8 12]; % use this frequency range (Hz) for alpha power
% create a zero-phase 4th order butterworth filter
alpha_d = designfilt('bandpassiir','FilterOrder',4,'DesignMethod','butter',...
    'HalfPowerFrequency1',alpha_range(1),'HalfPowerFrequency2',alpha_range(2),...
    'SampleRate',eFs);
[b,a] = tf(alpha_d); % get the transfer function coefficients
alpha_pc_erps = cell(length(itis),1);
for ii = 1:length(itis)
    alpha_pc_erps{ii} = NaN(size(pc_erps_low{ii}));
    for n = 1:size(pc_erps_low{ii},3)
        alpha = filtfilt(b,a,pc_erps_low{ii}(:,:,n));
        alpha_pc_erps{ii}(:,:,n) = abs(hilbert(alpha));
    end
end
plot_cond_mderp(alpha_pc_erps,dly,itis,pcs_to_plot,pc_lbls,sbj,exp_condition,...
    itis_lbl,'alphapower_pcs',[0 isi median(rt,'omitnan')-sound_delay]);
% Concatenate conditions
ALPHA_ERPS = NaN(length(dly),nchan,ntr);
for n = 1:length(itis)
    idx = 1 + (1:size(alpha_pc_erps{n},3)) + (n-1)*(size(alpha_pc_erps{n},3)+1);
    ALPHA_ERPS(:,:,idx) = alpha_pc_erps{n};
end
plot_erpbyval({ALPHA_ERPS},RT,dly,pcs_to_plot,pc_lbls,'Response time (ms)',sbj,exp_condition,...
    'alphapower_pcs_rt','crange',[0 20]);
plot_erpbyval({ALPHA_ERPS},FREQDIFF,dly,pcs_to_plot,pc_lbls,'Frequency difference (semitones)',sbj,exp_condition,...
    'alphapower_pcs_freqdiff','crange',[0 20]);
plot_erpbyval({ALPHA_ERPS},TRDIFF,dly,pcs_to_plot,pc_lbls,'Freq. diff. from prev. trial (semitones)',sbj,exp_condition,...
    'alphapower_pcs_trdiff','crange',[0 20]);
