% Load the results from all subjects and plot the average ERPs
% Nate Zuk (2021)

% Necessary paths
addpath('..');
addpath('../behav_analysis/');
addpath(genpath('C:\Users\zsxbo\Projects\EEG-analysis-tools'));
U = userpath;
addpath([U '\fieldtrip-20210614\']);
addpath([U '\shadedErrorBar\']);

% Parameters
exp_condition = 'active';
exp_name = 'Shahaf2017';
sound_delay = 50; % amount of time between the trigger and the stimulus (in ms)
erp_range = [-100 900]; % range of delays to use to get the ERP (in ms)
% baseline = [];
baseline = [-100 0]; % range of delays to use as the baseline
% Apply mastoids reference
reference = 'mastoids';
% Preprocessing path
% preprocpth = 'A:\PreprocessedEEG\';
eFs = 256; % sampling rate of the EEG data (used for interpolating ERP by reaction time)
front_chans = [1:8, 23:32]; % frontal channels, including Cz
back_chans = 9:22; % back channels
ctrl_clr = [0 0 1];
asd_clr = [0 0.8 0];
miss_thres = 100; % reject subjects with this many misses or more
fig_pth = 'fig';

%% Identify subjects
% Use the Participant_List to identify control and ASD subjects
sbj_list_file = 'A:\Participants\Participants.xlsx';
[sbjs_ctrl,sbjs_asd] = load_subjects_from_list(sbj_list_file);

%% Subject performance
behavpth = sprintf('A:/Behavior/%s/',exp_name);
ctrl_propc = NaN(length(sbjs_ctrl),4);
ctrl_all_propc = NaN(length(sbjs_ctrl),1); % used to get overall proportion correct
    % needed to reject subjects who have too few incorrect trials in
    % corr/incorr bias comparison
ctrl_dpr = NaN(length(sbjs_ctrl),4,2);
ctrl_diffdpr = NaN(length(sbjs_ctrl),4);
ctrl_diffpc = NaN(length(sbjs_ctrl),4);
ctrl_diffdpracc = NaN(length(sbjs_ctrl),4,2);
ctrl_diffpcacc = NaN(length(sbjs_ctrl),4,2);
ctrl_avgpcbyacc = NaN(length(sbjs_ctrl),4,2);
ctrl_stbias = cell(length(sbjs_ctrl),1);
ctrl_acc = cell(length(sbjs_ctrl),1);
% bias conditioned on correct/incorrect previous trials
% ctrl_diffdpr_acc = NaN(length(sbjs_ctrl),4,2);
% ctrl_diffpc_acc = NaN(length(sbjs_ctrl),4,2);
% ctrl_ntr_acc = NaN(length(sbjs_ctrl),4,2);
% bias conditioned on easy/hard previous trials
ctrl_diffdpr_dfclt = NaN(length(sbjs_ctrl),4,2);
ctrl_diffpc_dfclt = NaN(length(sbjs_ctrl),4,2);
ctrl_ntr_dfclt = NaN(length(sbjs_ctrl),4,2);
ctrl_rt = cell(length(sbjs_ctrl),1);
ctrl_md_rt = NaN(length(sbjs_ctrl),4);
ctrl_iqr_rt = NaN(length(sbjs_ctrl),4);
ctrl_rtslow = NaN(length(sbjs_ctrl),4);
ctrl_miss = NaN(length(sbjs_ctrl),1);
for s = 1:length(sbjs_ctrl)
    [ncorr,~,ctrl_miss(s),tottr,ctrl_acc{s}] = calc_fd_perf(sbjs_ctrl{s},behavpth);
%     ctrl_propc(s) = ncorr/tottr;
    [dpr,propcorr,~,itis,ctrl_stbias{s},iti_order] = calculate_prevtrial_dpr_bias(sbjs_ctrl{s},behavpth);
%     [~,blk_by_iti] = sort(iti_order); % to rearrange the behavioral data
%         % so that it is ordered by ITI, you need to sort iti_order, which
%         % specifies which block each ITI was presented
    ctrl_dpr(s,:,:) = dpr;
    ctrl_diffdpr(s,:) = (dpr(:,1)-dpr(:,2))';
    ctrl_diffpc(s,:) = (propcorr(:,1)-propcorr(:,2))';
    ctrl_rt{s} = get_fd_rt(sbjs_ctrl{s},behavpth);
    % Get short-term bias conditioned on previously correct/incorrect
    % responses
%     [dpr_acc,pc_acc,ntr] = calculate_prevtrial_dpr_corr(sbjs_ctrl{s},behavpth);
%     ctrl_diffdpr_acc(s,:,1) = dpr_acc(:,1)-dpr_acc(:,2); % hard trials
%     ctrl_diffdpr_acc(s,:,2) = dpr_acc(:,3)-dpr_acc(:,4); % easy trials
%     ctrl_diffpc_acc(s,:,1) = pc_acc(:,1)-pc_acc(:,2); 
%     ctrl_diffpc_acc(s,:,2) = pc_acc(:,3)-pc_acc(:,4); 
%     ctrl_ntr_acc(s,:,1) = ntr(:,1)+ntr(:,2); % store the number of trials for each condition
%     ctrl_ntr_acc(s,:,2) = ntr(:,3)+ntr(:,4);
    % Get short-term bias conditioned on easy/hard trials
    [dpr_dfclt,pc_dfclt,ntr] = calculate_prevtrial_dpr_diffic(sbjs_ctrl{s},behavpth);
    ctrl_diffdpr_dfclt(s,:,1) = dpr_dfclt(:,1)-dpr_dfclt(:,2); % hard trials
    ctrl_diffdpr_dfclt(s,:,2) = dpr_dfclt(:,3)-dpr_dfclt(:,4); % easy trials
    ctrl_diffpc_dfclt(s,:,1) = pc_dfclt(:,1)-pc_dfclt(:,2); 
    ctrl_diffpc_dfclt(s,:,2) = pc_dfclt(:,3)-pc_dfclt(:,4); 
    ctrl_ntr_dfclt(s,:,1) = ntr(:,1)+ntr(:,2); % store the number of trials for each condition
    ctrl_ntr_dfclt(s,:,2) = ntr(:,3)+ntr(:,4);
    % rearrange the reaction time by iti
    rt_iti = reshape(ctrl_rt{s},[length(ctrl_rt{s})/length(itis) length(itis)]);
    ctrl_rt{s} = rt_iti(:,iti_order);
    ctrl_md_rt(s,:) = median(ctrl_rt{s},'omitnan');
    ctrl_iqr_rt(s,:) = iqr(ctrl_rt{s});
    %%% Calculate post-error RT slowing (3-5-2023)
    acc = reshape(ctrl_acc{s},[length(ctrl_acc{s})/length(itis) length(itis)]);
    for ii = 1:length(itis) % calculate separately for each ITI (posterr_slow assumes one condition)
        ctrl_rtslow(s,ii) = posterr_slow(ctrl_rt{s}(:,ii),acc(:,iti_order(ii)));
    end
    % Calculate the proportion of correct responses per ITI
    ctrl_acc{s} = acc(2:end,iti_order); % rearrange by itis, and remove the first row (first trial, before inter-trial intervals)
    ctrl_propc(s,:) = sum(ctrl_acc{s}==1)./sum(~isnan(ctrl_acc{s})); % (28-9-2022) Only include non-skipped trials when calculating PC
    ctrl_all_propc(s) = sum(sum(ctrl_acc{s}==1))/sum(sum(~isnan(ctrl_acc{s})));
    % Calculate performance conditioned on correct/incorrect previous
    % trials
    [dpr_prevacc,pc_prevacc,ntr_prevacc] = calculate_prevtrial_dpr_corr(sbjs_ctrl{s},behavpth);
    ctrl_diffdpracc(s,:,1) = dpr_prevacc(1,:)-dpr_prevacc(2,:); % d-prime difference conditioned on correct trials
    ctrl_diffdpracc(s,:,2) = dpr_prevacc(3,:)-dpr_prevacc(4,:); % d-prime difference conditioned on incorrect trials
    % compute the difference in percent correct (maybe d-prime difference is smaller
    % because of the number of trials???)
    ctrl_diffpcacc(s,:,1) = pc_prevacc(1,:)-pc_prevacc(2,:);
    ctrl_diffpcacc(s,:,2) = pc_prevacc(3,:)-pc_prevacc(4,:);
    % calculate average PC following correct/incorrect previous trials
    ctrl_avgpcbyacc(s,:,1) = (pc_prevacc(1,:).*ntr_prevacc(1,:) + pc_prevacc(2,:).*ntr_prevacc(2,:))...
        ./(ntr_prevacc(1,:)+ntr_prevacc(2,:));
    ctrl_avgpcbyacc(s,:,2) = (pc_prevacc(3,:).*ntr_prevacc(3,:) + pc_prevacc(4,:).*ntr_prevacc(4,:))...
        ./(ntr_prevacc(3,:)+ntr_prevacc(4,:));
end
asd_propc = NaN(length(sbjs_asd),4);
asd_all_propc = NaN(length(sbjs_asd),1);
asd_dpr = NaN(length(sbjs_asd),4,2);
asd_diffdpr = NaN(length(sbjs_asd),4);
asd_diffpc = NaN(length(sbjs_asd),4);
asd_diffdpracc = NaN(length(sbjs_asd),4,2);
asd_diffpcacc = NaN(length(sbjs_asd),4,2);
asd_avgpcbyacc = NaN(length(sbjs_asd),4,2);
asd_stbias = cell(length(sbjs_asd),1);
asd_acc = cell(length(sbjs_asd),1);
% bias conditioned on easy/hard trials
asd_diffdpr_dfclt = NaN(length(sbjs_asd),4,2);
asd_diffpc_dfclt = NaN(length(sbjs_asd),4,2);
asd_ntr_dfclt = NaN(length(sbjs_asd),4,2);
asd_rt = cell(length(sbjs_asd),1);
asd_md_rt = NaN(length(sbjs_asd),4);
asd_iqr_rt = NaN(length(sbjs_asd),4);
asd_rtslow = NaN(length(sbjs_asd),4);
asd_miss = NaN(length(sbjs_asd),1);
for s = 1:length(sbjs_asd)
    [ncorr,~,asd_miss(s),tottr,asd_acc{s}] = calc_fd_perf(sbjs_asd{s},behavpth);
%     asd_propc(s) = ncorr/tottr;
    [dpr,propcorr,~,itis,asd_stbias{s},iti_order] = calculate_prevtrial_dpr_bias(sbjs_asd{s},behavpth);
%     [~,blk_by_iti] = sort(iti_order); % to rearrange the behavioral data
%         % so that it is ordered by ITI, you need to sort iti_order, which
%         % specifies which block each ITI was presented
    asd_dpr(s,:,:) = dpr;
    asd_diffdpr(s,:) = (dpr(:,1)-dpr(:,2))';
    asd_diffpc(s,:) = (propcorr(:,1)-propcorr(:,2))';
    asd_rt{s} = get_fd_rt(sbjs_asd{s},behavpth);
    % Get short-term bias conditioned on easy/hard trials
    [dpr_dfclt,pc_dfclt,ntr] = calculate_prevtrial_dpr_diffic(sbjs_asd{s},behavpth);
    asd_diffdpr_dfclt(s,:,1) = dpr_dfclt(:,1)-dpr_dfclt(:,2); % hard trials
    asd_diffdpr_dfclt(s,:,2) = dpr_dfclt(:,3)-dpr_dfclt(:,4); % easy trials
    asd_diffpc_dfclt(s,:,1) = pc_dfclt(:,1)-pc_dfclt(:,2); 
    asd_diffpc_dfclt(s,:,2) = pc_dfclt(:,3)-pc_dfclt(:,4); 
    asd_ntr_dfclt(s,:,1) = ntr(:,1)+ntr(:,2); % store the number of trials for each condition
    asd_ntr_dfclt(s,:,2) = ntr(:,3)+ntr(:,4);
    % rearrange the reaction time by iti
    rt_iti = reshape(asd_rt{s},[length(asd_rt{s})/length(itis) length(itis)]);
    asd_rt{s} = rt_iti(:,iti_order);
    asd_md_rt(s,:) = median(asd_rt{s},'omitnan');
    asd_iqr_rt(s,:) = iqr(asd_rt{s});
    %%% Calculate post-error RT slowing (3-5-2023)
    acc = reshape(asd_acc{s},[length(asd_acc{s})/length(itis) length(itis)]);
    for ii = 1:length(itis) % calculate separately for each ITI (posterr_slow assumes one condition)
        asd_rtslow(s,ii) = posterr_slow(asd_rt{s}(:,ii),acc(:,iti_order(ii)));
    end
    % Calculate the proportion of correct responses per ITI
    asd_acc{s} = acc(2:end,iti_order); % rearrange by itis, and remove the first row (first trial, before inter-trial intervals)
    asd_propc(s,:) = sum(asd_acc{s}==1)./sum(~isnan(asd_acc{s}));
    asd_all_propc(s) = sum(sum(asd_acc{s}==1))/sum(sum(~isnan(asd_acc{s})));
    % Calculate performance conditioned on correct/incorrect previous
    % trials
    [dpr_prevacc,pc_prevacc,ntr_prevacc] = calculate_prevtrial_dpr_corr(sbjs_asd{s},behavpth);
    asd_diffdpracc(s,:,1) = dpr_prevacc(1,:)-dpr_prevacc(2,:); % d-prime difference conditioned on correct trials
    asd_diffdpracc(s,:,2) = dpr_prevacc(3,:)-dpr_prevacc(4,:); % d-prime difference conditioned on incorrect trials
    % compute the difference in percent correct (maybe d-prime difference is smaller
    % because of the number of trials???)
    asd_diffpcacc(s,:,1) = pc_prevacc(1,:)-pc_prevacc(2,:);
    asd_diffpcacc(s,:,2) = pc_prevacc(3,:)-pc_prevacc(4,:);
        % calculate average PC following correct/incorrect previous trials
    asd_avgpcbyacc(s,:,1) = (pc_prevacc(1,:).*ntr_prevacc(1,:) + pc_prevacc(2,:).*ntr_prevacc(2,:))...
        ./(ntr_prevacc(1,:)+ntr_prevacc(2,:));
    asd_avgpcbyacc(s,:,2) = (pc_prevacc(3,:).*ntr_prevacc(3,:) + pc_prevacc(4,:).*ntr_prevacc(4,:))...
        ./(ntr_prevacc(3,:)+ntr_prevacc(4,:));
end

% Reject subjects based on the number of misses
% use_ctrl = ctrl_miss < miss_thres;
% use_asd = asd_miss < miss_thres;
% fprintf('Rejecting %d controls and %d ASD\n',sum(~use_ctrl),sum(~use_asd));

% Reject subjects based on performance
perf_thres = [0 100];
use_ctrl = ctrl_all_propc*100 > perf_thres(1) & ctrl_all_propc*100 < perf_thres(2);
use_asd = asd_all_propc*100 > perf_thres(1) & asd_all_propc*100 < perf_thres(2);

% Plot performance vs average bias
figure
hold on
plot(ctrl_all_propc*100,mean(ctrl_diffdpr,2),'.','Color',ctrl_clr,'MarkerSize',14);
plot(asd_all_propc*100,mean(asd_diffdpr,2),'.','Color',asd_clr,'MarkerSize',14);
xlabel('% correct');
ylabel('Average d-prime bias');
ctrl_all_lg = sprintf('Controls (N=%d)',length(sbjs_ctrl));
asd_all_lg = sprintf('ASD (N=%d)',length(sbjs_asd));
legend(ctrl_all_lg,asd_all_lg);
saveas(gcf,'fig/AllSbj_FD_PCavgbias.png');

% Plot performance and bias
ctrl_lg = sprintf('Controls (N=%d)',sum(use_ctrl));
asd_lg = sprintf('ASD (N=%d)',sum(use_asd));
figure
set(gcf,'Position',[775 100 800 1000]);
h = subplot(3,1,1);
% Use dot median plot instead
%%% (10-11-2022) Use the true ITI as the value, not SOA
dotmdlbls = {repelem(itis-0.6,sum(use_ctrl),1),repelem(itis-0.6,sum(use_asd),1)};
    % labels for dot-median-plot, which plots controls and ASD around the
    % same x value as different colors
CTRLPC = reshape(ctrl_propc(use_ctrl,:)*100,[numel(ctrl_propc(use_ctrl,:)),1]);
ASDPC = reshape(asd_propc(use_asd,:)*100,[numel(asd_propc(use_asd,:)),1]);
md_h = dot_median_plot(dotmdlbls,{CTRLPC, ASDPC},[ctrl_clr; asd_clr],...
    'fig_handle',h);
legend(md_h,{ctrl_lg,asd_lg},'Location','southwest');
xlabel('ITI (s)');
ylabel('Percent correct');
% Test if proportions correct are significantly different
p_propc = NaN(length(itis),1);
u_propc = NaN(length(itis),1);
for n = 1:length(itis)
    [p_propc(n),~,st_propc] = ranksum(ctrl_propc(use_ctrl,n),asd_propc(use_asd,n));
    u_propc(n) = st_propc.ranksum;
    fprintf('PC - rank-sum: ITI = %.1f s, U = %.0f, p = %.3f\n',itis(n),u_propc(n),p_propc(n));
end

CTRLDPR = reshape(ctrl_diffdpr(use_ctrl,:),[numel(ctrl_diffdpr(use_ctrl,:)),1]);
ASDDPR = reshape(asd_diffdpr(use_asd,:),[numel(asd_diffdpr(use_asd,:)),1]);
h = subplot(3,1,2);
md_h = dot_median_plot(dotmdlbls,{CTRLDPR, ASDDPR},[ctrl_clr; asd_clr],...
    'fig_handle',h);
legend(md_h,{ctrl_lg,asd_lg},'Location','southwest');
xlabel('ITI (s)');
ylabel('d''_{st,bias+} - d''_{st,bias-}');
p_diffdpr = NaN(length(itis),1);
u_diffdpr = NaN(length(itis),1);
for n = 1:length(itis)
    [p_diffdpr(n),~,st_diffdpr] = ranksum(ctrl_diffdpr(use_ctrl,n),asd_diffdpr(use_asd,n));
    u_diffdpr(n) = st_diffdpr.ranksum;
    fprintf('dpr diff - rank-sum: ITI = %.1f s, U = %.0f, p = %.3f\n',itis(n),u_diffdpr(n),p_diffdpr(n));
end
% Friedman test of change in d-prime with ITI
[p_frd_dpr_ctrl,tbl_frd_dpr_ctrl,st_frd_dpr_ctrl] = friedman(ctrl_diffdpr(use_ctrl,:),1,'off');
[p_frd_dpr_asd,tbl_frd_dpr_asd,st_frd_dpr_asd] = friedman(asd_diffdpr(use_asd,:),1,'off');

% PC-based bias
CTRLPC = reshape(ctrl_diffpc(use_ctrl,:),[numel(ctrl_diffpc(use_ctrl,:)),1]);
ASDPC = reshape(asd_diffpc(use_asd,:),[numel(asd_diffpc(use_asd,:)),1]);
h = subplot(3,1,3);
md_h = dot_median_plot(dotmdlbls,{CTRLPC, ASDPC},[ctrl_clr; asd_clr],...
    'fig_handle',h);
legend(md_h,{ctrl_lg,asd_lg},'Location','southwest');
xlabel('ITI (s)');
ylabel('PC_{st,bias+} - PC_{st,bias-} (%)');
p_diffpc = NaN(length(itis),1);
u_diffpc = NaN(length(itis),1);
for n = 1:length(itis)
    [p_diffpc(n),~,st_diffpc] = ranksum(ctrl_diffpc(use_ctrl,n),asd_diffpc(use_asd,n));
    u_diffpc(n) = st_diffpc.ranksum;
    fprintf('PC diff - rank-sum: ITI = %.1f s, U = %.0f, p = %.3f\n',itis(n),u_diffpc(n),p_diffpc(n));
end

% save the performance image
saveas(gcf,[fig_pth '/AllSbj_FDperf.png']);
saveas(gcf,[fig_pth '/AllSbj_FDperf.svg']);

% Plot the correlation between the change in performance and the change in
% bias with ITI (for controls)
figure
subplot(1,2,1);
plot((ctrl_propc(:,3)-ctrl_propc(:,1))*100,ctrl_diffdpr(:,3)-ctrl_diffdpr(:,1),'.','MarkerSize',14,'Color',ctrl_clr)
legend(ctrl_lg)
set(gca,'FontSize',12);
xlabel('Change in percent correct (5.9 s ITI - 1.4 s ITI)');
ylabel('Change in s.t. bias (5.9 s ITI - 1.4 s ITI)');
[c,p] = corr(ctrl_propc(:,3)-ctrl_propc(:,1),ctrl_diffdpr(:,3)-ctrl_diffdpr(:,1),'type','Spearman');
title(sprintf('rho = %.3f, p = %.3f',c,p));

subplot(1,2,2);
plot((ctrl_propc(:,4)-ctrl_propc(:,1))*100,ctrl_diffdpr(:,4)-ctrl_diffdpr(:,1),'.','MarkerSize',14,'Color',ctrl_clr)
legend(ctrl_lg)
set(gca,'FontSize',12);
xlabel('Change in percent correct (8.9 s ITI - 1.4 s ITI)');
ylabel('Change in s.t. bias (8.9 s ITI - 1.4 s ITI)');
[c,p] = corr(ctrl_propc(:,4)-ctrl_propc(:,1),ctrl_diffdpr(:,4)-ctrl_diffdpr(:,1),'type','Spearman');
title(sprintf('rho = %.3f, p = %.3f',c,p));

% Plot the d-prime of Bias+/Bias- separately
figure
set(gcf,'Position',[775 100 1000 500]);
h = subplot(1,1,1);
% dprmdlbls = {repelem(itis,sum(use_ctrl)*2), repelem(itis,sum(use_asd)*2)};
dprmdlbls = [dotmdlbls(1) dotmdlbls(1) dotmdlbls(2) dotmdlbls(2)];
    % copy the control and ASD lbls twice, once for Bias+, once for Bias-
CTRLDPR_BP = reshape(ctrl_dpr(use_ctrl,:,1),[sum(use_ctrl)*length(itis),1]);
CTRLDPR_BM = reshape(ctrl_dpr(use_ctrl,:,2),[sum(use_ctrl)*length(itis),1]);
ASDDPR_BP = reshape(asd_dpr(use_asd,:,1),[sum(use_asd)*length(itis),1]);
ASDDPR_BM = reshape(asd_dpr(use_asd,:,2),[sum(use_asd)*length(itis),1]);
md_h = dot_median_plot(dprmdlbls,{CTRLDPR_BP, CTRLDPR_BM, ASDDPR_BP, ASDDPR_BM},...
    [ctrl_clr; ctrl_clr*0.5; asd_clr; asd_clr*0.5],'fig_handle',h);
legend(md_h,{'Controls, Bias+','Controls, Bias-','ASD, Bias+','ASD, Bias-'},'Location','southwest');
xlabel('ITI (s)');
ylabel('d''_{st}');
% stats comparing Bias+ and Bias- performance between groups
p_dpr = NaN(length(itis),2);
u_dpr = NaN(length(itis),2);
for n = 1:length(itis)
    [p_dpr(n,1),~,st_dpr] = ranksum(ctrl_dpr(use_ctrl,n,1),asd_dpr(use_asd,n,1));
    u_dpr(n,1) = st_dpr.ranksum;
    fprintf('dpr Bias+: ITI = %.1f s, U = %.0f, p = %.3f\n',itis(n),u_dpr(n,1),p_dpr(n,1));
    [p_dpr(n,2),~,st_dpr] = ranksum(ctrl_dpr(use_ctrl,n,2),asd_dpr(use_asd,n,2));
    u_dpr(n,2) = st_dpr.ranksum;
    fprintf('dpr Bias-: ITI = %.1f s, U = %.0f, p = %.3f\n',itis(n),u_dpr(n,2),p_dpr(n,2));
end
saveas(gcf,'fig/AllSbj_FDsepbias.png');

% Plot the short-term bias conditioned on easy/hard trials
% figure
% set(gcf,'Position',[775 100 1000 500]);
% h = subplot(1,1,1);
% % dprmdlbls = {repelem(itis,sum(use_ctrl)*2), repelem(itis,sum(use_asd)*2)};
% dprmdlbls = [dotmdlbls(1) dotmdlbls(1) dotmdlbls(2) dotmdlbls(2)];
%     % copy the control and ASD lbls twice, once for Bias+, once for Bias-
% CTRLDIFFDPR_HARD = reshape(ctrl_diffdpr_dfclt(use_ctrl,:,1),[sum(use_ctrl)*length(itis),1]);
% CTRLDIFFDPR_EASY = reshape(ctrl_diffdpr_dfclt(use_ctrl,:,2),[sum(use_ctrl)*length(itis),1]);
% ASDDIFFDPR_HARD = reshape(asd_diffdpr_dfclt(use_asd,:,1),[sum(use_asd)*length(itis),1]);
% ASDDIFFDPR_EASY = reshape(asd_diffdpr_dfclt(use_asd,:,2),[sum(use_asd)*length(itis),1]);
% md_h = dot_median_plot(dprmdlbls,{CTRLDIFFDPR_HARD, CTRLDIFFDPR_EASY, ASDDIFFDPR_HARD, ASDDIFFDPR_EASY},...
%     [ctrl_clr; ctrl_clr*0.5; asd_clr; asd_clr*0.5],'fig_handle',h);
% legend(md_h,{'Controls, Previous hard','Controls, Previous easy','ASD, Previous hard','ASD, Previous easy'},'Location','southwest');
% xlabel('ITI (s)');
% ylabel('d''_{st,bias+} - d''_{st,bias-}');
% % stats comparing Bias+ and Bias- performance between groups
% p_dfclt = NaN(length(itis),2);
% w_dfclt = NaN(length(itis),2);
% for n = 1:length(itis)
%     % controls
%     [p_dfclt(n,1),~,st_dpr] = signrank(ctrl_diffdpr_dfclt(use_ctrl,n,1),ctrl_diffdpr_dfclt(use_ctrl,n,2));
%     w_dfclt(n,1) = st_dpr.signedrank;
%     fprintf('Controls, dpr diff, cmp difficulty: ITI = %.1f s, W = %.0f, p = %.3f\n',itis(n),w_dfclt(n,1),p_dfclt(n,1));
%     % asd
%     [p_dfclt(n,2),~,st_dpr] = signrank(asd_diffdpr_dfclt(use_asd,n,1),asd_diffdpr_dfclt(use_asd,n,2));
%     w_dfclt(n,2) = st_dpr.signedrank;
%     fprintf('ASD, dpr diff, cmp difficulty: ITI = %.1f s, W = %.0f, p = %.3f\n',itis(n),w_dfclt(n,2),p_dfclt(n,2));
% end
% saveas(gcf,'fig/AllSbj_FD_biasbydifficulty.png');
% based on d-prime
figure
set(gcf,'Position',[100 100 1050 800]);
dfcltcmp_h = subplot(2,1,1);
combined_diffdprdfclt = {squeeze(ctrl_diffdpr_dfclt(use_ctrl,1,:)),... % 2 s ITI
    squeeze(ctrl_diffdpr_dfclt(use_ctrl,2,:)),...
    squeeze(ctrl_diffdpr_dfclt(use_ctrl,3,:)),...
    squeeze(ctrl_diffdpr_dfclt(use_ctrl,4,:)),...
    squeeze(asd_diffdpr_dfclt(use_asd,1,:)),... % ASD sbjs
    squeeze(asd_diffdpr_dfclt(use_asd,2,:)),...
    squeeze(asd_diffdpr_dfclt(use_asd,3,:)),...
    squeeze(asd_diffdpr_dfclt(use_asd,4,:))};
dot_connect_plot(combined_diffdprdfclt,{'Hard','Easy'},...
    [repelem(ctrl_clr,length(itis),1).*[1 0.8 0.6 0.4]'; repelem(asd_clr,length(itis),1).*[1 0.8 0.6 0.4]'],...
    'fig_handle',dfcltcmp_h);
xlabel('Difficulty of previous trial');
% legend(cond_h,{ctrl_acccmp_lg,asd_acccmp_lg},'Location','southwest');
ylabel('d''_{st,bias+} - d''_{st,bias-}');
% based on PC
dfcltcmp_h = subplot(2,1,2);
combined_diffpcdfclt = {squeeze(ctrl_diffpc_dfclt(use_ctrl,1,:))*100,... % 2 s ITI
    squeeze(ctrl_diffpc_dfclt(use_ctrl,2,:))*100,...
    squeeze(ctrl_diffpc_dfclt(use_ctrl,3,:))*100,...
    squeeze(ctrl_diffpc_dfclt(use_ctrl,4,:))*100,...
    squeeze(asd_diffpc_dfclt(use_asd,1,:))*100,... % ASD sbjs
    squeeze(asd_diffpc_dfclt(use_asd,2,:))*100,...
    squeeze(asd_diffpc_dfclt(use_asd,3,:))*100,...
    squeeze(asd_diffpc_dfclt(use_asd,4,:))*100};
cond_h = dot_connect_plot(combined_diffpcdfclt,{'Hard','Easy'},...
    [repelem(ctrl_clr,length(itis),1).*[1 0.8 0.6 0.4]'; repelem(asd_clr,length(itis),1).*[1 0.8 0.6 0.4]'],...
    'fig_handle',dfcltcmp_h);
xlabel('Difficulty of previous trial');
% legend(cond_h,{ctrl_acccmp_lg,asd_acccmp_lg},'Location','southwest');
ylabel('PC_{st,bias+} - PC_{st,bias-}');
set(gca,'YLim',[-50 100]);
% create the legend
ctrl_acccmp_lg = cell(length(itis),1);
asd_acccmp_lg = cell(length(itis),1);
for n = 1:length(itis)
    ctrl_acccmp_lg{n} = sprintf('Controls (N=%d), %.1f s SOA',length(sbjs_ctrl),itis(n));
    asd_acccmp_lg{n} = sprintf('ASD (N=%d), %.1f s SOA',length(sbjs_asd),itis(n));
end
legend(cond_h,[ctrl_acccmp_lg,asd_acccmp_lg],'Location','southwest');
p_diffpc_dfclt = NaN(2,length(itis));
st_diffpc_dfclt = NaN(2,length(itis));
for n = 1:length(itis)
    [p_diffpc_dfclt(1,n),~,st] = signrank(squeeze(ctrl_diffpc_dfclt(use_ctrl,n,2)),...
        squeeze(ctrl_diffpc_dfclt(use_ctrl,n,1)));
    st_diffpc_dfclt(1,n) = st.signedrank;
    [p_diffpc_dfclt(2,n),~,st] = signrank(squeeze(asd_diffpc_dfclt(use_asd,n,2)),...
        squeeze(asd_diffpc_dfclt(use_asd,n,1)));
    st_diffpc_dfclt(2,n) = st.signedrank;
end
saveas(gcf,'fig/AllSbj_FD_biasbydifficulty.png');

% Plot response times for each group
% Compute the median response time for each subject
% ctrl_md_rt = cellfun(@(x) median(x,'omitnan'),ctrl_rt);
% asd_md_rt = cellfun(@(x) median(x,'omitnan'),asd_rt);
CTRLRT = reshape(ctrl_md_rt(use_ctrl,:),[numel(ctrl_md_rt(use_ctrl,:)),1]);
ASDRT = reshape(asd_md_rt(use_asd,:),[numel(asd_md_rt(use_asd,:)),1]);
figure
h_rt = subplot(1,3,1);
md_h = dot_median_plot(dotmdlbls,{CTRLRT-650, ASDRT-650},[ctrl_clr; asd_clr],'fig_handle',h_rt);
set(gcf,'Position',[345 515 1050 400]);
legend(md_h,{ctrl_lg,asd_lg},'Location','northwest');
xlabel('ITI (s)');
ylabel('Response time (ms, rel 2nd tone)');
p_rt = NaN(length(itis),1);
u_rt = NaN(length(itis),1);
for n = 1:length(itis)
    [p_rt(n),~,st_rt] = ranksum(ctrl_md_rt(use_ctrl,n),asd_md_rt(use_asd,n));
    u_rt(n) = st_rt.ranksum;
    fprintf('RT - rank-sum: ITIS = %.1f s, U = %.0f, p = %.3f\n',itis(n),u_rt(n),p_rt(n));
end
% IQR of RT
CTRLIQR = reshape(ctrl_iqr_rt(use_ctrl,:),[numel(ctrl_iqr_rt(use_ctrl,:)),1]);
ASDIQR = reshape(asd_iqr_rt(use_asd,:),[numel(asd_iqr_rt(use_asd,:)),1]);
h_rt = subplot(1,3,2);
% md_h = dot_median_plot(dotmdlbls,{ctrl_md_rt-2050, asd_md_rt-2050},[ctrl_clr; asd_clr]);
md_h = dot_median_plot(dotmdlbls,{CTRLIQR, ASDIQR},[ctrl_clr; asd_clr],'fig_handle',h_rt);
% set(gcf,'Position',[345 515 500 400]);
legend(md_h,{ctrl_lg,asd_lg},'Location','northwest');
xlabel('ITI (s)');
ylabel('IQR of response time (ms)');
p_iqr = NaN(length(itis),1);
u_iqr = NaN(length(itis),1);
for n = 1:length(itis)
    [p_iqr(n),~,st_iqr] = ranksum(ctrl_iqr_rt(use_ctrl,n),asd_iqr_rt(use_asd,n));
    u_iqr(n) = st_iqr.ranksum;
    fprintf('IQR of RT - rank-sum: ITIS = %.1f s, U = %.0f, p = %.3f\n',itis(n),u_iqr(n),p_iqr(n));
end
% post-error RT slowing
CTRLRTSLOW = reshape(ctrl_rtslow(use_ctrl,:),[numel(ctrl_rtslow(use_ctrl,:)),1]);
ASDRTSLOW = reshape(asd_rtslow(use_asd,:),[numel(asd_rtslow(use_asd,:)),1]);
h_rt = subplot(1,3,3);
% md_h = dot_median_plot(dotmdlbls,{ctrl_md_rt-2050, asd_md_rt-2050},[ctrl_clr; asd_clr]);
md_h = dot_median_plot(dotmdlbls,{CTRLRTSLOW, ASDRTSLOW},[ctrl_clr; asd_clr],'fig_handle',h_rt);
% set(gcf,'Position',[345 515 500 400]);
legend(md_h,{ctrl_lg,asd_lg},'Location','northwest');
xlabel('ITI (s)');
ylabel('Post-error RT slowing (ms)');
p_rtslow = NaN(length(itis),1);
u_rtslow = NaN(length(itis),1);
for n = 1:length(itis)
    [p_rtslow(n),~,st_rtslow] = ranksum(ctrl_rtslow(use_ctrl,n),asd_rtslow(use_asd,n));
    u_rtslow(n) = st_rtslow.ranksum;
    fprintf('Post-error RT slowing - rank-sum: ITIS = %.1f s, U = %.0f, p = %.3f\n',itis(n),u_rtslow(n),p_rtslow(n));
end

saveas(gcf,'fig/AllSbj_FDrt.png');
saveas(gcf,'fig/AllSbj_FDrt.svg');

% Plot the short-term bias based on previously correct and incorrect trials
%%% Note: This doesn't work if there are too few incorrect trials, I have
%%% to reject ASD subjects with >91% performance
rej_toogood = 100; % reject subjects with performance above this point
use_ctrl_acccmp = ctrl_all_propc*100 < rej_toogood;
use_asd_acccmp = asd_all_propc*100 < rej_toogood;
% make a new legend accounting for these missing subjects
% ctrl_acccmp_lg = sprintf('Controls (N=%d)',sum(use_ctrl&use_ctrl_acccmp));
% asd_acccmp_lg = sprintf('ASD (N=%d)',sum(use_asd&use_asd_acccmp));
% % plot
% figure
% set(gcf,'Position',[100 100 1200 800]);
% acccmp_h = subplot(3,1,1);
% combined_diffdpracc = {squeeze(ctrl_diffdpracc(use_ctrl&use_ctrl_acccmp,1,:)),... % 2 s ITI
%     squeeze(ctrl_diffdpracc(use_ctrl&use_ctrl_acccmp,2,:)),...
%     squeeze(ctrl_diffdpracc(use_ctrl&use_ctrl_acccmp,3,:)),...
%     squeeze(ctrl_diffdpracc(use_ctrl&use_ctrl_acccmp,4,:)),...
%     squeeze(asd_diffdpracc(use_asd&use_asd_acccmp,1,:)),... % ASD sbjs
%     squeeze(asd_diffdpracc(use_asd&use_asd_acccmp,2,:)),...
%     squeeze(asd_diffdpracc(use_asd&use_asd_acccmp,3,:)),...
%     squeeze(asd_diffdpracc(use_asd&use_asd_acccmp,4,:))};
% corrincorr_xlbl = cell(2*length(itis),1);
% for n = 1:length(itis)
%     idx = 1:2 + (n-1)*2;
%     corrincorr_xlbl{idx(1)} = sprintf('Correct, %.1f s SOA',itis(n));
%     corrincorr_xlbl{idx(2)} = sprintf('Incorrect, %.1f s SOA',itis(n));
% end
% dot_connect_plot(combined_diffdpracc,{'Correct','Incorrect'},...
%     [repelem(ctrl_clr,length(itis),1).*[1 0.8 0.6 0.4]'; repelem(asd_clr,length(itis),1).*[1 0.8 0.6 0.4]'],...
%     'fig_handle',acccmp_h);
% xlabel('Accuracy in previous trial');
% % legend(cond_h,{ctrl_acccmp_lg,asd_acccmp_lg},'Location','southwest');
% ylabel('d''_{st,bias+} - d''_{st,bias-}');
% p_diffdpr = NaN(2,1);
% st_diffdpr = NaN(2,1);
% [p_diffdpr(1),~,st] = signrank(ctrl_diffdpracc(use_ctrl&use_ctrl_acccmp,2),ctrl_diffdpracc(use_ctrl&use_ctrl_acccmp,1));
% st_diffdpr(1) = st.signedrank;
% [p_diffdpr(2),~,st] = signrank(asd_diffdpracc(use_asd&use_asd_acccmp,2),asd_diffdpracc(use_asd&use_asd_acccmp,1));
% st_diffdpr(2) = st.signedrank;
% title(sprintf('Controls: W = %3d, p = %.3f; ASD: W = %3d, p = %.3f',...
%     st_diffdpr(1),p_diffdpr(1),st_diffdpr(2),p_diffdpr(2)));
% % is the change in d-pr corr/incorr difference significantly different
% % between groups?
% [p_grpcorrdiff,~,st_grpcorrdiff] = ranksum(diff(ctrl_diffdpracc(use_ctrl&use_ctrl_acccmp,:),[],2),diff(asd_diffdpracc(use_asd&use_asd_acccmp,:),[],2));
% fprintf('Corr/incorr bias difference, rank-sum: U = %d, p = %.3f\n',...
%     st_grpcorrdiff.ranksum,p_grpcorrdiff);

% plot bias+/bias- pc difference conditioned on correct/incorrect trials
figure
set(gcf,'Position',[100 100 1050 800]);
acccmp_h = subplot(2,1,1);
combined_diffpcacc = {squeeze(ctrl_diffpcacc(use_ctrl&use_ctrl_acccmp,1,:))*100,... % 2 s ITI
    squeeze(ctrl_diffpcacc(use_ctrl&use_ctrl_acccmp,2,:))*100,...
    squeeze(ctrl_diffpcacc(use_ctrl&use_ctrl_acccmp,3,:))*100,...
    squeeze(ctrl_diffpcacc(use_ctrl&use_ctrl_acccmp,4,:))*100,...
    squeeze(asd_diffpcacc(use_asd&use_asd_acccmp,1,:))*100,... % ASD sbjs
    squeeze(asd_diffpcacc(use_asd&use_asd_acccmp,2,:))*100,...
    squeeze(asd_diffpcacc(use_asd&use_asd_acccmp,3,:))*100,...
    squeeze(asd_diffpcacc(use_asd&use_asd_acccmp,4,:))*100};
dot_connect_plot(combined_diffpcacc,{'Correct','Incorrect'},...
    [repelem(ctrl_clr,length(itis),1).*[1 0.8 0.6 0.4]'; repelem(asd_clr,length(itis),1).*[1 0.8 0.6 0.4]'],...
    'fig_handle',acccmp_h);
xlabel('Accuracy in previous trial');
% legend(cond_h,{ctrl_acccmp_lg,asd_acccmp_lg},'Location','southwest');
ylabel('PC_{st,bias+} - PC_{st,bias-}');
set(gca,'YLim',[0 100]);
p_diffpc = NaN(2,length(itis));
st_diffpc = NaN(2,length(itis));
for n = 1:length(itis)
    [p_diffpc(1,n),~,st] = signrank(squeeze(ctrl_diffpcacc(use_ctrl&use_ctrl_acccmp,n,2)),...
        squeeze(ctrl_diffpcacc(use_ctrl&use_ctrl_acccmp,n,1)));
    st_diffpc(1,n) = st.signedrank;
    [p_diffpc(2,n),~,st] = signrank(squeeze(asd_diffpcacc(use_asd&use_asd_acccmp,n,2)),...
        squeeze(asd_diffpcacc(use_asd&use_asd_acccmp,n,1)));
    st_diffpc(2,n) = st.signedrank;
end
% title(sprintf('Controls: W = %3d, p = %.3f; ASD: W = %3d, p = %.3f',...
%     st_diffpc(1),p_diffpc(1),st_diffpc(2),p_diffpc(2)));
% is the change in PC corr/incorr difference significantly different
% between groups?
% [p_grppcdiff,~,st_grppcdiff] = ranksum(diff(ctrl_diffpcacc(use_ctrl&use_ctrl_acccmp,:),[],2),diff(asd_diffpcacc(use_asd&use_asd_acccmp,:),[],2));
% fprintf('Corr/incorr PC-based bias difference, rank-sum: U = %d, p = %.3f\n',...
%     st_grppcdiff.ranksum,p_grppcdiff);

% plot average PC for correct/incorrect previous trials
acccmp_h = subplot(2,1,2);
combined_avgpcbyacc = {squeeze(ctrl_avgpcbyacc(use_ctrl&use_ctrl_acccmp,1,:))*100,... % 2 s ITI
    squeeze(ctrl_avgpcbyacc(use_ctrl&use_ctrl_acccmp,2,:))*100,...
    squeeze(ctrl_avgpcbyacc(use_ctrl&use_ctrl_acccmp,3,:))*100,...
    squeeze(ctrl_avgpcbyacc(use_ctrl&use_ctrl_acccmp,4,:))*100,...
    squeeze(asd_avgpcbyacc(use_asd&use_asd_acccmp,1,:))*100,... % ASD sbjs
    squeeze(asd_avgpcbyacc(use_asd&use_asd_acccmp,2,:))*100,...
    squeeze(asd_avgpcbyacc(use_asd&use_asd_acccmp,3,:))*100,...
    squeeze(asd_avgpcbyacc(use_asd&use_asd_acccmp,4,:))*100};
cond_h = dot_connect_plot(combined_avgpcbyacc,{'Correct','Incorrect'},...
    [repelem(ctrl_clr,length(itis),1).*[1 0.8 0.6 0.4]'; repelem(asd_clr,length(itis),1).*[1 0.8 0.6 0.4]'],...
    'fig_handle',acccmp_h);
xlabel('Accuracy in previous trial');
% % create the legend
% ctrl_acccmp_lg = cell(length(itis),1);
% asd_acccmp_lg = cell(length(itis),1);
% for n = 1:length(itis)
%     ctrl_acccmp_lg{n} = sprintf('Controls (N=%d), %.1f s SOA',length(sbjs_ctrl),itis(n));
%     asd_acccmp_lg{n} = sprintf('ASD (N=%d), %.1f s SOA',length(sbjs_asd),itis(n));
% end
legend(cond_h,[ctrl_acccmp_lg,asd_acccmp_lg],'Location','southwest');
ylabel('Percent correct');
p_pcbyacc = NaN(2,length(itis));
st_pcbyacc = NaN(2,length(itis));
for n = 1:length(itis)
    [p_pcbyacc(1,n),~,st] = signrank(squeeze(ctrl_avgpcbyacc(use_ctrl&use_ctrl_acccmp,n,2)),...
        squeeze(ctrl_avgpcbyacc(use_ctrl&use_ctrl_acccmp,n,1)));
    st_pcbyacc(1,n) = st.signedrank;
    [p_pcbyacc(2,n),~,st] = signrank(squeeze(asd_avgpcbyacc(use_asd&use_asd_acccmp,n,2)),...
        squeeze(asd_avgpcbyacc(use_asd&use_asd_acccmp,n,1)));
    st_pcbyacc(2,n) = st.signedrank;
end
% title(sprintf('Controls: W = %3d, p = %.3f; ASD: W = %3d, p = %.3f',...
%     st_pcbyacc(1),p_pcbyacc(1),st_pcbyacc(2),p_pcbyacc(2)));

saveas(gcf,'fig/AllSbj_FD_biasbyprevcorr.png');

% Save the behavioral results
save('AllSbj_FD_behav','ctrl_md_rt','asd_md_rt','ctrl_iqr_rt','asd_iqr_rt','ctrl_propc','asd_propc','ctrl_dpr','asd_dpr',...
    'ctrl_diffdpr','asd_diffdpr','ctrl_diffpc','asd_diffpc','ctrl_rtslow','asd_rtslow',...
    'sbjs_ctrl','sbjs_asd','use_ctrl','use_asd');

%% ERP using 1-30 Hz filter

preprocpth = 'A:\PreprocessedEEG\Shahaf2017\';
% the above directory should include both controls and ASD

% Identify the channel to plot
chan_to_plot = {'Cz','Fz','Pz'};

iti_lbls = cell(length(itis),1);
for n = 1:length(itis)
    iti_lbls{n} = sprintf('%.1f s SOA',itis(n));
end

% Calculate the ERPs
fprintf('Controls: \n');
ctrl_erps = cell(length(sbjs_ctrl),1);
for s = 1:length(sbjs_ctrl)
    ctrl_erps{s} = cell(length(itis),1);
    preproc_fl = sprintf('%s_%s_midfreq',sbjs_ctrl{s},exp_condition);
    for n = 1:length(itis)
        triggers = [1 2]*10 + floor(itis(n)); % skips the first trial
        [ctrl_erps{s}(n),dly,~,chan_lbls] = calc_all_erps([preprocpth preproc_fl],...
            triggers,erp_range,baseline,reference,sound_delay);
    end
end
fprintf('ASD: \n');
asd_erps = cell(length(sbjs_asd),1);
for s = 1:length(sbjs_asd)
    asd_erps{s} = cell(length(itis),1);
    preproc_fl = sprintf('%s_%s_midfreq',sbjs_asd{s},exp_condition);
    for n = 1:length(itis)
        triggers = [1 2]*10 + floor(itis(n)); % skips the first trial
        [asd_erps{s}(n),dly,~,chan_lbls] = calc_all_erps([preprocpth preproc_fl],...
            triggers,erp_range,baseline,reference,sound_delay);
    end
end

% Plot the ERPs for 1-30 Hz
plot_mdsbj_cond_mderp(ctrl_erps,asd_erps,dly,itis,chan_to_plot,chan_lbls,exp_condition,...
    iti_lbls,'midfreq','spline_ds',3);
% Plot the ERPs for 1-30 Hz using the *average* across subjects (better
% comparison with Shahaf and Amos's plots
plot_avgsbj_cond_mderp(ctrl_erps,asd_erps,dly,itis,chan_to_plot,chan_lbls,exp_condition,...
    iti_lbls,'midfreq','spline_ds',3);
% 
% Plot just the first tone response
first_tone_range = [-100 400];
second_tone_range = [500 1000];
% get the ERPs just for the first and second tones
first_idx = dly>=first_tone_range(1) & dly<=first_tone_range(2);
second_idx = dly>=second_tone_range(1) & dly<=second_tone_range(2);
ctrl_firsttone = cell(length(sbjs_ctrl),1);
ctrl_secondtone = cell(length(sbjs_ctrl),1);
% ctrl_cmb = cell(length(sbjs_ctrl),1);
for s = 1:length(sbjs_ctrl)
    ctrl_firsttone{s} = cell(length(itis),1);
    ctrl_secondtone{s} = cell(length(itis),1);
    for n = 1:length(itis)
        ctrl_firsttone{s}{n} = ctrl_erps{s}{n}(first_idx,:,:);
        ctrl_secondtone{s}{n} = ctrl_erps{s}{n}(second_idx,:,:);
    end
%     ctrl_cmb{s} = {ctrl_firsttone{s}; ctrl_secondtone{s}};
end
asd_firsttone = cell(length(sbjs_asd),1);
asd_secondtone = cell(length(sbjs_asd),1);
% asd_cmb = cell(length(sbjs_asd),1);
for s = 1:length(sbjs_asd)
    asd_firsttone{s} = cell(length(itis),1);
    asd_secondtone{s} = cell(length(itis),1);
    for n = 1:length(itis)
        asd_firsttone{s}{n} = asd_erps{s}{n}(first_idx,:,:);
        asd_secondtone{s}{n} = asd_erps{s}{n}(second_idx,:,:);
    end
%     asd_cmb{s} = {asd_firsttone{s}; asd_secondtone{s}};
end
plot_mdsbj_cond_mderp(ctrl_firsttone,asd_firsttone,dly(first_idx),itis,chan_to_plot,chan_lbls,exp_condition,iti_lbls,'firsttone');
plot_mdsbj_cond_mderp(ctrl_secondtone,asd_secondtone,dly(second_idx),itis,chan_to_plot,chan_lbls,exp_condition,iti_lbls,'secondtone');
% 
% % Examine the difference in average voltages over front channels from
% % 25-75 ms
% p50_range = [25 75];
% plot_dlyavg(ctrl_firsttone,asd_firsttone,dly(first_idx),p50_range,front_chans,...
%     'P50, front channels (\muV)','firsttone_p50');
% 
% % Examine the difference in average voltages over back channels from
% % 250-350 ms
% p300_range = [250 350];
% plot_dlyavg(ctrl_firsttone,asd_firsttone,dly(first_idx),p300_range,back_chans,...
%     'P300, back channels (\muV)','firsttone_p300');
% 
% First N100 responses, adaptation?
n100_range = [70 150];
[ctrl_fn1,asd_fn1] = plot_dlyerppk_bycond(ctrl_firsttone,asd_firsttone,dly(first_idx),-1,n100_range,32,...
    'first_n100',iti_lbls,'Peak of 1st N100, Cz (\muV)');

% 2nd N100
[ctrl_sn1,asd_sn1] = plot_dlyerppk_bycond(ctrl_secondtone,asd_secondtone,dly(first_idx),-1,n100_range,32,...
    'second_n100',iti_lbls,'Peak of 2st N100, Cz (\muV)');

% Change in first to second N1
ctrl_n1diff = ctrl_fn1-ctrl_sn1;
asd_n1diff = asd_fn1-asd_sn1;
md_h = dot_connect_plot({ctrl_n1diff,asd_n1diff},iti_lbls,[ctrl_clr;asd_clr]);
% plot a dashed line for 0
plot([0.5 2.5],[0 0],'k--')
set(gcf,'Position',[200 200 750 300]);
ylabel([sprintf('Difference in N100 peak from\n 1st to 2nd') ' Cz (\muV)']);
legend(md_h,{ctrl_lg,asd_lg},'Location','northwest');
saveas(gcf,'fig\AllSbj_n1diff_dlyerppk.png');
p_n1diff = NaN(length(itis),1);
st_n1diff = NaN(length(itis),1);
for ii = 1:length(itis)
    [p_n1diff(ii),~,st] = ranksum(ctrl_n1diff(:,ii),asd_n1diff(:,ii));
    st_n1diff(ii) = st.zval;
    fprintf('Comparing difference in N100 peak, %.1f s ITI: z = %.3f, p = %.3f\n',itis(ii),st_n1diff(ii),p_n1diff(ii));
end

% First P200 responses, adaptation?
p200_range = [170 250];
[ctrl_fp2,asd_fp2] = plot_dlyerppk_bycond(ctrl_firsttone,asd_firsttone,dly(first_idx),1,p200_range,32,...
    'first_p200',iti_lbls,'Peak of 1st P200, Cz (\muV)');
% 
%% ERP using 0.05-30 Hz filter

erp_range_low = [-500 4000];
baseline_low = [-500 0];

fprintf('Controls: \n');
ctrl_erps_low = cell(length(sbjs_ctrl),1);
for s = 1:length(sbjs_ctrl)
    ctrl_erps_low{s} = cell(length(itis),1);
    preproc_fl = sprintf('%s_%s_lowfreq',sbjs_ctrl{s},exp_condition);
    for n = 1:length(itis)
        triggers = [1 2]*10 + floor(itis(n)); % skips the first trial
        [ctrl_erps_low{s}(n),dly,~,chan_lbls] = calc_all_erps([preprocpth preproc_fl],...
            triggers,erp_range_low,baseline,reference,sound_delay);
    end
end

fprintf('ASD: \n');
asd_erps_low = cell(length(sbjs_asd),1);
for s = 1:length(sbjs_asd)
    asd_erps_low{s} = cell(length(itis),1);
    preproc_fl = sprintf('%s_%s_lowfreq',sbjs_asd{s},exp_condition);
    for n = 1:length(itis)
        triggers = [1 2]*10 + floor(itis(n)); % skips the first trial
        [asd_erps_low{s}(n),dly,~,chan_lbls] = calc_all_erps([preprocpth preproc_fl],...
            triggers,erp_range_low,baseline,reference,sound_delay);
    end
end

plot_mdsbj_cond_mderp(ctrl_erps_low,asd_erps_low,dly,itis,chan_to_plot,chan_lbls,exp_condition,...
    iti_lbls,'lowfreq','spline_ds',3,'ylim',[-15 15]);

%%% Plot bias+/bias- difference in low-frequency ERP
ctrl_biasdiff = cell(length(sbjs_ctrl),1);
bdiff_spline_ds = 3;
for s = 1:length(sbjs_ctrl)
    ctrl_biasdiff{s} = cell(length(itis),1);
    for n = 1:length(itis)
        % Identify the reaction times for bias+ (1) and bias- (2)
        % trials
        %%% Note (3-10-2022): RORU8269 did not complete 9.5 s ITI block (last block),
        %%% so missing trials need to be accounted for
        ntr_this_sbj = size(ctrl_erps_low{s}{n},3);
        bp_idx = ctrl_stbias{s}(:,n)==1;
        bp_erp = ctrl_erps_low{s}{n}(:,:,bp_idx(1:ntr_this_sbj));
        bm_idx = ctrl_stbias{s}(:,n)==2;
        bm_erp = ctrl_erps_low{s}{n}(:,:,bm_idx(1:ntr_this_sbj));
        ctrl_biasdiff{s}{n} = spline_median(bp_erp,bdiff_spline_ds,3)-spline_median(bm_erp,bdiff_spline_ds,3);
    end
end
asd_biasdiff = cell(length(sbjs_asd),1);
for s = 1:length(sbjs_asd)
    asd_biasdiff{s} = cell(length(itis),1);
    for n = 1:length(itis)
        % Identify the reaction times for bias+ (1) and bias- (2)
        % trials
        bp_idx = asd_stbias{s}(:,n)==1;
        bp_erp = asd_erps_low{s}{n}(:,:,bp_idx);
        bm_idx = asd_stbias{s}(:,n)==2;
        bm_erp = asd_erps_low{s}{n}(:,:,bm_idx);
        asd_biasdiff{s}{n} = spline_median(bp_erp,bdiff_spline_ds,3)-spline_median(bm_erp,bdiff_spline_ds,3);
    end
end
plot_mdsbj_cond_mderp(ctrl_biasdiff,asd_biasdiff,dly,itis,...
    chan_to_plot,chan_lbls,exp_condition,iti_lbls,'lowfreq_biascmp',...
    'ylim',[-10 10],'spline_ds',bdiff_spline_ds,'leg_loc','northwest');

%% Get the ERP aligned to response time
rt_erp_range = [-1000 1000];
rt_baseline = [-400 -200];
% rt_baseline = [];
rt_dly_idx = floor(rt_erp_range(1)/1000*eFs):ceil(rt_erp_range(2)/1000*eFs);
ctrl_rt_erps_low = cell(length(sbjs_ctrl),1);
ctrl_rt_erps_low_nb = cell(length(sbjs_ctrl),1); % ERPs without re-baselining
for s = 1:length(sbjs_ctrl)
    ctrl_rt_erps_low{s} = cell(length(itis),1);
    ctrl_rt_erps_low_nb{s} = cell(length(itis),1);
    for n = 1:length(itis)
        ctrl_rt_erps_low{s}{n} = realign_erp_to_rt(ctrl_erps_low{s}{n},dly,eFs,...
            ctrl_rt{s}(2:end,n),rt_erp_range,sound_delay,rt_baseline);
        ctrl_rt_erps_low_nb{s}{n} = realign_erp_to_rt(ctrl_erps_low{s}{n},dly,eFs,...
            ctrl_rt{s}(2:end,n),rt_erp_range,sound_delay,[]);
    end
end
asd_rt_erps_low = cell(length(sbjs_asd),1);
asd_rt_erps_low_nb = cell(length(sbjs_asd),1);
for s = 1:length(sbjs_asd)
    asd_rt_erps_low{s} = cell(length(itis),1);
    asd_rt_erps_low_nb{s} = cell(length(itis),1);
    for n = 1:length(itis)
        asd_rt_erps_low{s}{n} = realign_erp_to_rt(asd_erps_low{s}{n},dly,eFs,...
            asd_rt{s}(2:end,n),rt_erp_range,sound_delay,rt_baseline);
        asd_rt_erps_low_nb{s}{n} = realign_erp_to_rt(asd_erps_low{s}{n},dly,eFs,...
            asd_rt{s}(2:end,n),rt_erp_range,sound_delay,[]);
    end
end
allsbj_rt_dly = rt_dly_idx/eFs*1000;
plot_mdsbj_cond_mderp(ctrl_rt_erps_low,asd_rt_erps_low,allsbj_rt_dly,itis,...
    chan_to_plot,chan_lbls,exp_condition,iti_lbls,'lowfreq_rel_rt',...
    'leg_loc','southeast','spline_ds',3);
plot_mdsbj_cond_mderp(ctrl_rt_erps_low_nb,asd_rt_erps_low_nb,allsbj_rt_dly,itis,...
    chan_to_plot,chan_lbls,exp_condition,iti_lbls,'lowfreq_rel_rt_nb',...
    'leg_loc','northwest','spline_ds',3);

% Plot by correct/incorrect
corr_val = [1 0];
ctrl_rt_corrdiff = cell(length(sbjs_ctrl),1);
spline_ds = 3;
for s = 1:length(sbjs_ctrl)
    ctrl_rt_corrdiff{s} = cell(length(itis),1);
    for n = 1:length(itis)
        % Identify the reaction times for correct (1) or incorrect (0)
        % trials
        corr_idx = ctrl_acc{s}(:,n)==corr_val(1);
%         rt_corr = ctrl_rt{s}(corr_idx,n);
        corr_erp = ctrl_rt_erps_low{s}{n}(:,:,corr_idx);
        incorr_idx = ctrl_acc{s}(:,n)==corr_val(2);
%         rt_incorr = ctrl_rt{s}(incorr_idx,n);
        incorr_erp = ctrl_rt_erps_low{s}{n}(:,:,incorr_idx);
        ctrl_rt_corrdiff{s}{n} = spline_median(incorr_erp,spline_ds,3)-spline_median(corr_erp,spline_ds,3);
    end
end
asd_rt_corrdiff = cell(length(sbjs_asd),1);
for s = 1:length(sbjs_asd)
    asd_rt_corrdiff{s} = cell(length(itis),1);
    for n = 1:length(itis)
        % Identify the reaction times for correct (1) or incorrect (0)
        % trials
        corr_idx = asd_acc{s}(:,n)==corr_val(1);
%         rt_corr = ctrl_rt{s}(corr_idx,n);
        corr_erp = asd_rt_erps_low{s}{n}(:,:,corr_idx);
        incorr_idx = asd_acc{s}(:,n)==corr_val(2);
%         rt_incorr = ctrl_rt{s}(incorr_idx,n);
        incorr_erp = asd_rt_erps_low{s}{n}(:,:,incorr_idx);
        asd_rt_corrdiff{s}{n} = spline_median(incorr_erp,spline_ds,3)-spline_median(corr_erp,spline_ds,3);
    end
end
plot_mdsbj_cond_mderp(ctrl_rt_corrdiff,asd_rt_corrdiff,allsbj_rt_dly,itis,...
    chan_to_plot,chan_lbls,exp_condition,iti_lbls,'lowfreq_rt_corrcmp',...
    'ylim',[-10 10],'spline_ds',3,'leg_loc','northwest');

