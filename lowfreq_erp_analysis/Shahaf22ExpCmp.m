% Load the behavioral results from both the 2s/2s new experiment and the
% two-tone task from Shahaf's experiment, and compare the short-term bias
% of ASD subjects
%%% Need to update to account for rejected subjects
% Nate Zuk (2022)

% Load the results from both tasks (note: AllSbjReport_22.m and
% AllSbjReport_FD.m must be run first to calculate and save the d-prime
% differences)
fd_shahaf = load('C:\Users\zsxbo\Projects\preprocessing-two-tone-task-data\lowfreq_erp_analysis\AllSbj_FD_behav');
fd_22 = load('C:\Users\zsxbo\Projects\erp-adaptation\eeg_experiments\analysis_22\AllSbj_22_behav');

% Rename TSWE6664 in 22, this was the second time they did that task
tswe_idx = strcmp(fd_22.sbjs_asd,'TSWE6664-2');
fd_22.sbjs_asd{tswe_idx} = 'TSWE6664';

itis = [2 3.5 6.5 9.5] - 0.6; % (10-11-2022) Use the true ITI, not SOA
asd_clr = [0 0.7 0];

% Compare control d-prime difference between experiments
p_ctrl_expdpr = NaN(4,1); u_ctrl_expdpr = NaN(4,1);
for n = 1:4
    [p_ctrl_expdpr(n),~,st_ctrl_expdpr] = ...
        ranksum(fd_shahaf.ctrl_diffdpr(fd_shahaf.use_ctrl,n),fd_22.ctrl_diffdpr(fd_22.use_ctrl));
    u_ctrl_expdpr(n) = st_ctrl_expdpr.ranksum;
end

% Compare ASD d-prime difference between experiments
p_asd_expdpr = NaN(4,1); u_asd_expdpr = NaN(4,1);
for n = 1:4
    [p_asd_expdpr(n),~,st_asd_expdpr] = ...
        ranksum(fd_shahaf.asd_diffdpr(fd_shahaf.use_asd,n),fd_22.asd_diffdpr(fd_22.use_asd));
    u_asd_expdpr(n) = st_asd_expdpr.ranksum;
end

% Compare specific control subjects with signed rank test who performed both
% experiments
% [ctrl_both_exp,idx_shahaf,idx_22] = intersect(fd_shahaf.sbjs_ctrl,fd_22.sbjs_ctrl);
% % show the difference in d-prime prediction accuracy between experiments
% dprchange_pairedcmp = NaN(length(idx_shahaf),2);
% for ii = 1:length(idx_shahaf)
%     dprchange_pairedcmp(ii,1) = fd_22.ctrl_diffdpr(idx_22(ii)) - fd_shahaf.ctrl_diffdpr(idx_shahaf(ii),1);
%     dprchange_pairedcmp(ii,2) = fd_22.ctrl_diffdpr(idx_22(ii)) - fd_shahaf.ctrl_diffdpr(idx_shahaf(ii),2);
%     fprintf('Change in st dpr diff, %s: %.1f s ITI = %.3f, %.1f s ITI = %.3f\n',...
%         ctrl_both_exp{ii},itis(1),dprchange_pairedcmp(ii,1),itis(2),dprchange_pairedcmp(ii,2));
% end
% figure
% plot([1 2 3],[fd_shahaf.ctrl_diffdpr(idx_shahaf,1:2) fd_22.ctrl_diffdpr(idx_22)],'o-',...
%     'Color',[0 0 1],'LineWidth',2,'MarkerSize',10);
% set(gca,'FontSize',14,'XTick',1:3,'XTickLabel',{'2 s ITI','3.5 s ITI','2s/2s'},...
%     'XTickLabelRotation',45);
% ylabel('d''_{st,bias+} - d''_{st,bias-}');

% Compare specific ASD subjects with signed rank test who performed both
% experiments
[asd_both_exp,idx_shahaf,idx_22] = intersect(fd_shahaf.sbjs_asd(fd_shahaf.use_asd),fd_22.sbjs_asd(fd_22.use_asd));
% remove diffdpr values that are from rejected subjects
%%% try stat using diffpc instead of diffdpr
figure
set(gcf,'Position',[100 100 450 750]);
% d-prime based
subplot(2,1,1);
hold on
ttdiffdpr = fd_22.asd_diffdpr(fd_22.use_asd);
fddiffdpr = fd_shahaf.asd_diffdpr(fd_shahaf.use_asd,:);
% show the difference in d-prime prediction accuracy between experiments
dprchange_pairedcmp = NaN(length(idx_shahaf),2);
for ii = 1:length(idx_shahaf)
    dprchange_pairedcmp(ii,1) = ttdiffdpr(idx_22(ii)) - fddiffdpr(idx_shahaf(ii),1);
    dprchange_pairedcmp(ii,2) = ttdiffdpr(idx_22(ii)) - fddiffdpr(idx_shahaf(ii),2);
    fprintf('Change in st dpr diff, %s: %.1f s ITI = %.3f, %.1f s ITI = %.3f\n',...
        asd_both_exp{ii},itis(1),dprchange_pairedcmp(ii,1),itis(2),dprchange_pairedcmp(ii,2));
end
plot([1 2 3],[fddiffdpr(idx_shahaf,1:2) ttdiffdpr(idx_22)],'o-',...
    'Color',asd_clr*0.5+0.5,'LineWidth',1,'MarkerSize',10);
plot([1 2 3],median([fddiffdpr(idx_shahaf,1:2) ttdiffdpr(idx_22)]),'o',...
    'Color',asd_clr,'LineWidth',3,'MarkerSize',10);
set(gca,'FontSize',14,'XTick',1:3,'XTickLabel',{'1.4 s ITI','2.9 s ITI','2s/2s'},...
    'XTickLabelRotation',45);
ylabel('d''_{st,bias+} - d''_{st,bias-}');

% Is the change in paired comparisons significant?
[p_paired(1),~,st_paired(1)] = signrank(dprchange_pairedcmp(:,1));
fprintf('Cmp to 2 s SOA: W = %.3f, p = %.3f\n',st_paired(1).signedrank,p_paired(1));
[p_paired(2),~,st_paired(2)] = signrank(dprchange_pairedcmp(:,2));
fprintf('Cmp to 3.5 s SOA: W = %.3f, p = %.3f\n',st_paired(2).signedrank,p_paired(2));

% PC based
subplot(2,1,2);
hold on
ttdiffpc = fd_22.asd_diffpc(fd_22.use_asd);
fddiffpc = fd_shahaf.asd_diffpc(fd_shahaf.use_asd,:);
% show the difference in d-prime prediction accuracy between experiments
pcchange_pairedcmp = NaN(length(idx_shahaf),2);
for ii = 1:length(idx_shahaf)
    pcchange_pairedcmp(ii,1) = ttdiffpc(idx_22(ii)) - fddiffpc(idx_shahaf(ii),1);
    pcchange_pairedcmp(ii,2) = ttdiffpc(idx_22(ii)) - fddiffpc(idx_shahaf(ii),2);
    fprintf('Change in st pc diff, %s: %.1f s ITI = %.3f, %.1f s ITI = %.3f\n',...
        asd_both_exp{ii},itis(1),pcchange_pairedcmp(ii,1),itis(2),pcchange_pairedcmp(ii,2));
end
plot([1 2 3],[fddiffpc(idx_shahaf,1:2) ttdiffpc(idx_22)],'o-',...
    'Color',asd_clr*0.5+0.5,'LineWidth',1,'MarkerSize',10);
plot([1 2 3],median([fddiffpc(idx_shahaf,1:2) ttdiffpc(idx_22)]),'o',...
    'Color',asd_clr,'LineWidth',3,'MarkerSize',10);
set(gca,'FontSize',14,'XTick',1:3,'XTickLabel',{'1.4 s ITI','2.9 s ITI','2s/2s'},...
    'XTickLabelRotation',45);
ylabel('PC_{st,bias+} - PC_{st,bias-}');

saveas(gcf,'fig/Shahaf22_indivsbj_biascmp.png');

% Plot performance across experiments
figure
hold on
ttpc = fd_22.asd_propc(fd_22.use_asd);
fdpc = fd_shahaf.asd_propc(fd_shahaf.use_asd,:);
plot([1 2 3],[fdpc(idx_shahaf,1:2) ttpc(idx_22)]*100,'o-',...
    'Color',asd_clr*0.5+0.5,'LineWidth',1,'MarkerSize',10);
plot([1 2 3],median([fdpc(idx_shahaf,1:2) ttpc(idx_22)]*100),'o',...
    'Color',asd_clr,'LineWidth',3,'MarkerSize',10);
set(gca,'FontSize',14,'XTick',1:3,'XTickLabel',{'1.4 s ITI','2.9 s ITI','2s/2s'},...
    'XTickLabelRotation',45);
ylabel('% correct');
