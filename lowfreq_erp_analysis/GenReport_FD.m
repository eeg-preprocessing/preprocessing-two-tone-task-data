% Generate a report containin the summary information for the FD_jitter.m
% experiment (two-tone discrimination used for several years in the lab)
% Nate Zuk (2021)

% Necessary paths
addpath('..');
addpath('../..');
addpath('../behav_analysis/');
addpath(genpath('C:\Users\natha\Projects\EEG-analysis-tools'));
U = userpath;
addpath([U '\fieldtrip-20210614\']);

% Parameters
sbj = 'ITKI6815';
exp_condition = 'active';
sound_delay = 50; % amount of time between the trigger and the stimulus (in ms)
eFs = 256; % sampling rate of the EEG (needed for realigning EEG to RT
isi = 600; % inter-stimulus interval in a trial (in ms)
erp_range = [-500 5000]; % range of delays to use to get the ERP (in ms)
% baseline = [];
baseline = [-500 0]; % range of delays to use as the baseline
spline_ds = 3; % downsampling factor for the splines (at Fs=256, 3 produces a cutoff of ~30 Hz)
% Apply mastoids reference
reference = 'mastoids';
% Preprocessing path
preprocpth = 'A:\PreprocessedEEG\Shahaf2017\';
% itis = [2 3.5 6.5 9.5]; % inter-trial-onset intervals in each block

%% Subject performance
behavpth = 'A:\Behavior\Shahaf2017\';
[ncorr,~,nmiss,tottr,acc] = calc_fd_perf(sbj,behavpth);

% Display the percent correct
propc = ncorr/tottr;
fprintf('Performance: %.1f%% correct\n',propc*100);

% Display the number of misses
fprintf('%d / %d misses\n',nmiss,tottr);

% Calculate d-prime difference between short-term bias+ and bias-
% conditions
[dpr,propcorr,~,itis,stbias,iti_order] = calculate_prevtrial_dpr_bias(sbj,behavpth);
disp('D-prime difference between short-term bias+/bias-:');
for n = 1:length(itis)
    fprintf('%.1f s ITI: %.3f\n',itis(n),dpr(n,1)-dpr(n,2));
end
disp('Prop. corr. difference between short-term bias+/bias-:');
for n = 1:length(itis)
    fprintf('%.1fs ITI: %.3f\n',itis(n),propcorr(n,1)-propcorr(n,2));
end

% iti_order is the order that each ITI was presented
% To rearrange the behavioral data so that it is ordered by ITI, use
% [~,idx] = sort(iti_order), which gets the place that each block should
% be to be ordered by ITI
% [~,blk_by_iti] = sort(iti_order);

% Show the median response time
rt = get_fd_rt(sbj,behavpth);
% rearrange the reaction time by iti
rt_iti = reshape(rt,[length(rt)/length(itis) length(itis)]);
rt_iti_ordered = rt_iti(:,iti_order);
for n = 1:length(itis)
    fprintf('Response time (rel 2nd tone), %.1f s ITI: %.1f ms\n',itis(n),median(rt_iti_ordered(:,n),'omitnan')-600-sound_delay);
end

% Rearrange the accuracy array so columns are ordered by iti
ntrials_per_block = 101;
acc = reshape(acc,[length(acc)/length(itis) length(itis)]);
acc = acc(2:end,iti_order); % rearrange by itis, and remove the first row (first trial, before inter-trial intervals)

%% ERP using 1-30 Hz filter

preproc_fl = sprintf('%s_%s_midfreq',sbj,exp_condition);
midfreq_erprange = [-500 1500];

% Identify the channel to plot
chan_to_plot = {'Cz','Fz','Pz'};

% Calculate the ERPs
all_erps = cell(length(itis),1);
itis_lbl = cell(length(itis),1);
for n = 1:length(itis)
    triggers = [1 2]*10 + floor(itis(n)); % skips the first trial
    [all_erps(n),dly,~,chan_lbls] = calc_all_erps([preprocpth preproc_fl],triggers,midfreq_erprange,...
        baseline,reference,sound_delay);
    itis_lbl{n} = sprintf('%.1f s ITI',itis(n));
end

% Calculate the ERPs
% triggers = 1:3;
% [all_erps,dly,~,chan_lbls,null_erp] = calc_all_erps([preprocpth preproc_fl],...
%     triggers,erp_range,baseline,reference,sound_delay);
% 
% % Calculate the ratio of the average N1 to the average of the noise
% dly_idx = dly>=0 & dly<=250; % get peak to peak for N1 & P2
% n1 = squeeze(min(all_erps{1}(dly_idx,32,:)));
% fn1 = squeeze(min(null_erp{1}(dly_idx,32,:)));
% fprintf('Cz: Mean N1 to noise std = %.3f\n',mean(n1)/std(fn1));
% fprintf('Noise mean N1 to noise std = %.3f\n',mean(fn1)/std(fn1));

% Plot the ERPs for 1-30 Hz
plot_cond_mderp(all_erps,dly,itis,chan_to_plot,chan_lbls,sbj,exp_condition,...
    itis_lbl,'midfreq',[0 isi median(rt,'omitnan')-sound_delay],'spline_ds',3);

%% ERP using 0.05-30 Hz filter

lowfreq_fl = sprintf('%s_%s_lowfreq',sbj,exp_condition);

% Identify the channel to plot
chan_to_plot = {'Cz','Fz','Pz'};

% Calculate the ERPs
all_erps_low = cell(length(itis),1);
% itis_lbl = cell(length(itis),1);
for n = 1:length(itis)
    triggers = [1 2]*10 + floor(itis(n)); % skips the first trial
    [all_erps_low(n),dly,~,chan_lbls] = calc_all_erps([preprocpth lowfreq_fl],triggers,erp_range,...
        baseline,reference,sound_delay);
%     itis_lbl{n} = sprintf('%.1f s ITI',itis(n));
end

% zero-center the ERPs
% all_erps_low{1} = detrend(all_erps_low{1},0);

% Plot the ERPs for 0.05-30 Hz
% updated to plot each ITI separately
plot_cond_mderp(all_erps_low,dly,itis,chan_to_plot,chan_lbls,sbj,exp_condition,...
    itis_lbl,'lowfreq',[0 isi median(rt,'omitnan')-sound_delay],'spline_ds',3);

% Plot ERPs as a function of response time
% concatenate all trials
ntr = numel(rt_iti_ordered);
nchan = size(all_erps_low{1},2);
RT = reshape(rt_iti_ordered,[ntr,1])-sound_delay; % adjust for delay of sound rel to trigger
ERPS_LOW = NaN(length(dly),nchan,ntr);
for n = 1:length(itis)
    % need to account for skipped first trial in each set, which had
    % trigger 35
    idx = 1 + (1:size(all_erps_low{n},3)) + (n-1)*(size(all_erps_low{n},3)+1);
    ERPS_LOW(:,:,idx) = all_erps_low{n};
end
plot_erpbyval({ERPS_LOW},RT,dly,chan_to_plot,chan_lbls,'Response time (ms)',sbj,exp_condition,...
    'lowfreq_rt','crange',[-50 50],'show_vals',true);

% Show the topography of the signal around 3 s
% late_pos_range = [2500 3500];
% make_avgeeg_topoplot(all_erps_low{1},dly,late_pos_range,sbj,exp_condition,'lowfreq');

% Plot by bias+ and bias-
%%% Possibly plot the difference in bias instead, so that all ITIs can be
%%% shown overlaid (decreasing bias effect with ITI)
b_lbl = {'Bias+','Bias-'}; % for labeling Bias+ and Bias-
bias_erps_low = cell(length(itis),2);
% bias_lbls = cell(2,length(itis));
% % cnd_vals = NaN(2,length(itis));
for n = 1:length(itis)
    for ii = 1:2
        bias_erps_low{n,ii} = all_erps_low{n}(:,:,stbias(:,n)==ii);
%         bias_lbls{ii,n} = sprintf('%s, %.1f s ITI',b_lbl{ii},itis(n));
        % use the trigger values as condition values (for plot_cond_mderp)
%         cnd_vals(ii,n) = floor(itis(n))*10 + ii;
    end
%     plot_cond_mderp(bias_erps_low(:,n),dly,[1 2],chan_to_plot,chan_lbls,sbj,exp_condition,...
%         bias_lbls(:,n),sprintf('lowfreq_bias_%diti',floor(itis(n)),[0 isi median(rt,'omitnan')]));
end
% Plot separate conditions as dashed lines
plot_condsep_mderp(bias_erps_low,dly,chan_to_plot,chan_lbls,sbj,exp_condition,...
    itis_lbl,{'Bias+','Bias-'},'lowfreq_bias',[0 isi median(rt,'omitnan')-sound_delay],'spline_ds',3);
% Plot the difference between conditions
plot_conddiff_mderp(bias_erps_low,dly,chan_to_plot,chan_lbls,sbj,exp_condition,...
    itis_lbl,'lowfreq_bias',[0 isi median(rt,'omitnan')-sound_delay],'spline_ds',3);
% cnd_vals = reshape(cnd_vals,[length(itis)*2 1]);
% bias_lbls = reshape(bias_lbls,[length(itis)*2 1]);
% bias_erps_low = reshape(bias_erps_low,[length(itis)*2 1]);
% plot_cond_mderp(bias_erps_low,dly,cnd_vals,chan_to_plot,chan_lbls,sbj,exp_condition,...
%     bias_lbls,'lowfreq_bias',[0 isi median(rt,'omitnan')]);

% Plot by correct/incorrect
corrincorr_lbl = {'Correct','Incorrect'};
corr_erps_low = cell(length(itis),2);
% corr_lbls = cell(2,length(itis));
corr_val = [1 0];
for n = 1:length(itis)
    for ii = 1:2
        corr_erps_low{n,ii} = all_erps_low{n}(:,:,acc(:,n)==corr_val(ii));
%         corr_lbls{ii,n} = sprintf('%s, %.1f s ITI',corrincorr_lbls{ii},itis(n));
    end
end
% corr_erps_low = reshape(corr_erps_low,[length(itis)*2 1]);
% plot_cond_mderp(corr_erps_low,dly,[1 2],chan_to_plot,chan_lbls,sbj,exp_condition,...
%     corr_lbls,'lowfreq_corr',[0 isi median(rt,'omitnan')]);
plot_condsep_mderp(corr_erps_low,dly,chan_to_plot,chan_lbls,sbj,exp_condition,...
    itis_lbl,{'Correct','Incorrect'},'lowfreq_corr',[0 isi median(rt,'omitnan')-sound_delay],'spline_ds',3);
plot_conddiff_mderp(corr_erps_low,dly,chan_to_plot,chan_lbls,sbj,exp_condition,...
    itis_lbl,'lowfreq_corr',[0 isi median(rt,'omitnan')-sound_delay],'spline_ds',3);

%% Realign ERPs to RT
%%% (Copied from GenReport_22)
rt_erp_range = [-1000 1000];
rt_baseline = [-400 -200]; % baseline the ERPs relative to RT (in ms)
% Calculate correct/incorrect ERPs aligned to response time
rt_corr_erps = cell(length(itis),2);
rt_bias_erps = cell(length(itis),2);
for ii = 1:2
    for n = 1:length(itis) % for each iti
        % Identify the reaction times for correct (1) or incorrect (2)
        % trials
        aidx = find(acc(:,n)==corr_val(ii)); % get the indexes for this condition
        % rt_iti_ordered contains the first trial, while acc and stbias do
        % not. This should be skipped
        rt_corr = rt_iti_ordered(aidx+1,n);
        rt_corr_erps{n,ii} = realign_erp_to_rt(corr_erps_low{n,ii},dly,eFs,...
            rt_corr,rt_erp_range,sound_delay,rt_baseline);
        % Get reaction times for bias+ and bias- trials
        bidx = find(stbias(:,n)==ii);
        rt_bias = rt_iti_ordered(bidx+1,n);
        rt_bias_erps{n,ii} = realign_erp_to_rt(bias_erps_low{n,ii},dly,eFs,...
            rt_bias,rt_erp_range,sound_delay,rt_baseline);
    end
end
% compute the delay vector
rt_dly_idx = floor(rt_erp_range(1)/1000*eFs):ceil(rt_erp_range(2)/1000*eFs);
rt_dly = rt_dly_idx/eFs*1000;

% Plot Bias+/Bias- ERPs aligned to RT
plot_condsep_mderp(rt_bias_erps,rt_dly,chan_to_plot,chan_lbls,sbj,exp_condition,...
    itis_lbl,{'Bias+','Bias-'},'lowfreq_rt_bias',[],'spline_ds',3);
% Plot the difference between conditions
plot_conddiff_mderp(rt_bias_erps,rt_dly,chan_to_plot,chan_lbls,sbj,exp_condition,...
    itis_lbl,'lowfreq_rt_bias',[],'spline_ds',3);

% Plot Correct/Incorrect ERPs aligned to RT
plot_condsep_mderp(rt_corr_erps,rt_dly,chan_to_plot,chan_lbls,sbj,exp_condition,...
    itis_lbl,{'Correct','Incorrect'},'lowfreq_rt_corr',[],'spline_ds',3);
% Plot the difference between conditions
plot_conddiff_mderp(rt_corr_erps,rt_dly,chan_to_plot,chan_lbls,sbj,exp_condition,...
    itis_lbl,'lowfreq_rt_corr',[],'spline_ds',3);