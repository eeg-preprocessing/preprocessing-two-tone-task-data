% Load each of the preprocessed BDF files and examine the magnitude of the
% electrical artifact topographically


preproc_path = '/Volumes/NO NAME/PreprocessedEEG/';
exp_labels = {'sagi2015','Shahaf2017','Shahaf2020','Shahaf2021','test_eeg_quality'};
exp_conditions = {'passive','active'};

% electrical noise range of frequenices
elec_frq = [45 55];

elec_topos = cell(1,length(exp_labels));
for n = 1:length(exp_labels)
    fprintf('-- %s --\n',exp_labels{n});
    sbjs = get_subjects_from_mats([preproc_path exp_labels{n}]);
    elec_topos{n} = NaN(32,length(sbjs));
    for s = 1:length(sbjs)
        % load the eeg data
        fl_pas = sprintf('%s_%s',sbjs{s},exp_conditions{1});
        d_pas = load([preproc_path exp_labels{n} '/' fl_pas]);
        % Compute the spectrum
        for c = 1:size(d_pas.eeg,2)
            [Pw,frq] = pwelch(d_pas.eeg(:,c),d_pas.eFs,[],[],d_pas.eFs);
            % Get the energy on average at the electrical noise frequency
            use_frq = frq>=elec_frq(1) & frq<=elec_frq(2);
            elec_topos{n}(c,s) = mean(Pw(use_frq));
        end
        % display the session name
        fprintf('%s\n',sbjs{s});
    end
end

% Plot the average topographies for each experiment label
figure
for n = 1:length(exp_labels)
    subplot(1,length(exp_labels),n);
    topoplot(mean(elec_topos{n},2),'chanlocs_32.xyz');
    colorbar;
    caxis([0 max(mean(elec_topos{n},2))]);
    set(gca,'FontSize',12);
    title(exp_labels{n},'Interpreter','none');
end