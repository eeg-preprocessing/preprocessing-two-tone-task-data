function plot_rndsmpl_erps_by_exp(erps,dly,ch_idx,iti_idx,cond_labels,ch_labels,...
    iti_values,exp_labels,nsmpl,nperm)
% For each experiment, randomly sample a subset of sessions when computing the
% average ERP. Repeat this for some number of permutations, and plot the mean
% and 95% quantiles of those permutations. Each channel and ITI is plotted in its own
% figure. Conditions and experiments are plotted in a ncond x nexp subplot in each
% figure.
% Inputs:
% - erps = a cell array of dimensions: conditions x experiments. Each
%       cell contains a 4D array with dimensions: delay x channels x ITI x
%       session
% - dly = array of ERP delays
% - ch_idx = set of indexes for the channels that should be plotted
% - iti_idx = set of indexes for the ITIs that should be plotted
% - cond_labels = labels for each condition (cell array)
% - ch_labels = labels for each channel (cell array)
% - iti_values = values for each ITI (numeric array)
% - exp_labels = labels for each experiment (cell array)
% - nsmpl = number of sessions to sample
% - nperm (optional) = number of resampling permutations (default: 1000)
% Nate Zuk (2021)

ncond = length(cond_labels);
nexp = length(exp_labels);

if nargin < 10
    nperm = 1000;
end

% setup the color map
for n = 1:length(iti_idx)
    for c = 1:length(ch_idx)
        % Plot for this pair of channel x iti
        figure;
        for jj = 1:nexp
            for ii = 1:ncond
                % identify the correct subplot to use
%                 sbplt_idx = (ii-1)*ncond+jj;
                sbplt_idx = (ii-1)*nexp+jj;
                subplot(ncond,nexp,sbplt_idx);
                hold on;
                % get the ERPs for this specific condition, experiment,
                % channel, and ITI
                sp_erp = squeeze(erps{ii,jj}(:,ch_idx(c),iti_idx(n),:));
                % Iteratively resample ERPs across sessions and average 
                nsbj = size(sp_erp,2); % get the number of sessions
                boot_erp = NaN(length(dly),nperm);
                for b = 1:nperm
                    rnd_sbj = randperm(nsbj,nsmpl);
                    boot_erp(:,b) = mean(sp_erp(:,rnd_sbj),2);
                end
                % plot the mean and 95% quantiles of the sampling
                hold on
                plot(dly,mean(boot_erp,2),'k','LineWidth',3); % mean value
                plot(dly,quantile(boot_erp,0.05,2),'k','LineWidth',1.5); % lower quantile
                plot(dly,quantile(boot_erp,0.95,2),'k','LineWidth',1.5); % upper quantile
                set(gca,'FontSize',14,'YLim',[-6 6]);
                % Labeling
                xlabel('Time (ms)');
                ylabel('Average ERP (\muV)');
                % Create the title
                tle = sprintf('%s, %s; %s, %.1f s ITI',exp_labels{jj},cond_labels{ii},...
                    ch_labels{c},iti_values(n));
                title(tle);
            end
        end
    end
end