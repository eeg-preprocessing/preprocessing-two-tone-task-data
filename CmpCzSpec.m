% Load each of the preprocessed BDF files and compare the spectra at
% electrode Cz (after eyeblink removal)

preproc_path = '/Volumes/NO NAME/PreprocessedEEG/';
exp_labels = {'sagi2015','Shahaf2017','Shahaf2020','Shahaf2021','test_eeg_quality'};
exp_conditions = {'passive','active'};
exp_cond_to_plot = 2; % which condition to plot

% array of frequencies
frq = (0:256)/2; % Fs = 256 Hz

spCz = cell(1,length(exp_labels));
for n = 1:length(exp_labels)
    fprintf('-- %s --\n',exp_labels{n});
    sbjs = get_subjects_from_mats([preproc_path exp_labels{n}]);
    spCz{n} = NaN(length(frq),length(sbjs));
    for s = 1:length(sbjs)
        % load the eeg data
        fl_act = sprintf('%s_%s',sbjs{s},exp_conditions{exp_cond_to_plot});
        d_act = load([preproc_path exp_labels{n} '/' fl_act]);
        % Compute the spectrum
%         spCz{n}(:,s) = d_act.spCZ_afterrmv;
        cz = d_act.eeg(:,32) - mean(d_act.mastoids,2); % reference to mastoids
        [Cz,frq] = pwelch(cz,2*d_act.eFs,[],[],d_act.eFs);
        spCz{n}(:,s) = Cz;
        % display the session name
        fprintf('%s\n',sbjs{s});
    end
end

%% Plotting
figure
hold on
cmap = colormap('jet');
cmap = cmap*0.8;
cz_plt = NaN(length(exp_labels),1);
for n = 1:length(exp_labels)
    cidx = get_color_idx(n,length(exp_labels),cmap);
    % plot the average
    cz_plt(n) = plot(frq,mean(log10(spCz{n})*20,2),'Color',cmap(cidx,:),'LineWidth',2);
    % plot +/- 2STD
    plot(frq,mean(log10(spCz{n})*20,2)+2*std(log10(spCz{n})*20,[],2),...
        'Color',cmap(cidx,:),'LineWidth',0.5);
    plot(frq,mean(log10(spCz{n})*20,2)-2*std(log10(spCz{n})*20,[],2),...
        'Color',cmap(cidx,:),'LineWidth',0.5);
end
set(gca,'FontSize',14,'XScale','log','XLim',[0.5 128]);
xlabel('Frequency (Hz)');
ylabel('Power spectral density (dB)');
legend(cz_plt,exp_labels,'Interpreter','none');