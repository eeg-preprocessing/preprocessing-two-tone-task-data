function [vexp_cv,vexp_null] = cv_pca(x)
% Run PCA with bootstrapped cross-validation to evaluate the contribution
% of each of the principal components. This is done by:
% 1) Randomly splitting the data into training and testing samples
% 2) Running PCA on the training data to get a transformation
% 3) Calculating the variance explained for the testing data
% This operation is also run on a shuffled version of x where for each
% sample (row of x) the columns of x are randomized.
% Nate Zuk (2022)

prop_test = 0.8; % proportion of data to retain for testing
niter = 100; % number of iterations

% nsamples = size(x,1);
nfeat = size(x,2);

% zero-center the columns of x
% x = detrend(x,0);

fprintf('Calculating CV variance explained (%d iterations)',niter);
vexp_cv = NaN(nfeat,niter);
for n = 1:niter
    if mod(n,10)==0, fprintf('.'); end
    vexp_cv(:,n) = do_pca_with_validate(x,prop_test);
end
fprintf('\n');

fprintf('Calculate null variance explained (%d iterations)',niter);
vexp_null = NaN(nfeat,niter);
for n = 1:niter
    if mod(n,10)==0, fprintf('.'); end
    % randomize columns
    nullx = NaN(size(x));
    for ii = 1:size(x,1)
        nullx(ii,:) = x(ii,randperm(size(x,2)));
    end
    vexp_null(:,n) = do_pca_with_validate(nullx,prop_test);
end
fprintf('\n');

function vexp = do_pca_with_validate(x,prop_test)
nsamples = size(x,1);
% get the testing samples
test_idx = randperm(nsamples,round(nsamples*prop_test));
% the trainingamples are the rest of the samples
train_idx = setxor(1:nsamples,test_idx);
% zero-center the training and testing data separately
xtrain = detrend(x(train_idx,:),0);
xtest = detrend(x(test_idx,:),0);

% run pca on the training samples
[~,~,cf] = svd(xtrain);

% calculate the variance explained using the testing data
D = cf'*(xtest'*xtest)*cf;
lmb = diag(D);
vexp = lmb/sum(lmb);