% Load the evoked responses for each subject in each experiment group, and
% calculate the peak-to-peak amplitude in the passive and active conditions
% (N1 -> P2 for a delay range of 0-300 ms)
% (21-06-2021): This version plots the peak-to-peak amplitudes by session
% date.
% Nate Zuk (2021)

% erppth = 'A:\ERP\';
erppth = '/Volumes/NO NAME/ERP/';
exp_labels = {'sagi2015','Shahaf2017','Shahaf2020','Shahaf2021','test_eeg_quality'};
exp_conditions = {'passive','active'};
% sound_delays = [50 60]; % indicates the time between trigger and sound onset for each experiment
erp_range = [0 300]; % range of delays to use to get the ERP (in ms)
% baseline = [-100 0]; % range of delays to use as the baseline
iti = 9.5; % ITIs for each condition (the floor of these values is 
    % the second number in each trigger value)
% Apply reference (can select either nose or mastoids)
reference = 'mastoids';
% Identify the channel to plot
chan_to_plot = {'Cz','Fz'};
chan_idx = [32 31]; % indexes in the ERP matrix corresponding to these channels

peak_to_peak = cell(length(exp_labels),1);
all_sbjs = cell(length(exp_labels),1);
all_dates = cell(length(exp_labels),1);
for n = 1:length(exp_labels)
    fprintf('-- %s\n',exp_labels{n});
    % Get all subjects that were preprocessed
    sbjs = get_subjects_from_mats([erppth exp_labels{n} '/']);
    % Setup the peak-to-peak array
    peak_to_peak{n} = NaN(length(sbjs),length(exp_conditions),length(chan_to_plot));
    % Iterate through each subject
    for s = 1:length(sbjs)
        % Get the subject's ERP
        fl = sprintf('%s_ref-%s',sbjs{s},reference);
        res = load([erppth exp_labels{n} '/' fl]);
        erps = res.ERPs;
        % Get the index for the ITI
        iti_idx = res.itis==iti;
        % Get the peak-to-peak magnitudes for each channel and each
        % condition
        for ii = 1:length(chan_to_plot)
            for jj = 1:length(exp_conditions)
                peak_to_peak{n}(s,jj,ii) = get_peak_to_peak(erps{jj}(:,chan_idx(ii),iti_idx),res.dly,erp_range);
            end
        end
        % show that this file has been loaded
        disp(fl);
    end
    % get the dates for each session
    all_dates{n} = load_session_dates(sbjs);
    % save the list of subjects
    all_sbjs{n} = sbjs;
end

%% Plotting
plot_values_by_date(peak_to_peak,'Peak-to-peak amplitude (\muV)',...
    exp_conditions,chan_to_plot,exp_labels,all_dates,[1000 650],...
    sprintf('%.1f s ITI, ref %s',iti,reference));

%% Functions
function ptp = get_peak_to_peak(erp,dly,erp_range)
% Compute the peak-to-peak amplitude of a single-channel ERP signal within
% a particular range of delays (in ms)
use_idx = dly>=erp_range(1) & dly<=erp_range(2);
ptp = max(erp(use_idx)) - min(erp(use_idx));
end