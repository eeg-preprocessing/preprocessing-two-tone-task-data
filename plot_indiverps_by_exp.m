function plot_indiverps_by_exp(erps,dly,ch_idx,iti_idx,cond_labels,ch_labels,...
    iti_values,exp_labels,sbj_labels,mark_ranges)
% Plot individual ERPs for each set of sessions in an experiment as subplots. 
% ERPs for each ITI are plotted in the same subplot, and experiments /
% channels / conditions are plotted as separate figures.
% figure.
% Inputs:
% - erps = a cell array of dimensions: conditions x experiments. Each
%       cell contains a 4D array with dimensions: delay x channels x ITI x
%       session
% - dly = array of ERP delays
% - ch_idx = set of indexes for the channels that should be plotted
% - iti_idx = set of indexes for the ITIs that should be plotted
% - cond_labels = labels for each condition (cell array)
% - ch_labels = labels for each channel (cell array)
% - iti_values = values for each ITI (numeric array)
% - exp_labels = labels for each experiment (cell array)
% - sbj_labels = labels for each session (cell array of cell arrays, one
% cell per experiment)
% - mark_ranges (optional) = even-numbered array containing the range of
% delays that should be marked in each plot, for comparison across
% subjects. Each successive pair contains the start/end delays of each
% range.
% Nate Zuk (2021)

ncond = length(cond_labels);
nexp = length(exp_labels);

if nargin < 10
    % Assume no markings if not specified
    mark_ranges = [];
end

% setup the color map
try
    cmap = colormap('turbo');
catch err
    cmap = colormap('jet')*0.8;
end

iti_leg = cell(length(iti_idx),1);
for n = 1:length(iti_idx)
    iti_leg{n} = sprintf('%.1f s ITI',iti_values(n));
end

for jj = 1:nexp
    for ii = 1:ncond
        for c = 1:length(ch_idx)
            figure('Name',sprintf('%s, %s, %s',exp_labels{jj},cond_labels{ii},ch_labels{c}));
            % iterate over subjects
            %sp_erp = squeeze(erps{ii,jj}(:,ch_idx(c),iti_idx,:));
            nsbj = size(erps{ii,jj},4); % get the number of sessions
            sp_erp = reshape(erps{ii,jj}(:,ch_idx(c),iti_idx,:),...
                [length(dly),length(iti_idx),nsbj]);
            for s = 1:nsbj
                % setup the subplot for this session (N x 5 array)
                subplot(ceil(nsbj/5),5,s);
                hold on
                % plot each ITI
                for n = 1:length(iti_idx)
                    % plot each subject as a different color
                    clr_idx = get_color_idx(n,length(iti_idx),cmap);
                    plot(dly,sp_erp(:,n,s),'Color',cmap(clr_idx,:),'LineWidth',1.5);
                    set(gca,'FontSize',11,'YLim',[-4 4]);
                end
                % mark ranges, if specified
%                 for m = 2:2:length(mark_ranges)
%                     midx = dly>=mark_ranges(m-1) & dly<=mark_ranges(m);
%                     plot(dly(midx),ones(sum(midx),1)*2,'r','LineWidth',2);
%                 end
                for m = 1:length(mark_ranges)
                    plot(mark_ranges(m)*ones(2,1),[-4 4],'k--','LineWidth',1);
                end
                % use the last plot to show the x and y axis labels
                xlabel('Time (ms)');
                ylabel('ERP (\muV)');
                title(sbj_labels{jj}{s})
            end
            % only show the legend for the last session
            legend(iti_leg);
        end
    end
end