function [ERP,dly,nonlin] = eventtrigeeg(stim,eeg,Fs,mindly,maxdly,idx,varargin)
% Compute the event-triggered EEG signal, equivalent to an ERP (event-related potential). 
% Inputs:
% - eeg = cell array containing EEG per trial
% - stim = cell array of stimulus vector for each trial (must contain 1s or 0s)
% - Fs = sampling frequency (Hz)
% - mindly = minimum possible delay of the model (ms)
% - maxdly = maximum possible delay of the model (ms)
% - idx = cell array (one element for each trial) 
% Outputs:
% - ERP = event-related potential model (lags x channels)
% - dly = delays corresponding to the ERP (in ms, negative delays
% correspond to EEG delay after event time)
% - nonlin = nonlinearity parameters, assuming Bernoulli distributed events
% Nate Zuk (2017)
% UPDATED: Nate Zuk (2020)
%   - Removed z-scoring. Assume that users will do this beforehand if
%   needed.
%   - Added nonlinearity computation
%   - Added documentation

ncond = size(stim{1},2); % # of conditions (columns of stim)

if nargin < 6 || isempty(idx), % if idx isn't specified...
    idx = {}; % set an empty cell
end

verbose = 1;

% Parse varargin
if ~isempty(varargin),
    for n = 2:2:length(varargin),
        eval([varargin{n-1} '=varargin{n};']);
    end
end

dly = -floor(mindly/1000*Fs):-1:-ceil(maxdly/1000*Fs);

%% Create the matrices for X and Y
if verbose, fprintf('Creating X and Y matrices...'); end
mattm = tic;
% X = create_design_matrix(eeg,dly,idx,'column_center',false,'row_center',true);
X = create_design_matrix(eeg,dly,idx,'column_center',false);
    % don't zero-center columns to get an unbiased ERP
Y = create_design_matrix(stim,0,idx,'column_center',false);
clear eeg
if verbose, fprintf('Completed @ %.3f s\n',toc(mattm)); end

% Compute XTX and XTy
[~,xty] = compute_linreg_matrices(X,Y,[],[],'verbose',verbose);

%% Compute the ERP model for each lambda value
if verbose, fprintf('Computing ERP model\n'); end
ERP = NaN(size(xty));
for c = 1:ncond,
    % divide by the number of events (non-whitened ERP)
    nevt = sum(cellfun(@(x) sum(x(:,c)),stim));
    ERP(:,c) = 1/nevt*xty(:,c);
end

% convert delays into ms
dly = dly/Fs*1000;

%% Compute nonlinearity
if nargout == 3 % if the user has specified outputing the nonlinearity parameters
    if verbose, fprintf('Computing nonlinearity\n'); end
    x = cell_to_time_samples(X);
    y = cell_to_time_samples(Y);
    nonlin = fit_nonlinearity(x,y,ERP);
end