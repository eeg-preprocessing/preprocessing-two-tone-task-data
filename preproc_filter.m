function flteeg = preproc_filter(eeg,Fs,varargin)
% Filter the eeg using a zero-phase lowpass butterworth filter 
% Setup for Prob_sequence

N = 4; % order of the butterworth filter
Fc_low = 1; % 3 dB lower cutoff frequency of the butterworth filter
Fc_high = 30; % 3 dB upper cutoff frequency of the butterworth filter

if ~isempty(varargin),
    for n = 2:2:length(varargin),
        eval([varargin{n-1} '=varargin{n};']);
    end
end

% Generate bandpass filter
b = fdesign.bandpass('N,F3dB1,F3dB2',N,Fc_low,Fc_high,Fs);
bpf = design(b,'butter');

% Pad the start and end of the signal with constant terms equal to the
% first and last indexes respectively. The duration of the padding is 2x
% the bandwidth of the lower cutoff frequency
padding_size = ceil(10/Fc_low*Fs);
nchan = size(eeg,2);
padded_eeg = [ones(padding_size,nchan)*mean(eeg(1:padding_size,:)); ...
    eeg; ...
    ones(padding_size,nchan)*mean(eeg(end-(1:padding_size),:))];

flt_padded_eeg = filtfilthd(bpf,padded_eeg);

% Remove the indexes for the padding
flteeg = flt_padded_eeg((padding_size+1):(end-padding_size),:);
