# Preprocessing two-tone task data

This project contains a re-analysis of EEG data collected during a two-tone discrimination task (active condition) and presentation of the tones during a movie (passive). I will be using the same preprocessing pipeline on all bdf files to evaluate if/how the results change over multiple experiments using this same paradigm (see [Jaffe-Dax et al, 2017][jaffedax2017] and more recent work with ASD subjects).

The main scripts used for preprocessing and ERP calculation are:

`PreprocessBDFFile.m` = Runs the preprocessing steps on a bdf file. This includes:

1. Robust linear detrending
2. Bandpass filtering between 1-30 Hz with a 4th order butterworth filter (`prefiltereeg.m`), and removal of edge artifacts produced by the filter using linear regression (`rmfltartifact.m`)
3. Use linear regression to remove eyeblinks and eye movement artifacts via the best fit of EOGu, EOGd, HL, and HR.

`CalculateERPs.m` = Removes the reference (either the nose electrode or the average of the mastoids), and then calculates the average evoked response (`compute_erp.m`). (Note: Events with large voltages, > 30uV, are not removed in its current form. To reject these events, uncomment lines 65-66 in `compute_erp.m`. Otherwise, you will be notified when an event should be rejected, but events are not actually rejected.)

These preprocessing scripts save the figures in pdfs. You must have a directory in the same path as these scripts called `fig` with subdirectories named identically to the experiment labels (`sagi2015`, `Shahaf2017`, etc.).

The scripts that plot all session data are:

`ComputePeaktoPeak.m` = Plot the peak to peak voltages between 0-300 ms for the average evoked response in each session.
`ComputePeaktoPeak_plotbydate.m` = same as `ComputePeaktoPeak.m`, but plot the sessions chronologically instead of alphabetically by session tag.
`ComputeAvgSessionERP.m` = Plot the average ERP for each session and overlay the session-averaged ERPs for each experiment. A separate figure is used for each ITI and channel.
`ComputeAvgERPbyITI.m` = Plot the session-averaged ERPs for each ITI.

[jaffedax2017]: https://elifesciences.org/articles/20557
